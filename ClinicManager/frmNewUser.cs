using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmNewUser : Form
	{
		public frmNewUser()
		{
			InitializeComponent();
			this.gbxButtons.BackColor = this.BackColor;
			this.gbxDataGrid.BackColor = this.BackColor;
			this.gbxFields.BackColor = this.BackColor;
			this.dgdUsers.BackgroundColor = this.BackColor;
			this.dgdUsers.GridColor = this.BackColor;
			FillDataGrid();
		}

		#region "Common Methods"

		private void ClearForm()
		{
			txtUsername.Text = "";
			txtPassword.Text = "default";
			txtNationalIDNo.Text = "";
			cmbUserType.Text = "standard";
			txtSurname.Text = "";
			txtName.Text = "";

			txtUsername.Focus();
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllUserRecords();
			dgdUsers.DataSource = ds;
			dgdUsers.DataMember = ds.Tables[0].ToString();
			dgdUsers.Columns[1].Visible = false;
		}

		#endregion

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtUsername.Text)) {
				MessageBox.Show("Please enter the Username.", "Missing Input");
				this.txtUsername.Focus();
				return;
			}

			if (string.IsNullOrWhiteSpace(this.cmbUserType.Text)) {
				MessageBox.Show("Please enter the User Type.", "Missing Input");
				this.cmbUserType.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtNationalIDNo.Text)) {
				MessageBox.Show("Please enter the User's National ID Number.", "Missing Input");
				this.txtNationalIDNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtName.Text)) {
				MessageBox.Show("Please enter the User's Name.", "Missing Input");
				this.txtName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtSurname.Text)) {
				MessageBox.Show("Please enter the User's Surname.", "Missing Input");
				this.txtSurname.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreateUserRecord(this);
			this.Close();
		}

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtUsername.Text)) {
				MessageBox.Show("Please enter the Username.", "Missing Input");
				this.txtUsername.Focus();
				return;
			}

			if (string.IsNullOrWhiteSpace(this.cmbUserType.Text)) {
				MessageBox.Show("Please enter the User Type.", "Missing Input");
				this.cmbUserType.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtNationalIDNo.Text)) {
				MessageBox.Show("Please enter the User's National ID Number.", "Missing Input");
				this.txtNationalIDNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtName.Text)) {
				MessageBox.Show("Please enter the User's Name.", "Missing Input");
				this.txtName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtSurname.Text)) {
				MessageBox.Show("Please enter the User's Surname.", "Missing Input");
				this.txtSurname.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreateUserRecord(this);
			FillDataGrid();
			ClearForm();
		}
	}
}
