using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmNewPractitioner : Form
	{

		private DataTable tablePractitionerStatus;
		public frmNewPractitioner(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			this.gbxButtons.BackColor = this.BackColor;
			this.gbxDataGrid.BackColor = this.BackColor;
			this.gbxFields.BackColor = this.BackColor;
			this.dgdPractitioners.BackgroundColor = this.BackColor;
			this.dgdPractitioners.GridColor = this.BackColor;
			FillDataGrid();

			EntityChange += handler;
		}

		#region "Common Methods"

		private void InitializeDataTables()
		{
			tablePractitionerStatus = (new DataAccess().ReadPractitionerStatus()).Tables[0];
			if (tablePractitionerStatus != null) {
				this.cmbPractitionerStatus.DisplayMember = "STATUSCODE";
				this.cmbPractitionerStatus.ValueMember = "STATUSCODE";
				this.cmbPractitionerStatus.DataSource = tablePractitionerStatus;
			}
		}

		private void ClearForm()
		{
			txtPractitionerCode.Text = "";
			txtPractitionerName.Text = "";
			cmbPractitionerStatus.Text = "";
			txtQualification.Text = "";
			txtEmailAddress.Text = "";
			txtTelNo.Text = "";
			txtPhysicalAddress.Text = "";
			txtStreet.Text = "";
			cmbCountry.Text = Convert.ToString(cmbCountry.Items[cmbCountry.Items.Count - 1]);
			cmbCity.Text = Convert.ToString(cmbCity.Items[0]);

			txtPractitionerCode.Focus();
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllPractitionerRecords();
			dgdPractitioners.DataSource = ds;
			dgdPractitioners.DataMember = ds.Tables[0].ToString();
		}

		public event EventHandler EntityChange;

		#endregion

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtPractitionerCode.Text)) {
				MessageBox.Show("Please enter the Practitioner Code.", "Missing Input");
				this.txtPractitionerCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPractitionerName.Text)) {
				MessageBox.Show("Please enter the Practitioner's Name.", "Missing Input");
				this.txtPractitionerName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.cmbPractitionerStatus.Text)) {
				MessageBox.Show("Please enter the Practitioner's status.", "Missing Input");
				this.cmbPractitionerStatus.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtQualification.Text)) {
				MessageBox.Show("Please enter the Contact Person for the Practitioner.", "Missing Input");
				this.txtQualification.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtTelNo.Text)) {
				MessageBox.Show("Please enter the Practitioner's telephone number.", "Missing Input");
				this.txtTelNo.Focus();
				return;
			}
			int telNo = 0;
			if (!(int.TryParse(txtTelNo.Text, out telNo))) {
				MessageBox.Show("Only numeric values permitted for telephone number.", "Invalid Input");
				txtTelNo.SelectAll();
				txtTelNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPhysicalAddress.Text)) {
				MessageBox.Show("Please enter the Practitioner's physical address.", "Missing Input");
				this.txtPhysicalAddress.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreatePractitionerRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.Close();
		}

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtPractitionerCode.Text)) {
				MessageBox.Show("Please enter the Practitioner Code.", "Missing Input");
				this.txtPractitionerCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPractitionerName.Text)) {
				MessageBox.Show("Please enter the Practitioner's Name.", "Missing Input");
				this.txtPractitionerName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.cmbPractitionerStatus.Text)) {
				MessageBox.Show("Please enter the Practitioner's status.", "Missing Input");
				this.cmbPractitionerStatus.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtQualification.Text)) {
				MessageBox.Show("Please enter the Contact Person for the Practitioner.", "Missing Input");
				this.txtQualification.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtTelNo.Text)) {
				MessageBox.Show("Please enter the Practitioner's telephone number.", "Missing Input");
				this.txtTelNo.Focus();
				return;
			}
			int telNo = 0;
			if (!(int.TryParse(txtTelNo.Text, out telNo))) {
				MessageBox.Show("Only numeric values permitted for telephone number.", "Invalid Input");
				txtTelNo.SelectAll();
				txtTelNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPhysicalAddress.Text)) {
				MessageBox.Show("Please enter the Practitioner's physical address.", "Missing Input");
				this.txtPhysicalAddress.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreatePractitionerRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			FillDataGrid();
			ClearForm();
		}
	}
}
