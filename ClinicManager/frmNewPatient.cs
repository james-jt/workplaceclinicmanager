using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmNewPatient : Form, IUpdatesDashboard
	{
		public frmNewPatient(EventHandler handler)
		{
			InitializeComponent();
			this.gbxButtons.BackColor = this.BackColor;
			this.gbxDataGrid.BackColor = this.BackColor;
			this.gbxFields.BackColor = this.BackColor;
			this.dgdPatients.BackgroundColor = this.BackColor;
			this.dgdPatients.GridColor = this.BackColor;

			FillDataGrid();

			EntityChange += handler;
		}

		#region "Common Methods"

		private void ClearForm()
		{
			txtPatientName.Text = "";
			txtPatientSurname.Text = "";
			txtEmpNo.Text = "";
			txtDpt.Text = "";
			dtpPatientDOB.Value = System.DateTime.Today.AddYears(-30);
			txtPatientName.Focus();
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllPatientRecords();
			dgdPatients.DataSource = ds;
			dgdPatients.DataMember = ds.Tables[0].ToString();
		}

		public event EventHandler EntityChange;

		#endregion


		#region "Events"

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtPatientName.Text)) {
				MessageBox.Show("Please Enter Patient's Name.", "Missing Input");
				this.txtPatientName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPatientSurname.Text)) {
				MessageBox.Show("Please Enter Patient's Surname.", "Missing Input");
				this.txtPatientSurname.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtEmpNo.Text)) {
				MessageBox.Show("Please Enter Patient's Employee Number.", "Missing Input");
				this.txtEmpNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtDpt.Text)) {
				MessageBox.Show("Please Enter Patient's Department.", "Missing Input");
				this.txtDpt.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreatePatientRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			FillDataGrid();
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtPatientName.Text)) {
				MessageBox.Show("Please enter patient's name.", "Missing Input");
				this.txtPatientName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPatientSurname.Text)) {
				MessageBox.Show("Please enter patient's surname.", "Missing Input");
				this.txtPatientSurname.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtEmpNo.Text)) {
				MessageBox.Show("Please enter patient's employee number.", "Missing Input");
				this.txtEmpNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtDpt.Text)) {
				MessageBox.Show("Please enter patient's department.", "Missing Input");
				this.txtDpt.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreatePatientRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		#endregion
	}
}
