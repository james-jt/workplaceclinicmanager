using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualBasic.Devices;

// This file was created by the VB to C# converter (SharpDevelop 4.4.2.9749).
// It contains classes for supporting the VB "My" namespace in C#.
// If the VB application does not use the "My" namespace, or if you removed the usage
// after the conversion to C#, you can delete this file.

namespace Pharmacy.My
{
	sealed partial class MyProject
	{
		[ThreadStatic] static MyApplication application;
		
		public static MyApplication Application {
			[DebuggerStepThrough]
			get {
				if (application == null)
					application = new MyApplication();
				return application;
			}
		}
		
		[ThreadStatic] static MyComputer computer;
		
		public static MyComputer Computer {
			[DebuggerStepThrough]
			get {
				if (computer == null)
					computer = new MyComputer();
				return computer;
			}
		}
		
		[ThreadStatic] static User user;
		
		public static User User {
			[DebuggerStepThrough]
			get {
				if (user == null)
					user = new User();
				return user;
			}
		}
		
		[ThreadStatic] static MyForms forms;
		
		public static MyForms Forms {
			[DebuggerStepThrough]
			get {
				if (forms == null)
					forms = new MyForms();
				return forms;
			}
		}
		
		internal sealed class MyForms
		{
			global::Pharmacy.frmViewPractitioner frmViewPractitioner_instance;
			bool frmViewPractitioner_isCreating;
			public global::Pharmacy.frmViewPractitioner frmViewPractitioner {
				[DebuggerStepThrough] get { return GetForm(ref frmViewPractitioner_instance, ref frmViewPractitioner_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmViewPractitioner_instance, value); }
			}
			
			global::Pharmacy.frmNewPatient frmNewPatient_instance;
			bool frmNewPatient_isCreating;
			public global::Pharmacy.frmNewPatient frmNewPatient {
				[DebuggerStepThrough] get { return GetForm(ref frmNewPatient_instance, ref frmNewPatient_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmNewPatient_instance, value); }
			}
			
			global::Pharmacy.frmAddAdjustments frmAddAdjustments_instance;
			bool frmAddAdjustments_isCreating;
			public global::Pharmacy.frmAddAdjustments frmAddAdjustments {
				[DebuggerStepThrough] get { return GetForm(ref frmAddAdjustments_instance, ref frmAddAdjustments_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmAddAdjustments_instance, value); }
			}
			
			global::Pharmacy.frmViewSupplier frmViewSupplier_instance;
			bool frmViewSupplier_isCreating;
			public global::Pharmacy.frmViewSupplier frmViewSupplier {
				[DebuggerStepThrough] get { return GetForm(ref frmViewSupplier_instance, ref frmViewSupplier_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmViewSupplier_instance, value); }
			}
			
			global::Pharmacy.frmNewPractitioner frmNewPractitioner_instance;
			bool frmNewPractitioner_isCreating;
			public global::Pharmacy.frmNewPractitioner frmNewPractitioner {
				[DebuggerStepThrough] get { return GetForm(ref frmNewPractitioner_instance, ref frmNewPractitioner_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmNewPractitioner_instance, value); }
			}
			
			global::Pharmacy.frmViewUser frmViewUser_instance;
			bool frmViewUser_isCreating;
			public global::Pharmacy.frmViewUser frmViewUser {
				[DebuggerStepThrough] get { return GetForm(ref frmViewUser_instance, ref frmViewUser_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmViewUser_instance, value); }
			}
			
			global::Pharmacy.frmAddBatches frmAddBatches_instance;
			bool frmAddBatches_isCreating;
			public global::Pharmacy.frmAddBatches frmAddBatches {
				[DebuggerStepThrough] get { return GetForm(ref frmAddBatches_instance, ref frmAddBatches_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmAddBatches_instance, value); }
			}
			
			global::Pharmacy.frmNewSupplier frmNewSupplier_instance;
			bool frmNewSupplier_isCreating;
			public global::Pharmacy.frmNewSupplier frmNewSupplier {
				[DebuggerStepThrough] get { return GetForm(ref frmNewSupplier_instance, ref frmNewSupplier_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmNewSupplier_instance, value); }
			}
			
			global::Pharmacy.frmNewUser frmNewUser_instance;
			bool frmNewUser_isCreating;
			public global::Pharmacy.frmNewUser frmNewUser {
				[DebuggerStepThrough] get { return GetForm(ref frmNewUser_instance, ref frmNewUser_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmNewUser_instance, value); }
			}
			
			global::Pharmacy.frmAddDrugReturns frmAddDrugReturns_instance;
			bool frmAddDrugReturns_isCreating;
			public global::Pharmacy.frmAddDrugReturns frmAddDrugReturns {
				[DebuggerStepThrough] get { return GetForm(ref frmAddDrugReturns_instance, ref frmAddDrugReturns_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmAddDrugReturns_instance, value); }
			}
			
			global::Pharmacy.frmLogIn frmLogIn_instance;
			bool frmLogIn_isCreating;
			public global::Pharmacy.frmLogIn frmLogIn {
				[DebuggerStepThrough] get { return GetForm(ref frmLogIn_instance, ref frmLogIn_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmLogIn_instance, value); }
			}
			
			global::Pharmacy.frmReport frmReport_instance;
			bool frmReport_isCreating;
			public global::Pharmacy.frmReport frmReport {
				[DebuggerStepThrough] get { return GetForm(ref frmReport_instance, ref frmReport_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmReport_instance, value); }
			}
			
			global::Pharmacy.frmMain frmMain_instance;
			bool frmMain_isCreating;
			public global::Pharmacy.frmMain frmMain {
				[DebuggerStepThrough] get { return GetForm(ref frmMain_instance, ref frmMain_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmMain_instance, value); }
			}
			
			global::Pharmacy.frmAddDrugs frmAddDrugs_instance;
			bool frmAddDrugs_isCreating;
			public global::Pharmacy.frmAddDrugs frmAddDrugs {
				[DebuggerStepThrough] get { return GetForm(ref frmAddDrugs_instance, ref frmAddDrugs_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmAddDrugs_instance, value); }
			}
			
			global::Pharmacy.frmResetPassword frmResetPassword_instance;
			bool frmResetPassword_isCreating;
			public global::Pharmacy.frmResetPassword frmResetPassword {
				[DebuggerStepThrough] get { return GetForm(ref frmResetPassword_instance, ref frmResetPassword_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmResetPassword_instance, value); }
			}
			
			global::Pharmacy.frmChangePassword frmChangePassword_instance;
			bool frmChangePassword_isCreating;
			public global::Pharmacy.frmChangePassword frmChangePassword {
				[DebuggerStepThrough] get { return GetForm(ref frmChangePassword_instance, ref frmChangePassword_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmChangePassword_instance, value); }
			}
			
			global::Pharmacy.frmNewDrug frmNewDrug_instance;
			bool frmNewDrug_isCreating;
			public global::Pharmacy.frmNewDrug frmNewDrug {
				[DebuggerStepThrough] get { return GetForm(ref frmNewDrug_instance, ref frmNewDrug_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmNewDrug_instance, value); }
			}
			
			global::Pharmacy.frmStockAdjustment frmStockAdjustment_instance;
			bool frmStockAdjustment_isCreating;
			public global::Pharmacy.frmStockAdjustment frmStockAdjustment {
				[DebuggerStepThrough] get { return GetForm(ref frmStockAdjustment_instance, ref frmStockAdjustment_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmStockAdjustment_instance, value); }
			}
			
			global::Pharmacy.frmStockReturns frmStockReturns_instance;
			bool frmStockReturns_isCreating;
			public global::Pharmacy.frmStockReturns frmStockReturns {
				[DebuggerStepThrough] get { return GetForm(ref frmStockReturns_instance, ref frmStockReturns_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmStockReturns_instance, value); }
			}
			
			global::Pharmacy.frmDispensaryReturns frmDispensaryReturns_instance;
			bool frmDispensaryReturns_isCreating;
			public global::Pharmacy.frmDispensaryReturns frmDispensaryReturns {
				[DebuggerStepThrough] get { return GetForm(ref frmDispensaryReturns_instance, ref frmDispensaryReturns_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmDispensaryReturns_instance, value); }
			}
			
			global::Pharmacy.frmStockReceipt frmStockReceipt_instance;
			bool frmStockReceipt_isCreating;
			public global::Pharmacy.frmStockReceipt frmStockReceipt {
				[DebuggerStepThrough] get { return GetForm(ref frmStockReceipt_instance, ref frmStockReceipt_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmStockReceipt_instance, value); }
			}
			
			global::Pharmacy.frmViewDrug frmViewDrug_instance;
			bool frmViewDrug_isCreating;
			public global::Pharmacy.frmViewDrug frmViewDrug {
				[DebuggerStepThrough] get { return GetForm(ref frmViewDrug_instance, ref frmViewDrug_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmViewDrug_instance, value); }
			}
			
			global::Pharmacy.frmDispenseDrugs frmDispenseDrugs_instance;
			bool frmDispenseDrugs_isCreating;
			public global::Pharmacy.frmDispenseDrugs frmDispenseDrugs {
				[DebuggerStepThrough] get { return GetForm(ref frmDispenseDrugs_instance, ref frmDispenseDrugs_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmDispenseDrugs_instance, value); }
			}
			
			global::Pharmacy.frmViewPatient frmViewPatient_instance;
			bool frmViewPatient_isCreating;
			public global::Pharmacy.frmViewPatient frmViewPatient {
				[DebuggerStepThrough] get { return GetForm(ref frmViewPatient_instance, ref frmViewPatient_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmViewPatient_instance, value); }
			}
			
			global::Pharmacy.frmHome frmHome_instance;
			bool frmHome_isCreating;
			public global::Pharmacy.frmHome frmHome {
				[DebuggerStepThrough] get { return GetForm(ref frmHome_instance, ref frmHome_isCreating); }
				[DebuggerStepThrough] set { SetForm(ref frmHome_instance, value); }
			}
			
			[DebuggerStepThrough]
			static T GetForm<T>(ref T instance, ref bool isCreating) where T : Form, new()
			{
				if (instance == null || instance.IsDisposed) {
					if (isCreating) {
						throw new InvalidOperationException(Utils.GetResourceString("WinForms_RecursiveFormCreate", new string[0]));
					}
					isCreating = true;
					try {
						instance = new T();
					} catch (System.Reflection.TargetInvocationException ex) {
						throw new InvalidOperationException(Utils.GetResourceString("WinForms_SeeInnerException", new string[] { ex.InnerException.Message }), ex.InnerException);
					} finally {
						isCreating = false;
					}
				}
				return instance;
			}
			
			[DebuggerStepThrough]
			static void SetForm<T>(ref T instance, T value) where T : Form
			{
				if (instance != value) {
					if (value == null) {
						instance.Dispose();
						instance = null;
					} else {
						throw new ArgumentException("Property can only be set to null");
					}
				}
			}
		}
	}
	
	partial class MyApplication : WindowsFormsApplicationBase
	{
		[STAThread]
		public static void Main(string[] args)
		{
			Application.SetCompatibleTextRenderingDefault(UseCompatibleTextRendering);
			MyProject.Application.Run(args);
		}
	}
	
	partial class MyComputer : Computer
	{
	}
}
