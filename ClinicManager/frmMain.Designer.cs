using Microsoft.VisualBasic;
using System;
using System.Windows.Forms.RibbonHelpers;
namespace Pharmacy
{

	partial class frmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.rbnAppRibbon = new System.Windows.Forms.Ribbon();
			this.btnShowHome = new System.Windows.Forms.RibbonButton();
			this.rtbHome = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
			this.btnDispenseDrugs = new System.Windows.Forms.RibbonButton();
			this.btnHomeDrugReturns = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
			this.btnHomeReceiveInventory = new System.Windows.Forms.RibbonButton();
			this.btnHomeAdjustInventory = new System.Windows.Forms.RibbonButton();
			this.btnHomeInventoryReturns = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
			this.btnHomeShowHome = new System.Windows.Forms.RibbonButton();
			this.btnHomeChangeTheme = new System.Windows.Forms.RibbonButton();
			this.btnOffice2007Theme = new System.Windows.Forms.RibbonButton();
			this.btnOffice2010Theme = new System.Windows.Forms.RibbonButton();
			this.btnOffice2013Theme = new System.Windows.Forms.RibbonButton();
			this.btnHomeChangePassword = new System.Windows.Forms.RibbonButton();
			this.rtbPatients = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
			this.btnNewPatient = new System.Windows.Forms.RibbonButton();
			this.btnViewPatient = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
			this.btnPatientsDispenseDrugs = new System.Windows.Forms.RibbonButton();
			this.btnDrugReturns = new System.Windows.Forms.RibbonButton();
			this.rtbDrugs = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
			this.btnNewDrug = new System.Windows.Forms.RibbonButton();
			this.btnViewDrug = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel10 = new System.Windows.Forms.RibbonPanel();
			this.btnReceiveInventory = new System.Windows.Forms.RibbonButton();
			this.btnAdjustInventory = new System.Windows.Forms.RibbonButton();
			this.btnInvetoryReturns = new System.Windows.Forms.RibbonButton();
			this.rtbSuppliers = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel11 = new System.Windows.Forms.RibbonPanel();
			this.btnNewSupplier = new System.Windows.Forms.RibbonButton();
			this.btnViewSupplier = new System.Windows.Forms.RibbonButton();
			this.rtbReports = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
			this.btnRptViewAllPatients = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
			this.btnRptViewDrugs = new System.Windows.Forms.RibbonButton();
			this.btnRptViewBatches = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel7 = new System.Windows.Forms.RibbonPanel();
			this.btnRptViewSuppliers = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel12 = new System.Windows.Forms.RibbonPanel();
			this.btnRptViewDispensaryTxn = new System.Windows.Forms.RibbonButton();
			this.btnRptViewInventory = new System.Windows.Forms.RibbonButton();
			this.rtbAdmin = new System.Windows.Forms.RibbonTab();
			this.ribbonPanel16 = new System.Windows.Forms.RibbonPanel();
			this.btnNewUser = new System.Windows.Forms.RibbonButton();
			this.btnViewUser = new System.Windows.Forms.RibbonButton();
			this.btnResetPassword = new System.Windows.Forms.RibbonButton();
			this.ribbonPanel14 = new System.Windows.Forms.RibbonPanel();
			this.btnNewPractitioner = new System.Windows.Forms.RibbonButton();
			this.btnViewPractitioner = new System.Windows.Forms.RibbonButton();
			this.ribbonComboBox2 = new System.Windows.Forms.RibbonComboBox();
			this.stpMainForm = new System.Windows.Forms.StatusStrip();
			this.pnlStatistics = new System.Windows.Forms.Panel();
			this.gbxReorderList = new System.Windows.Forms.GroupBox();
			this.lblStatsReorderDrugs = new System.Windows.Forms.Label();
			this.gbxStatistics = new System.Windows.Forms.GroupBox();
			this.lblStatsSuppliers = new System.Windows.Forms.Label();
			this.lblStatsDrugs = new System.Windows.Forms.Label();
			this.lblStatsPatients = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblTime = new System.Windows.Forms.Label();
			this.lblLoggedOnUser = new System.Windows.Forms.Label();
			this.lblDate = new System.Windows.Forms.Label();
			this.lblLoggedOnAs = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.tmrCurrentTime = new System.Windows.Forms.Timer(this.components);
			this.button1 = new System.Windows.Forms.RibbonButton();
			this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
			this.btnChangePassword = new System.Windows.Forms.RibbonButton();
			this.btnThemeOffice2013 = new System.Windows.Forms.RibbonButton();
			this.btnThemeOffice2010 = new System.Windows.Forms.RibbonButton();
			this.btnThemeOffice2007 = new System.Windows.Forms.RibbonButton();
			this.btnChangeTheme = new System.Windows.Forms.RibbonButton();
			this.lblStatsPractitioners = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.pnlStatistics.SuspendLayout();
			this.gbxReorderList.SuspendLayout();
			this.gbxStatistics.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// rbnAppRibbon
			// 
			this.rbnAppRibbon.Cursor = System.Windows.Forms.Cursors.Default;
			this.rbnAppRibbon.Font = new System.Drawing.Font("Trebuchet MS", 9f);
			this.rbnAppRibbon.Location = new System.Drawing.Point(0, 0);
			this.rbnAppRibbon.Minimized = false;
			this.rbnAppRibbon.Name = "rbnAppRibbon";
			// 
			// 
			// 
			this.rbnAppRibbon.OrbDropDown.AllowDrop = true;
			this.rbnAppRibbon.OrbDropDown.BorderRoundness = 8;
			this.rbnAppRibbon.OrbDropDown.Location = new System.Drawing.Point(0, 0);
			this.rbnAppRibbon.OrbDropDown.Name = "File";
			this.rbnAppRibbon.OrbDropDown.Size = new System.Drawing.Size(0, 72);
			this.rbnAppRibbon.OrbDropDown.TabIndex = 0;
			this.rbnAppRibbon.OrbImage = null;
			this.rbnAppRibbon.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010;
			this.rbnAppRibbon.OrbText = "File";
			this.rbnAppRibbon.OrbVisible = false;
			// 
			// 
			// 
			this.rbnAppRibbon.QuickAcessToolbar.Items.Add(this.btnShowHome);
			this.rbnAppRibbon.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9f);
			this.rbnAppRibbon.Size = new System.Drawing.Size(1016, 175);
			this.rbnAppRibbon.TabIndex = 0;
			this.rbnAppRibbon.Tabs.Add(this.rtbHome);
			this.rbnAppRibbon.Tabs.Add(this.rtbPatients);
			this.rbnAppRibbon.Tabs.Add(this.rtbDrugs);
			this.rbnAppRibbon.Tabs.Add(this.rtbSuppliers);
			this.rbnAppRibbon.Tabs.Add(this.rtbReports);
			this.rbnAppRibbon.Tabs.Add(this.rtbAdmin);
			this.rbnAppRibbon.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
			this.rbnAppRibbon.ThemeColor = System.Windows.Forms.RibbonTheme.Green;
			// 
			// btnShowHome
			// 
			this.btnShowHome.Image = (System.Drawing.Image)resources.GetObject("btnShowHome.Image");
			this.btnShowHome.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
			this.btnShowHome.SmallImage = (System.Drawing.Image)resources.GetObject("btnShowHome.SmallImage");
			this.btnShowHome.Text = "Home";
			this.btnShowHome.Click += new System.EventHandler(this.btnShowHome_Click);
			// 
			// rtbHome
			// 
			this.rtbHome.Panels.Add(this.ribbonPanel4);
			this.rtbHome.Panels.Add(this.ribbonPanel3);
			this.rtbHome.Panels.Add(this.ribbonPanel9);
			this.rtbHome.Text = "Home";
			// 
			// ribbonPanel4
			// 
			this.ribbonPanel4.ButtonMoreEnabled = false;
			this.ribbonPanel4.ButtonMoreVisible = false;
			this.ribbonPanel4.Items.Add(this.btnDispenseDrugs);
			this.ribbonPanel4.Items.Add(this.btnHomeDrugReturns);
			this.ribbonPanel4.Text = "Drugs";
			// 
			// btnDispenseDrugs
			// 
			this.btnDispenseDrugs.Image = (System.Drawing.Image)resources.GetObject("btnDispenseDrugs.Image");
			this.btnDispenseDrugs.SmallImage = (System.Drawing.Image)resources.GetObject("btnDispenseDrugs.SmallImage");
			this.btnDispenseDrugs.Text = "Dispense";
			this.btnDispenseDrugs.Click += new System.EventHandler(this.btnDispenseDrugs_Click);
			// 
			// btnHomeDrugReturns
			// 
			this.btnHomeDrugReturns.Image = (System.Drawing.Image)resources.GetObject("btnHomeDrugReturns.Image");
			this.btnHomeDrugReturns.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeDrugReturns.SmallImage");
			this.btnHomeDrugReturns.Text = "Returns";
			this.btnHomeDrugReturns.Click += new System.EventHandler(this.btnHomeDrugReturns_Click);
			// 
			// ribbonPanel3
			// 
			this.ribbonPanel3.ButtonMoreEnabled = false;
			this.ribbonPanel3.ButtonMoreVisible = false;
			this.ribbonPanel3.Items.Add(this.btnHomeReceiveInventory);
			this.ribbonPanel3.Items.Add(this.btnHomeAdjustInventory);
			this.ribbonPanel3.Items.Add(this.btnHomeInventoryReturns);
			this.ribbonPanel3.Text = "Inventory";
			// 
			// btnHomeReceiveInventory
			// 
			this.btnHomeReceiveInventory.Image = (System.Drawing.Image)resources.GetObject("btnHomeReceiveInventory.Image");
			this.btnHomeReceiveInventory.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeReceiveInventory.SmallImage");
			this.btnHomeReceiveInventory.Text = "Receive";
			this.btnHomeReceiveInventory.Click += new System.EventHandler(this.btnHomeReceiveInventory_Click);
			// 
			// btnHomeAdjustInventory
			// 
			this.btnHomeAdjustInventory.Image = (System.Drawing.Image)resources.GetObject("btnHomeAdjustInventory.Image");
			this.btnHomeAdjustInventory.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeAdjustInventory.SmallImage");
			this.btnHomeAdjustInventory.Text = "Adjust";
			this.btnHomeAdjustInventory.Click += new System.EventHandler(this.btnAdjustInventory_Click);
			// 
			// btnHomeInventoryReturns
			// 
			this.btnHomeInventoryReturns.Image = (System.Drawing.Image)resources.GetObject("btnHomeInventoryReturns.Image");
			this.btnHomeInventoryReturns.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeInventoryReturns.SmallImage");
			this.btnHomeInventoryReturns.Text = "Returns";
			this.btnHomeInventoryReturns.Click += new System.EventHandler(this.btnHomeInventoryReturns_Click);
			// 
			// ribbonPanel9
			// 
			this.ribbonPanel9.ButtonMoreEnabled = false;
			this.ribbonPanel9.ButtonMoreVisible = false;
			this.ribbonPanel9.Items.Add(this.btnHomeShowHome);
			this.ribbonPanel9.Items.Add(this.btnHomeChangeTheme);
			this.ribbonPanel9.Items.Add(this.btnHomeChangePassword);
			this.ribbonPanel9.Text = "Tools";
			// 
			// btnHomeShowHome
			// 
			this.btnHomeShowHome.Image = (System.Drawing.Image)resources.GetObject("btnHomeShowHome.Image");
			this.btnHomeShowHome.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeShowHome.SmallImage");
			this.btnHomeShowHome.Text = "Home";
			this.btnHomeShowHome.Click += new System.EventHandler(this.btnHomeShowHome_Click);
			// 
			// btnHomeChangeTheme
			// 
			this.btnHomeChangeTheme.DropDownItems.Add(this.btnOffice2007Theme);
			this.btnHomeChangeTheme.DropDownItems.Add(this.btnOffice2010Theme);
			this.btnHomeChangeTheme.DropDownItems.Add(this.btnOffice2013Theme);
			this.btnHomeChangeTheme.Image = (System.Drawing.Image)resources.GetObject("btnHomeChangeTheme.Image");
			this.btnHomeChangeTheme.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeChangeTheme.SmallImage");
			this.btnHomeChangeTheme.Style = System.Windows.Forms.RibbonButtonStyle.DropDown;
			this.btnHomeChangeTheme.Text = "Theme";
			// 
			// btnOffice2007Theme
			// 
			this.btnOffice2007Theme.Image = (System.Drawing.Image)resources.GetObject("btnOffice2007Theme.Image");
			this.btnOffice2007Theme.SmallImage = (System.Drawing.Image)resources.GetObject("btnOffice2007Theme.SmallImage");
			this.btnOffice2007Theme.Text = "Office 2007";
			this.btnOffice2007Theme.Click += new System.EventHandler(this.btnThemeOffice2007_Click);
			// 
			// btnOffice2010Theme
			// 
			this.btnOffice2010Theme.Image = (System.Drawing.Image)resources.GetObject("btnOffice2010Theme.Image");
			this.btnOffice2010Theme.SmallImage = (System.Drawing.Image)resources.GetObject("btnOffice2010Theme.SmallImage");
			this.btnOffice2010Theme.Text = "Office 2010";
			this.btnOffice2010Theme.Click += new System.EventHandler(this.btnThemeOffice2010_Click);
			// 
			// btnOffice2013Theme
			// 
			this.btnOffice2013Theme.Image = (System.Drawing.Image)resources.GetObject("btnOffice2013Theme.Image");
			this.btnOffice2013Theme.SmallImage = (System.Drawing.Image)resources.GetObject("btnOffice2013Theme.SmallImage");
			this.btnOffice2013Theme.Text = "Office 2013";
			this.btnOffice2013Theme.Click += new System.EventHandler(this.btnThemeOffice2013_Click);
			// 
			// btnHomeChangePassword
			// 
			this.btnHomeChangePassword.Image = (System.Drawing.Image)resources.GetObject("btnHomeChangePassword.Image");
			this.btnHomeChangePassword.SmallImage = (System.Drawing.Image)resources.GetObject("btnHomeChangePassword.SmallImage");
			this.btnHomeChangePassword.Text = "Password";
			this.btnHomeChangePassword.Click += new System.EventHandler(this.btnHomeChangePassword_Click);
			// 
			// rtbPatients
			// 
			this.rtbPatients.Panels.Add(this.ribbonPanel5);
			this.rtbPatients.Panels.Add(this.ribbonPanel6);
			this.rtbPatients.Text = "Patients";
			// 
			// ribbonPanel5
			// 
			this.ribbonPanel5.ButtonMoreEnabled = false;
			this.ribbonPanel5.ButtonMoreVisible = false;
			this.ribbonPanel5.Items.Add(this.btnNewPatient);
			this.ribbonPanel5.Items.Add(this.btnViewPatient);
			this.ribbonPanel5.Text = "Patients";
			// 
			// btnNewPatient
			// 
			this.btnNewPatient.Image = (System.Drawing.Image)resources.GetObject("btnNewPatient.Image");
			this.btnNewPatient.SmallImage = (System.Drawing.Image)resources.GetObject("btnNewPatient.SmallImage");
			this.btnNewPatient.Text = "New Patient";
			this.btnNewPatient.Click += new System.EventHandler(this.btnNewPatient_Click);
			// 
			// btnViewPatient
			// 
			this.btnViewPatient.Image = (System.Drawing.Image)resources.GetObject("btnViewPatient.Image");
			this.btnViewPatient.SmallImage = (System.Drawing.Image)resources.GetObject("btnViewPatient.SmallImage");
			this.btnViewPatient.Text = "View Patient";
			this.btnViewPatient.Click += new System.EventHandler(this.btnViewPatient_Click);
			// 
			// ribbonPanel6
			// 
			this.ribbonPanel6.ButtonMoreEnabled = false;
			this.ribbonPanel6.ButtonMoreVisible = false;
			this.ribbonPanel6.Items.Add(this.btnPatientsDispenseDrugs);
			this.ribbonPanel6.Items.Add(this.btnDrugReturns);
			this.ribbonPanel6.Text = "Operations";
			// 
			// btnPatientsDispenseDrugs
			// 
			this.btnPatientsDispenseDrugs.Image = (System.Drawing.Image)resources.GetObject("btnPatientsDispenseDrugs.Image");
			this.btnPatientsDispenseDrugs.SmallImage = (System.Drawing.Image)resources.GetObject("btnPatientsDispenseDrugs.SmallImage");
			this.btnPatientsDispenseDrugs.Text = "Dispense Drugs";
			this.btnPatientsDispenseDrugs.Click += new System.EventHandler(this.btnPatientsDispenseDrugs_Click);
			// 
			// btnDrugReturns
			// 
			this.btnDrugReturns.Image = (System.Drawing.Image)resources.GetObject("btnDrugReturns.Image");
			this.btnDrugReturns.SmallImage = (System.Drawing.Image)resources.GetObject("btnDrugReturns.SmallImage");
			this.btnDrugReturns.Text = "Drug Returns";
			this.btnDrugReturns.Click += new System.EventHandler(this.btnDrugReturns_Click);
			// 
			// rtbDrugs
			// 
			this.rtbDrugs.Panels.Add(this.ribbonPanel8);
			this.rtbDrugs.Panels.Add(this.ribbonPanel10);
			this.rtbDrugs.Text = "Drugs";
			// 
			// ribbonPanel8
			// 
			this.ribbonPanel8.ButtonMoreEnabled = false;
			this.ribbonPanel8.ButtonMoreVisible = false;
			this.ribbonPanel8.Items.Add(this.btnNewDrug);
			this.ribbonPanel8.Items.Add(this.btnViewDrug);
			this.ribbonPanel8.Text = "Drugs";
			// 
			// btnNewDrug
			// 
			this.btnNewDrug.Image = (System.Drawing.Image)resources.GetObject("btnNewDrug.Image");
			this.btnNewDrug.SmallImage = (System.Drawing.Image)resources.GetObject("btnNewDrug.SmallImage");
			this.btnNewDrug.Text = "New Drug";
			this.btnNewDrug.Click += new System.EventHandler(this.btnNewDrug_Click);
			// 
			// btnViewDrug
			// 
			this.btnViewDrug.Image = (System.Drawing.Image)resources.GetObject("btnViewDrug.Image");
			this.btnViewDrug.SmallImage = (System.Drawing.Image)resources.GetObject("btnViewDrug.SmallImage");
			this.btnViewDrug.Text = "View Drug";
			this.btnViewDrug.Click += new System.EventHandler(this.btnViewDrug_Click);
			// 
			// ribbonPanel10
			// 
			this.ribbonPanel10.ButtonMoreEnabled = false;
			this.ribbonPanel10.ButtonMoreVisible = false;
			this.ribbonPanel10.Items.Add(this.btnReceiveInventory);
			this.ribbonPanel10.Items.Add(this.btnAdjustInventory);
			this.ribbonPanel10.Items.Add(this.btnInvetoryReturns);
			this.ribbonPanel10.Text = "Inventory";
			// 
			// btnReceiveInventory
			// 
			this.btnReceiveInventory.Image = (System.Drawing.Image)resources.GetObject("btnReceiveInventory.Image");
			this.btnReceiveInventory.SmallImage = (System.Drawing.Image)resources.GetObject("btnReceiveInventory.SmallImage");
			this.btnReceiveInventory.Text = "Receive";
			this.btnReceiveInventory.Click += new System.EventHandler(this.btnReceiveInventory_Click);
			// 
			// btnAdjustInventory
			// 
			this.btnAdjustInventory.Image = (System.Drawing.Image)resources.GetObject("btnAdjustInventory.Image");
			this.btnAdjustInventory.SmallImage = (System.Drawing.Image)resources.GetObject("btnAdjustInventory.SmallImage");
			this.btnAdjustInventory.Text = "Adjust";
			this.btnAdjustInventory.Click += new System.EventHandler(this.btnAdjustInventory_Click);
			// 
			// btnInvetoryReturns
			// 
			this.btnInvetoryReturns.Image = (System.Drawing.Image)resources.GetObject("btnInvetoryReturns.Image");
			this.btnInvetoryReturns.SmallImage = (System.Drawing.Image)resources.GetObject("btnInvetoryReturns.SmallImage");
			this.btnInvetoryReturns.Text = "Returns";
			this.btnInvetoryReturns.Click += new System.EventHandler(this.btnInvetoryReturns_Click);
			// 
			// rtbSuppliers
			// 
			this.rtbSuppliers.Panels.Add(this.ribbonPanel11);
			this.rtbSuppliers.Text = "Suppliers";
			// 
			// ribbonPanel11
			// 
			this.ribbonPanel11.ButtonMoreEnabled = false;
			this.ribbonPanel11.ButtonMoreVisible = false;
			this.ribbonPanel11.Items.Add(this.btnNewSupplier);
			this.ribbonPanel11.Items.Add(this.btnViewSupplier);
			this.ribbonPanel11.Text = "Suppliers";
			// 
			// btnNewSupplier
			// 
			this.btnNewSupplier.Image = (System.Drawing.Image)resources.GetObject("btnNewSupplier.Image");
			this.btnNewSupplier.SmallImage = (System.Drawing.Image)resources.GetObject("btnNewSupplier.SmallImage");
			this.btnNewSupplier.Text = "New Supplier";
			this.btnNewSupplier.Click += new System.EventHandler(this.btnNewSupplier_Click);
			// 
			// btnViewSupplier
			// 
			this.btnViewSupplier.Image = (System.Drawing.Image)resources.GetObject("btnViewSupplier.Image");
			this.btnViewSupplier.SmallImage = (System.Drawing.Image)resources.GetObject("btnViewSupplier.SmallImage");
			this.btnViewSupplier.Text = "View Supplier";
			this.btnViewSupplier.Click += new System.EventHandler(this.btnViewSupplier_Click);
			// 
			// rtbReports
			// 
			this.rtbReports.Panels.Add(this.ribbonPanel1);
			this.rtbReports.Panels.Add(this.ribbonPanel2);
			this.rtbReports.Panels.Add(this.ribbonPanel7);
			this.rtbReports.Panels.Add(this.ribbonPanel12);
			this.rtbReports.Text = "Reports";
			// 
			// ribbonPanel1
			// 
			this.ribbonPanel1.ButtonMoreEnabled = false;
			this.ribbonPanel1.ButtonMoreVisible = false;
			this.ribbonPanel1.Items.Add(this.btnRptViewAllPatients);
			this.ribbonPanel1.Text = "Patients";
			// 
			// btnRptViewAllPatients
			// 
			this.btnRptViewAllPatients.Image = (System.Drawing.Image)resources.GetObject("btnRptViewAllPatients.Image");
			this.btnRptViewAllPatients.SmallImage = (System.Drawing.Image)resources.GetObject("btnRptViewAllPatients.SmallImage");
			this.btnRptViewAllPatients.Text = "View All";
			this.btnRptViewAllPatients.Click += new System.EventHandler(this.btnRptViewAllPatients_Click);
			// 
			// ribbonPanel2
			// 
			this.ribbonPanel2.ButtonMoreEnabled = false;
			this.ribbonPanel2.ButtonMoreVisible = false;
			this.ribbonPanel2.Items.Add(this.btnRptViewDrugs);
			this.ribbonPanel2.Items.Add(this.btnRptViewBatches);
			this.ribbonPanel2.Text = "Drugs";
			// 
			// btnRptViewDrugs
			// 
			this.btnRptViewDrugs.Image = (System.Drawing.Image)resources.GetObject("btnRptViewDrugs.Image");
			this.btnRptViewDrugs.SmallImage = (System.Drawing.Image)resources.GetObject("btnRptViewDrugs.SmallImage");
			this.btnRptViewDrugs.Text = "View Drugs";
			this.btnRptViewDrugs.Click += new System.EventHandler(this.btnRptViewDrugs_Click);
			// 
			// btnRptViewBatches
			// 
			this.btnRptViewBatches.Image = (System.Drawing.Image)resources.GetObject("btnRptViewBatches.Image");
			this.btnRptViewBatches.SmallImage = (System.Drawing.Image)resources.GetObject("btnRptViewBatches.SmallImage");
			this.btnRptViewBatches.Text = "View Batches";
			this.btnRptViewBatches.Click += new System.EventHandler(this.btnRptViewBatches_Click);
			// 
			// ribbonPanel7
			// 
			this.ribbonPanel7.ButtonMoreEnabled = false;
			this.ribbonPanel7.ButtonMoreVisible = false;
			this.ribbonPanel7.Items.Add(this.btnRptViewSuppliers);
			this.ribbonPanel7.Text = "Suppliers";
			// 
			// btnRptViewSuppliers
			// 
			this.btnRptViewSuppliers.Image = (System.Drawing.Image)resources.GetObject("btnRptViewSuppliers.Image");
			this.btnRptViewSuppliers.SmallImage = (System.Drawing.Image)resources.GetObject("btnRptViewSuppliers.SmallImage");
			this.btnRptViewSuppliers.Text = "View Suppliers";
			this.btnRptViewSuppliers.Click += new System.EventHandler(this.btnRptViewSuppliers_Click);
			// 
			// ribbonPanel12
			// 
			this.ribbonPanel12.ButtonMoreEnabled = false;
			this.ribbonPanel12.ButtonMoreVisible = false;
			this.ribbonPanel12.Items.Add(this.btnRptViewDispensaryTxn);
			this.ribbonPanel12.Items.Add(this.btnRptViewInventory);
			this.ribbonPanel12.Text = "Transactions";
			// 
			// btnRptViewDispensaryTxn
			// 
			this.btnRptViewDispensaryTxn.Image = (System.Drawing.Image)resources.GetObject("btnRptViewDispensaryTxn.Image");
			this.btnRptViewDispensaryTxn.SmallImage = (System.Drawing.Image)resources.GetObject("btnRptViewDispensaryTxn.SmallImage");
			this.btnRptViewDispensaryTxn.Text = "View Dispensary";
			this.btnRptViewDispensaryTxn.Click += new System.EventHandler(this.btnRptViewDispensaryTxn_Click);
			// 
			// btnRptViewInventory
			// 
			this.btnRptViewInventory.Image = (System.Drawing.Image)resources.GetObject("btnRptViewInventory.Image");
			this.btnRptViewInventory.SmallImage = (System.Drawing.Image)resources.GetObject("btnRptViewInventory.SmallImage");
			this.btnRptViewInventory.Text = "View Inventory";
			this.btnRptViewInventory.Click += new System.EventHandler(this.btnRptViewInventory_Click);
			// 
			// rtbAdmin
			// 
			this.rtbAdmin.Panels.Add(this.ribbonPanel16);
			this.rtbAdmin.Panels.Add(this.ribbonPanel14);
			this.rtbAdmin.Text = "Admin";
			// 
			// ribbonPanel16
			// 
			this.ribbonPanel16.ButtonMoreEnabled = false;
			this.ribbonPanel16.ButtonMoreVisible = false;
			this.ribbonPanel16.Items.Add(this.btnNewUser);
			this.ribbonPanel16.Items.Add(this.btnViewUser);
			this.ribbonPanel16.Items.Add(this.btnResetPassword);
			this.ribbonPanel16.Text = "Users";
			// 
			// btnNewUser
			// 
			this.btnNewUser.Image = (System.Drawing.Image)resources.GetObject("btnNewUser.Image");
			this.btnNewUser.SmallImage = (System.Drawing.Image)resources.GetObject("btnNewUser.SmallImage");
			this.btnNewUser.Text = "New User";
			this.btnNewUser.Click += new System.EventHandler(this.btnNewUser_Click);
			// 
			// btnViewUser
			// 
			this.btnViewUser.Image = (System.Drawing.Image)resources.GetObject("btnViewUser.Image");
			this.btnViewUser.SmallImage = (System.Drawing.Image)resources.GetObject("btnViewUser.SmallImage");
			this.btnViewUser.Text = "View User";
			this.btnViewUser.Click += new System.EventHandler(this.btnViewUser_Click);
			// 
			// btnResetPassword
			// 
			this.btnResetPassword.Image = (System.Drawing.Image)resources.GetObject("btnResetPassword.Image");
			this.btnResetPassword.SmallImage = (System.Drawing.Image)resources.GetObject("btnResetPassword.SmallImage");
			this.btnResetPassword.Text = "Reset Password";
			this.btnResetPassword.Click += new System.EventHandler(this.btnResetPassword_Click);
			// 
			// ribbonPanel14
			// 
			this.ribbonPanel14.ButtonMoreEnabled = false;
			this.ribbonPanel14.ButtonMoreVisible = false;
			this.ribbonPanel14.Items.Add(this.btnNewPractitioner);
			this.ribbonPanel14.Items.Add(this.btnViewPractitioner);
			this.ribbonPanel14.Text = "Practitioners";
			// 
			// btnNewPractitioner
			// 
			this.btnNewPractitioner.Image = (System.Drawing.Image)resources.GetObject("btnNewPractitioner.Image");
			this.btnNewPractitioner.SmallImage = (System.Drawing.Image)resources.GetObject("btnNewPractitioner.SmallImage");
			this.btnNewPractitioner.Text = "New Practitioner";
			this.btnNewPractitioner.Click += new System.EventHandler(this.btnNewPractitioner_Click);
			// 
			// btnViewPractitioner
			// 
			this.btnViewPractitioner.Image = (System.Drawing.Image)resources.GetObject("btnViewPractitioner.Image");
			this.btnViewPractitioner.SmallImage = (System.Drawing.Image)resources.GetObject("btnViewPractitioner.SmallImage");
			this.btnViewPractitioner.Text = "View Practitioner";
			this.btnViewPractitioner.Click += new System.EventHandler(this.btnViewPractitioner_Click);
			// 
			// ribbonComboBox2
			// 
			this.ribbonComboBox2.Text = "Colors ";
			this.ribbonComboBox2.TextBoxText = "";
			// 
			// stpMainForm
			// 
			this.stpMainForm.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.stpMainForm.Location = new System.Drawing.Point(0, 687);
			this.stpMainForm.Name = "stpMainForm";
			this.stpMainForm.Size = new System.Drawing.Size(1016, 22);
			this.stpMainForm.TabIndex = 3;
			this.stpMainForm.Text = "statusStrip1";
			// 
			// pnlStatistics
			// 
			this.pnlStatistics.Controls.Add(this.gbxReorderList);
			this.pnlStatistics.Controls.Add(this.gbxStatistics);
			this.pnlStatistics.Controls.Add(this.groupBox1);
			this.pnlStatistics.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlStatistics.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.pnlStatistics.Location = new System.Drawing.Point(0, 175);
			this.pnlStatistics.Name = "pnlStatistics";
			this.pnlStatistics.Size = new System.Drawing.Size(247, 512);
			this.pnlStatistics.TabIndex = 4;
			// 
			// gbxReorderList
			// 
			this.gbxReorderList.Controls.Add(this.lblStatsReorderDrugs);
			this.gbxReorderList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxReorderList.Location = new System.Drawing.Point(0, 296);
			this.gbxReorderList.Name = "gbxReorderList";
			this.gbxReorderList.Size = new System.Drawing.Size(247, 216);
			this.gbxReorderList.TabIndex = 2;
			this.gbxReorderList.TabStop = false;
			// 
			// lblStatsReorderDrugs
			// 
			this.lblStatsReorderDrugs.AutoSize = true;
			this.lblStatsReorderDrugs.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblStatsReorderDrugs.Location = new System.Drawing.Point(6, 16);
			this.lblStatsReorderDrugs.Name = "lblStatsReorderDrugs";
			this.lblStatsReorderDrugs.Size = new System.Drawing.Size(134, 15);
			this.lblStatsReorderDrugs.TabIndex = 5;
			this.lblStatsReorderDrugs.Text = "Re-order Level Reached:";
			// 
			// gbxStatistics
			// 
			this.gbxStatistics.Controls.Add(this.lblStatsSuppliers);
			this.gbxStatistics.Controls.Add(this.lblStatsPractitioners);
			this.gbxStatistics.Controls.Add(this.lblStatsDrugs);
			this.gbxStatistics.Controls.Add(this.lblStatsPatients);
			this.gbxStatistics.Controls.Add(this.label5);
			this.gbxStatistics.Controls.Add(this.label4);
			this.gbxStatistics.Controls.Add(this.label3);
			this.gbxStatistics.Controls.Add(this.label2);
			this.gbxStatistics.Controls.Add(this.label1);
			this.gbxStatistics.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxStatistics.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.gbxStatistics.Location = new System.Drawing.Point(0, 59);
			this.gbxStatistics.Name = "gbxStatistics";
			this.gbxStatistics.Size = new System.Drawing.Size(247, 237);
			this.gbxStatistics.TabIndex = 1;
			this.gbxStatistics.TabStop = false;
			// 
			// lblStatsSuppliers
			// 
			this.lblStatsSuppliers.AutoSize = true;
			this.lblStatsSuppliers.Location = new System.Drawing.Point(115, 165);
			this.lblStatsSuppliers.Name = "lblStatsSuppliers";
			this.lblStatsSuppliers.Size = new System.Drawing.Size(15, 17);
			this.lblStatsSuppliers.TabIndex = 9;
			this.lblStatsSuppliers.Text = "0";
			// 
			// lblStatsDrugs
			// 
			this.lblStatsDrugs.AutoSize = true;
			this.lblStatsDrugs.Location = new System.Drawing.Point(62, 85);
			this.lblStatsDrugs.Name = "lblStatsDrugs";
			this.lblStatsDrugs.Size = new System.Drawing.Size(15, 17);
			this.lblStatsDrugs.TabIndex = 7;
			this.lblStatsDrugs.Text = "0";
			// 
			// lblStatsPatients
			// 
			this.lblStatsPatients.AutoSize = true;
			this.lblStatsPatients.Location = new System.Drawing.Point(73, 45);
			this.lblStatsPatients.Name = "lblStatsPatients";
			this.lblStatsPatients.Size = new System.Drawing.Size(15, 17);
			this.lblStatsPatients.TabIndex = 6;
			this.lblStatsPatients.Text = "0";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(58, 17);
			this.label5.TabIndex = 4;
			this.label5.Text = "Statistics";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.label4.Location = new System.Drawing.Point(20, 167);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(94, 15);
			this.label4.TabIndex = 3;
			this.label4.Text = "Active Suppliers:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.label2.Location = new System.Drawing.Point(20, 87);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "Drugs:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.label1.Location = new System.Drawing.Point(20, 47);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Patients:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblTime);
			this.groupBox1.Controls.Add(this.lblLoggedOnUser);
			this.groupBox1.Controls.Add(this.lblDate);
			this.groupBox1.Controls.Add(this.lblLoggedOnAs);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(247, 59);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// lblTime
			// 
			this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblTime.AutoSize = true;
			this.lblTime.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblTime.Location = new System.Drawing.Point(164, 41);
			this.lblTime.Name = "lblTime";
			this.lblTime.Size = new System.Drawing.Size(71, 15);
			this.lblTime.TabIndex = 3;
			this.lblTime.Text = "10:09:25 AM";
			// 
			// lblLoggedOnUser
			// 
			this.lblLoggedOnUser.AutoSize = true;
			this.lblLoggedOnUser.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblLoggedOnUser.Location = new System.Drawing.Point(93, 12);
			this.lblLoggedOnUser.Name = "lblLoggedOnUser";
			this.lblLoggedOnUser.Size = new System.Drawing.Size(43, 15);
			this.lblLoggedOnUser.TabIndex = 2;
			this.lblLoggedOnUser.Text = "Admin";
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = true;
			this.lblDate.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblDate.Location = new System.Drawing.Point(3, 41);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(142, 15);
			this.lblDate.TabIndex = 1;
			this.lblDate.Text = "Tuesday, 11 October 2016";
			// 
			// lblLoggedOnAs
			// 
			this.lblLoggedOnAs.AutoSize = true;
			this.lblLoggedOnAs.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblLoggedOnAs.Location = new System.Drawing.Point(3, 12);
			this.lblLoggedOnAs.Name = "lblLoggedOnAs";
			this.lblLoggedOnAs.Size = new System.Drawing.Size(84, 15);
			this.lblLoggedOnAs.TabIndex = 0;
			this.lblLoggedOnAs.Text = "Logged on as: ";
			// 
			// panel3
			// 
			this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel3.Location = new System.Drawing.Point(247, 175);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(10, 512);
			this.panel3.TabIndex = 8;
			// 
			// tmrCurrentTime
			// 
			this.tmrCurrentTime.Enabled = true;
			this.tmrCurrentTime.Interval = 1000;
			this.tmrCurrentTime.Tick += new System.EventHandler(this.tmrCurrentTime_Tick);
			// 
			// button1
			// 
			this.button1.Image = (System.Drawing.Image)resources.GetObject("button1.Image");
			this.button1.SmallImage = (System.Drawing.Image)resources.GetObject("button1.SmallImage");
			// 
			// ribbonButton3
			// 
			this.ribbonButton3.Image = (System.Drawing.Image)resources.GetObject("ribbonButton3.Image");
			this.ribbonButton3.SmallImage = (System.Drawing.Image)resources.GetObject("ribbonButton3.SmallImage");
			this.ribbonButton3.Text = "Update";
			// 
			// btnChangePassword
			// 
			this.btnChangePassword.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
			this.btnChangePassword.Image = (System.Drawing.Image)resources.GetObject("btnChangePassword.Image");
			this.btnChangePassword.SmallImage = (System.Drawing.Image)resources.GetObject("btnChangePassword.SmallImage");
			this.btnChangePassword.Text = "Change Password";
			// 
			// btnThemeOffice2013
			// 
			this.btnThemeOffice2013.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
			this.btnThemeOffice2013.Image = (System.Drawing.Image)resources.GetObject("btnThemeOffice2013.Image");
			this.btnThemeOffice2013.SmallImage = (System.Drawing.Image)resources.GetObject("btnThemeOffice2013.SmallImage");
			this.btnThemeOffice2013.Text = "Office 2013";
			this.btnThemeOffice2013.Click += new System.EventHandler(this.btnThemeOffice2013_Click);
			// 
			// btnThemeOffice2010
			// 
			this.btnThemeOffice2010.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
			this.btnThemeOffice2010.Image = (System.Drawing.Image)resources.GetObject("btnThemeOffice2010.Image");
			this.btnThemeOffice2010.SmallImage = (System.Drawing.Image)resources.GetObject("btnThemeOffice2010.SmallImage");
			this.btnThemeOffice2010.Text = "Office 2010";
			this.btnThemeOffice2010.Click += new System.EventHandler(this.btnThemeOffice2010_Click);
			// 
			// btnThemeOffice2007
			// 
			this.btnThemeOffice2007.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
			this.btnThemeOffice2007.Image = (System.Drawing.Image)resources.GetObject("btnThemeOffice2007.Image");
			this.btnThemeOffice2007.SmallImage = (System.Drawing.Image)resources.GetObject("btnThemeOffice2007.SmallImage");
			this.btnThemeOffice2007.Text = "Office 2007";
			this.btnThemeOffice2007.Click += new System.EventHandler(this.btnThemeOffice2007_Click);
			// 
			// btnChangeTheme
			// 
			this.btnChangeTheme.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
			this.btnChangeTheme.DropDownItems.Add(this.btnThemeOffice2007);
			this.btnChangeTheme.DropDownItems.Add(this.btnThemeOffice2010);
			this.btnChangeTheme.DropDownItems.Add(this.btnThemeOffice2013);
			this.btnChangeTheme.Image = (System.Drawing.Image)resources.GetObject("btnChangeTheme.Image");
			this.btnChangeTheme.MaximumSize = new System.Drawing.Size(2, 2);
			this.btnChangeTheme.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
			this.btnChangeTheme.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
			this.btnChangeTheme.SmallImage = (System.Drawing.Image)resources.GetObject("btnChangeTheme.SmallImage");
			this.btnChangeTheme.Style = System.Windows.Forms.RibbonButtonStyle.DropDown;
			this.btnChangeTheme.Text = "ribbonButton1";
			this.btnChangeTheme.ToolTip = "Change Theme";
			// 
			// lblStatsPractitioners
			// 
			this.lblStatsPractitioners.AutoSize = true;
			this.lblStatsPractitioners.Location = new System.Drawing.Point(97, 125);
			this.lblStatsPractitioners.Name = "lblStatsPractitioners";
			this.lblStatsPractitioners.Size = new System.Drawing.Size(15, 17);
			this.lblStatsPractitioners.TabIndex = 8;
			this.lblStatsPractitioners.Text = "0";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.label3.Location = new System.Drawing.Point(20, 127);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(76, 15);
			this.label3.TabIndex = 2;
			this.label3.Text = "Practitioners:";
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(1016, 709);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.pnlStatistics);
			this.Controls.Add(this.stpMainForm);
			this.Controls.Add(this.rbnAppRibbon);
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.IsMdiContainer = true;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Clinic Manager";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.pnlStatistics.ResumeLayout(false);
			this.gbxReorderList.ResumeLayout(false);
			this.gbxReorderList.PerformLayout();
			this.gbxStatistics.ResumeLayout(false);
			this.gbxStatistics.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Ribbon rbnAppRibbon;
		private System.Windows.Forms.RibbonTab rtbHome;
		private System.Windows.Forms.RibbonPanel ribbonPanel4;
		private System.Windows.Forms.RibbonTab rtbPatients;
		private System.Windows.Forms.RibbonPanel ribbonPanel5;
		private System.Windows.Forms.RibbonPanel ribbonPanel6;
		private System.Windows.Forms.RibbonTab rtbDrugs;
		private System.Windows.Forms.RibbonPanel ribbonPanel8;
		private System.Windows.Forms.RibbonButton button1;
		private System.Windows.Forms.RibbonTab rtbAdmin;
		private System.Windows.Forms.RibbonTab rtbSuppliers;
		private System.Windows.Forms.RibbonButton btnNewPatient;
		private System.Windows.Forms.RibbonButton btnViewPatient;
		private System.Windows.Forms.RibbonButton btnDrugReturns;
		private System.Windows.Forms.RibbonButton ribbonButton3;
		private System.Windows.Forms.RibbonPanel ribbonPanel10;
		private System.Windows.Forms.RibbonPanel ribbonPanel11;
		private System.Windows.Forms.RibbonPanel ribbonPanel16;
		private System.Windows.Forms.RibbonButton btnNewDrug;
		private System.Windows.Forms.RibbonButton btnViewDrug;
		private System.Windows.Forms.RibbonButton btnReceiveInventory;
		private System.Windows.Forms.RibbonButton btnAdjustInventory;
		private System.Windows.Forms.RibbonButton btnNewSupplier;
		private System.Windows.Forms.RibbonButton btnViewSupplier;
		private System.Windows.Forms.RibbonComboBox ribbonComboBox2;
		private System.Windows.Forms.RibbonButton btnHomeDrugReturns;
		private System.Windows.Forms.RibbonButton btnDispenseDrugs;
		private System.Windows.Forms.RibbonButton btnNewUser;
		private System.Windows.Forms.RibbonButton btnViewUser;
		private System.Windows.Forms.StatusStrip stpMainForm;
		private System.Windows.Forms.RibbonButton btnShowHome;
		private System.Windows.Forms.Panel pnlStatistics;
		private System.Windows.Forms.GroupBox gbxStatistics;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.RibbonButton btnPatientsDispenseDrugs;
		private System.Windows.Forms.RibbonPanel ribbonPanel3;
		private System.Windows.Forms.RibbonButton btnHomeReceiveInventory;
		private System.Windows.Forms.RibbonButton btnHomeAdjustInventory;
		private System.Windows.Forms.RibbonButton btnHomeInventoryReturns;
		private System.Windows.Forms.RibbonButton btnInvetoryReturns;
		private System.Windows.Forms.RibbonTab rtbReports;
		private System.Windows.Forms.RibbonPanel ribbonPanel1;
		private System.Windows.Forms.RibbonPanel ribbonPanel2;
		private System.Windows.Forms.RibbonPanel ribbonPanel7;
		private System.Windows.Forms.RibbonPanel ribbonPanel14;
		private System.Windows.Forms.RibbonButton btnNewPractitioner;
		private System.Windows.Forms.RibbonButton btnViewPractitioner;
		private System.Windows.Forms.RibbonButton btnResetPassword;
		private System.Windows.Forms.RibbonPanel ribbonPanel9;
		private System.Windows.Forms.RibbonButton btnHomeShowHome;
		private System.Windows.Forms.RibbonButton btnHomeChangeTheme;
		private System.Windows.Forms.RibbonButton btnHomeChangePassword;
		private System.Windows.Forms.RibbonButton btnChangePassword;
		private System.Windows.Forms.RibbonButton btnThemeOffice2013;
		private System.Windows.Forms.RibbonButton btnThemeOffice2010;
		private System.Windows.Forms.RibbonButton btnThemeOffice2007;
		private System.Windows.Forms.RibbonButton btnChangeTheme;
		private System.Windows.Forms.RibbonButton btnOffice2007Theme;
		private System.Windows.Forms.RibbonButton btnOffice2010Theme;
		private System.Windows.Forms.RibbonButton btnOffice2013Theme;
		private System.Windows.Forms.Label lblLoggedOnAs;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Timer tmrCurrentTime;
		private System.Windows.Forms.Label lblLoggedOnUser;
		private System.Windows.Forms.Label lblTime;
		private System.Windows.Forms.Label lblStatsSuppliers;
		private System.Windows.Forms.Label lblStatsDrugs;
		private System.Windows.Forms.Label lblStatsPatients;
		private System.Windows.Forms.Label lblStatsReorderDrugs;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox gbxReorderList;
		private System.Windows.Forms.RibbonButton btnRptViewAllPatients;
		private System.Windows.Forms.RibbonButton btnRptViewDrugs;
		private System.Windows.Forms.RibbonButton btnRptViewBatches;
		private System.Windows.Forms.RibbonButton btnRptViewSuppliers;
		private System.Windows.Forms.RibbonPanel ribbonPanel12;
		private System.Windows.Forms.RibbonButton btnRptViewDispensaryTxn;
		private System.Windows.Forms.RibbonButton btnRptViewInventory;
		private System.Windows.Forms.Label lblStatsPractitioners;
		private System.Windows.Forms.Label label3;
	}
}
