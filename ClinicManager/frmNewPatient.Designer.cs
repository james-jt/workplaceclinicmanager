using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmNewPatient
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSaveAndNew = new System.Windows.Forms.Button();
			this.btnSaveAndClose = new System.Windows.Forms.Button();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.dtpPatientDOB = new System.Windows.Forms.DateTimePicker();
			this.lblPatientName = new System.Windows.Forms.Label();
			this.lblPatientSurname = new System.Windows.Forms.Label();
			this.lblEmpNo = new System.Windows.Forms.Label();
			this.lblDpt = new System.Windows.Forms.Label();
			this.lblPatientDOB = new System.Windows.Forms.Label();
			this.txtPatientName = new System.Windows.Forms.TextBox();
			this.txtEmpNo = new System.Windows.Forms.TextBox();
			this.txtPatientSurname = new System.Windows.Forms.TextBox();
			this.txtDpt = new System.Windows.Forms.TextBox();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdPatients = new System.Windows.Forms.DataGridView();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdPatients).BeginInit();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSaveAndNew);
			this.gbxButtons.Controls.Add(this.btnSaveAndClose);
			this.gbxButtons.Controls.Add(this.btnReset);
			this.gbxButtons.Controls.Add(this.btnCancel);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSaveAndNew
			// 
			this.btnSaveAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderSize = 0;
			this.btnSaveAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndNew.Location = new System.Drawing.Point(442, 22);
			this.btnSaveAndNew.Name = "btnSaveAndNew";
			this.btnSaveAndNew.Size = new System.Drawing.Size(94, 33);
			this.btnSaveAndNew.TabIndex = 6;
			this.btnSaveAndNew.Text = "Save and New";
			this.btnSaveAndNew.UseVisualStyleBackColor = false;
			this.btnSaveAndNew.Click += new System.EventHandler(this.btnSaveAndNew_Click);
			// 
			// btnSaveAndClose
			// 
			this.btnSaveAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderSize = 0;
			this.btnSaveAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndClose.Location = new System.Drawing.Point(556, 22);
			this.btnSaveAndClose.Name = "btnSaveAndClose";
			this.btnSaveAndClose.Size = new System.Drawing.Size(94, 33);
			this.btnSaveAndClose.TabIndex = 7;
			this.btnSaveAndClose.Text = "Save and Close";
			this.btnSaveAndClose.UseVisualStyleBackColor = false;
			this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
			// 
			// btnReset
			// 
			this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnReset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderSize = 0;
			this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnReset.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnReset.Location = new System.Drawing.Point(670, 22);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(94, 33);
			this.btnReset.TabIndex = 8;
			this.btnReset.Text = "Reset";
			this.btnReset.UseVisualStyleBackColor = false;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.FlatAppearance.BorderSize = 0;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnCancel.Location = new System.Drawing.Point(784, 22);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(94, 33);
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.dtpPatientDOB);
			this.gbxFields.Controls.Add(this.lblPatientName);
			this.gbxFields.Controls.Add(this.lblPatientSurname);
			this.gbxFields.Controls.Add(this.lblEmpNo);
			this.gbxFields.Controls.Add(this.lblDpt);
			this.gbxFields.Controls.Add(this.lblPatientDOB);
			this.gbxFields.Controls.Add(this.txtPatientName);
			this.gbxFields.Controls.Add(this.txtEmpNo);
			this.gbxFields.Controls.Add(this.txtPatientSurname);
			this.gbxFields.Controls.Add(this.txtDpt);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 182);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// dtpPatientDOB
			// 
			this.dtpPatientDOB.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.dtpPatientDOB.CalendarForeColor = System.Drawing.Color.DarkSlateGray;
			this.dtpPatientDOB.CalendarMonthBackground = System.Drawing.SystemColors.GradientActiveCaption;
			this.dtpPatientDOB.CalendarTitleBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.dtpPatientDOB.CalendarTitleForeColor = System.Drawing.Color.SteelBlue;
			this.dtpPatientDOB.Location = new System.Drawing.Point(612, 140);
			this.dtpPatientDOB.MaxDate = new System.DateTime(2001, 9, 24, 0, 0, 0, 0);
			this.dtpPatientDOB.MinDate = new System.DateTime(1936, 9, 24, 0, 0, 0, 0);
			this.dtpPatientDOB.Name = "dtpPatientDOB";
			this.dtpPatientDOB.Size = new System.Drawing.Size(266, 23);
			this.dtpPatientDOB.TabIndex = 5;
			this.dtpPatientDOB.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
			// 
			// lblPatientName
			// 
			this.lblPatientName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPatientName.AutoSize = true;
			this.lblPatientName.Location = new System.Drawing.Point(524, 39);
			this.lblPatientName.Name = "lblPatientName";
			this.lblPatientName.Size = new System.Drawing.Size(82, 15);
			this.lblPatientName.TabIndex = 0;
			this.lblPatientName.Text = "Patient Name:";
			// 
			// lblPatientSurname
			// 
			this.lblPatientSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPatientSurname.AutoSize = true;
			this.lblPatientSurname.Location = new System.Drawing.Point(509, 91);
			this.lblPatientSurname.Name = "lblPatientSurname";
			this.lblPatientSurname.Size = new System.Drawing.Size(97, 15);
			this.lblPatientSurname.TabIndex = 0;
			this.lblPatientSurname.Text = "Patient Surname:";
			// 
			// lblEmpNo
			// 
			this.lblEmpNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmpNo.AutoSize = true;
			this.lblEmpNo.Location = new System.Drawing.Point(40, 39);
			this.lblEmpNo.Name = "lblEmpNo";
			this.lblEmpNo.Size = new System.Drawing.Size(84, 15);
			this.lblEmpNo.TabIndex = 0;
			this.lblEmpNo.Text = "Employee No.:";
			// 
			// lblDpt
			// 
			this.lblDpt.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblDpt.AutoSize = true;
			this.lblDpt.Location = new System.Drawing.Point(51, 91);
			this.lblDpt.Name = "lblDpt";
			this.lblDpt.Size = new System.Drawing.Size(73, 15);
			this.lblDpt.TabIndex = 0;
			this.lblDpt.Text = "Department:";
			// 
			// lblPatientDOB
			// 
			this.lblPatientDOB.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPatientDOB.AutoSize = true;
			this.lblPatientDOB.Location = new System.Drawing.Point(532, 145);
			this.lblPatientDOB.Name = "lblPatientDOB";
			this.lblPatientDOB.Size = new System.Drawing.Size(74, 15);
			this.lblPatientDOB.TabIndex = 0;
			this.lblPatientDOB.Text = "Patient DOB:";
			// 
			// txtPatientName
			// 
			this.txtPatientName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPatientName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPatientName.Location = new System.Drawing.Point(612, 34);
			this.txtPatientName.Name = "txtPatientName";
			this.txtPatientName.Size = new System.Drawing.Size(266, 23);
			this.txtPatientName.TabIndex = 3;
			// 
			// txtEmpNo
			// 
			this.txtEmpNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtEmpNo.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtEmpNo.Location = new System.Drawing.Point(130, 34);
			this.txtEmpNo.Name = "txtEmpNo";
			this.txtEmpNo.Size = new System.Drawing.Size(266, 23);
			this.txtEmpNo.TabIndex = 1;
			// 
			// txtPatientSurname
			// 
			this.txtPatientSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPatientSurname.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPatientSurname.Location = new System.Drawing.Point(612, 86);
			this.txtPatientSurname.Name = "txtPatientSurname";
			this.txtPatientSurname.Size = new System.Drawing.Size(266, 23);
			this.txtPatientSurname.TabIndex = 4;
			// 
			// txtDpt
			// 
			this.txtDpt.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtDpt.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtDpt.Location = new System.Drawing.Point(130, 86);
			this.txtDpt.Name = "txtDpt";
			this.txtDpt.Size = new System.Drawing.Size(266, 23);
			this.txtDpt.TabIndex = 2;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdPatients);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 250);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 179);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			// 
			// dgdPatients
			// 
			this.dgdPatients.AllowUserToAddRows = false;
			this.dgdPatients.AllowUserToDeleteRows = false;
			this.dgdPatients.AllowUserToOrderColumns = true;
			this.dgdPatients.BackgroundColor = System.Drawing.Color.White;
			this.dgdPatients.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdPatients.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdPatients.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdPatients.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdPatients.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdPatients.Location = new System.Drawing.Point(3, 19);
			this.dgdPatients.Name = "dgdPatients";
			this.dgdPatients.ReadOnly = true;
			this.dgdPatients.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdPatients.RowHeadersVisible = false;
			this.dgdPatients.Size = new System.Drawing.Size(880, 157);
			this.dgdPatients.TabIndex = 0;
			this.dgdPatients.TabStop = false;
			// 
			// frmNewPatient
			// 
			this.AcceptButton = this.btnSaveAndNew;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmNewPatient";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "PatientInput";
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdPatients).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnSaveAndNew;
		public System.Windows.Forms.Button btnSaveAndClose;
		public System.Windows.Forms.Button btnReset;
		public System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		public System.Windows.Forms.DateTimePicker dtpPatientDOB;
		private System.Windows.Forms.Label lblPatientName;
		private System.Windows.Forms.Label lblPatientSurname;
		private System.Windows.Forms.Label lblEmpNo;
		private System.Windows.Forms.Label lblDpt;
		private System.Windows.Forms.Label lblPatientDOB;
		public System.Windows.Forms.TextBox txtPatientName;
		public System.Windows.Forms.TextBox txtEmpNo;
		public System.Windows.Forms.TextBox txtPatientSurname;
		public System.Windows.Forms.TextBox txtDpt;
		public System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdPatients;
	}
}
