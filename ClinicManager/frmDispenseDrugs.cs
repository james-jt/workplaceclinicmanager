using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmDispenseDrugs : Form, IUpdatesDashboard
	{
		public DataSet ds;
		public DataTable tableAddedDrugs;
		private DataTable tablePatients;

		private DataTable tablePractitioners;
		public frmDispenseDrugs(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			dgdAddedDrugs.DataSource = ds;
			dgdAddedDrugs.DataMember = ds.Tables["Drug"].ToString();
			EntityChange += handler;
		}


		#region "Common Methods"

		private void ClearForm()
		{
			cmbEmpNo.Text = "";
			txtEmpName.Text = "";
			txtEmpDOB.Text = "";
			txtDpt.Text = "";
			cmbPractitionerNo.Text = "";
			txtPractitionerName.Text = "";
			tableAddedDrugs.Rows.Clear();
			dgdAddedDrugs.Update();

			cmbEmpNo.Focus();
		}

		private void InitializeDataTables()
		{
			ds = new DataSet();

			tableAddedDrugs = new DataTable("Drug");
			DataColumn drugCodeCol = tableAddedDrugs.Columns.Add("Drug Code", typeof(string));
			drugCodeCol.AllowDBNull = false;
			DataColumn drugNameCol = tableAddedDrugs.Columns.Add("Drug Name", typeof(string));
			drugNameCol.AllowDBNull = false;
			DataColumn batchNoCol = tableAddedDrugs.Columns.Add("Batch No.", typeof(string));
			batchNoCol.AllowDBNull = false;
			DataColumn quantityCol = tableAddedDrugs.Columns.Add("Quantity", typeof(Int32));
			quantityCol.AllowDBNull = false;
			DataColumn noteCol = tableAddedDrugs.Columns.Add("Notes", typeof(string));
			ds.Tables.Add(tableAddedDrugs);

			tablePatients = (new DataAccess().ReadAllPatientRecords()).Tables[0];
			if (tablePatients != null) {
				this.cmbEmpNo.DisplayMember = "EMPLOYEEID";
				this.cmbEmpNo.ValueMember = "EMPLOYEEID";
				this.cmbEmpNo.DataSource = tablePatients;
			}

			tablePractitioners = (new DataAccess().ReadAllPractitionerRecords()).Tables[0];
			if (tablePractitioners != null) {
				this.cmbPractitionerNo.DisplayMember = "PRACTITIONERCODE";
				this.cmbPractitionerNo.ValueMember = "PRACTITIONERCODE";
				this.cmbPractitionerNo.DataSource = tablePractitioners;
			}
		}

		public event EventHandler EntityChange;

		#endregion


		private void cmbEmpNo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbEmpNo.Text)) {
				return;
			}
			string query = string.Format("EMPLOYEEID = '{0}'", cmbEmpNo.SelectedValue);
			DataRow[] selectedEmployee = tablePatients.Select(query);
			this.txtEmpName.Text = string.Format("{0}, {1}", selectedEmployee[0]["SURNAME"].ToString(), selectedEmployee[0]["NAME"].ToString());
			this.txtEmpDOB.Text = (selectedEmployee[0]["DATEOFBIRTH"].ToString()).Substring(0, 10);
			this.txtDpt.Text = selectedEmployee[0]["DEPARTMENT"].ToString();
		}

		private void btnAddDrugs_Click(object sender, EventArgs e)
		{
			frmAddDrugs form = new frmAddDrugs(ref this.tableAddedDrugs, ref this.dgdAddedDrugs);
			form.ShowDialog();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbEmpNo.Text)) {
				MessageBox.Show("Please enter Employee Number.", "Missing Input");
				return;
			}
			if (tableAddedDrugs.Rows.Count < 1) {
				MessageBox.Show("Please add at least 1 drug.", "Missing Input");
				return;
			}
			if (!(new DataAccess().SaveDispensaryTransaction(this))) {
				return;
			}
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.Close();
		}

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbEmpNo.Text)) {
				MessageBox.Show("Please enter Employee Number.", "Missing Input");
				return;
			}
			if (tableAddedDrugs.Rows.Count < 1) {
				MessageBox.Show("Please add at least 1 drug.", "Missing Input");
				return;
			}
			if (!(new DataAccess().SaveDispensaryTransaction(this))) {
				return;
			}
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.ClearForm();
		}
	}
}
