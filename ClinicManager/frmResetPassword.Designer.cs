using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmResetPassword
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbUsername = new System.Windows.Forms.ComboBox();
			this.lblSurname = new System.Windows.Forms.Label();
			this.txtSurname = new System.Windows.Forms.TextBox();
			this.lblName = new System.Windows.Forms.Label();
			this.lblUserType = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.TextBox();
			this.btnResetPassword = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cmbUsername
			// 
			this.cmbUsername.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbUsername.DisplayMember = "Text";
			this.cmbUsername.FormattingEnabled = true;
			this.cmbUsername.Location = new System.Drawing.Point(82, 12);
			this.cmbUsername.Name = "cmbUsername";
			this.cmbUsername.Size = new System.Drawing.Size(203, 23);
			this.cmbUsername.TabIndex = 1;
			this.cmbUsername.ValueMember = "Text";
			// 
			// lblSurname
			// 
			this.lblSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblSurname.AutoSize = true;
			this.lblSurname.Location = new System.Drawing.Point(15, 94);
			this.lblSurname.Name = "lblSurname";
			this.lblSurname.Size = new System.Drawing.Size(57, 15);
			this.lblSurname.TabIndex = 35;
			this.lblSurname.Text = "Surname:";
			// 
			// txtSurname
			// 
			this.txtSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtSurname.Enabled = false;
			this.txtSurname.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtSurname.Location = new System.Drawing.Point(82, 90);
			this.txtSurname.Name = "txtSurname";
			this.txtSurname.Size = new System.Drawing.Size(203, 23);
			this.txtSurname.TabIndex = 34;
			this.txtSurname.TabStop = false;
			// 
			// lblName
			// 
			this.lblName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(31, 56);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(42, 15);
			this.lblName.TabIndex = 31;
			this.lblName.Text = "Name:";
			// 
			// lblUserType
			// 
			this.lblUserType.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblUserType.AutoSize = true;
			this.lblUserType.Location = new System.Drawing.Point(8, 18);
			this.lblUserType.Name = "lblUserType";
			this.lblUserType.Size = new System.Drawing.Size(63, 15);
			this.lblUserType.TabIndex = 32;
			this.lblUserType.Text = "Username:";
			// 
			// txtName
			// 
			this.txtName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtName.Enabled = false;
			this.txtName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtName.Location = new System.Drawing.Point(82, 51);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(203, 23);
			this.txtName.TabIndex = 2;
			this.txtName.TabStop = false;
			// 
			// btnResetPassword
			// 
			this.btnResetPassword.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnResetPassword.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnResetPassword.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnResetPassword.FlatAppearance.BorderSize = 0;
			this.btnResetPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnResetPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnResetPassword.Location = new System.Drawing.Point(11, 126);
			this.btnResetPassword.Name = "btnResetPassword";
			this.btnResetPassword.Size = new System.Drawing.Size(133, 31);
			this.btnResetPassword.TabIndex = 2;
			this.btnResetPassword.Text = "Reset Password";
			this.btnResetPassword.UseVisualStyleBackColor = false;
			this.btnResetPassword.Click += new System.EventHandler(this.btnResetPassword_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.FlatAppearance.BorderSize = 0;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnCancel.Location = new System.Drawing.Point(152, 126);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(133, 31);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// frmResetPassword
			// 
			this.AcceptButton = this.btnResetPassword;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(297, 167);
			this.Controls.Add(this.btnResetPassword);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.cmbUsername);
			this.Controls.Add(this.lblSurname);
			this.Controls.Add(this.txtSurname);
			this.Controls.Add(this.lblName);
			this.Controls.Add(this.lblUserType);
			this.Controls.Add(this.txtName);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "frmResetPassword";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Password Reset";
			this.Load += new System.EventHandler(this.frmResetPassword_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		public System.Windows.Forms.ComboBox cmbUsername;
		private System.Windows.Forms.Label lblSurname;
		public System.Windows.Forms.TextBox txtSurname;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Label lblUserType;
		public System.Windows.Forms.TextBox txtName;
		public System.Windows.Forms.Button btnResetPassword;
		public System.Windows.Forms.Button btnCancel;
	}
}
