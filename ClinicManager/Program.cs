using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	sealed class Program
	{
		private Program()
		{
		}
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// 
		public static bool ReloadLogInForm = false;
		public static string userName = string.Empty;

		public static string userType = string.Empty;
		[STAThread()]
		static internal void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			do {
				Application.Run(new frmLogIn());
				ReloadLogInForm = false;
				if (userName != string.Empty) {
					Application.Run(new frmMain());
				}
			} while (ReloadLogInForm);
		}
	}
}
