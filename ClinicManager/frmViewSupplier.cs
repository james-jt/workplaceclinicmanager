using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{


	public partial class frmViewSupplier : Form, IUpdatesDashboard
	{

		private DataTable tableSupplierStatus;
		public frmViewSupplier(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			FillDataGrid();
			ResetFormControls();

			EntityChange += handler;
		}

		#region "Common Methods"

		private void InitializeDataTables()
		{
			tableSupplierStatus = (new DataAccess().ReadSupplierStatus()).Tables[0];
			if (tableSupplierStatus != null) {
				this.cmbSupplierStatus.DisplayMember = "STATUSCODE";
				this.cmbSupplierStatus.ValueMember = "STATUSCODE";
				this.cmbSupplierStatus.DataSource = tableSupplierStatus;
			}
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllSupplierRecords();
			dgdSuppliers.DataSource = ds;
			dgdSuppliers.DataMember = ds.Tables[0].ToString();
		}

		private void SetFormControls()
		{
			txtSupplierCode.Enabled = true;
			txtSupplierName.Enabled = false;
			cmbSupplierStatus.Enabled = false;
			txtContactPerson.Enabled = false;
			txtEmailAddress.Enabled = false;
			txtTelNo.Enabled = false;
			txtPhysicalAddress.Enabled = false;
			txtStreet.Enabled = false;
			cmbCountry.Enabled = false;
			cmbCity.Enabled = false;

			btnEdit.Enabled = true;
			btnDelete.Enabled = true;
			btnSave.Enabled = false;

			txtSupplierCode.SelectAll();
			txtSupplierCode.Focus();

			btnView.Text = "View";
		}

		private void ResetFormControls()
		{
			txtSupplierCode.SelectAll();
			txtSupplierCode.Focus();
			txtSupplierCode.Text = "";
			txtSupplierName.Text = "";
			cmbSupplierStatus.Text = "";
			txtContactPerson.Text = "";
			txtEmailAddress.Text = "";
			txtTelNo.Text = "";
			txtPhysicalAddress.Text = "";
			txtStreet.Text = "";
			cmbCountry.Text = Convert.ToString(cmbCountry.Items[cmbCountry.Items.Count - 1]);
			cmbCity.Text = Convert.ToString(cmbCity.Items[0]);
		}

		public event EventHandler EntityChange;

		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnView_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtSupplierCode.Text)) {
				MessageBox.Show("Please enter Supplier Code.", "Missing Input");
				ResetFormControls();
				return;
			}

			DataSet ds = new DataAccess().ReadSupplierRecord(this);

			if (ds.Tables[0].Rows.Count > 0) {
				txtSupplierName.Text = ds.Tables[0].Rows[0]["NAME"].ToString();
				cmbSupplierStatus.Text = ds.Tables[0].Rows[0]["SUPPLIERSTATUS"].ToString();
				txtContactPerson.Text = ds.Tables[0].Rows[0]["CONTACTPERSON"].ToString();
				txtTelNo.Text = ds.Tables[0].Rows[0]["TELNO"].ToString();
				txtEmailAddress.Text = ds.Tables[0].Rows[0]["EMAILADDRESS"].ToString();
				txtPhysicalAddress.Text = ds.Tables[0].Rows[0]["ADDRESS"].ToString();
				txtStreet.Text = ds.Tables[0].Rows[0]["STREET"].ToString();
				cmbCity.SelectedItem = ds.Tables[0].Rows[0]["CONTACTPERSON"].ToString();
				cmbCountry.SelectedItem = ds.Tables[0].Rows[0]["TELNO"].ToString();

				SetFormControls();
			} else {
				MessageBox.Show("The Supplier Code you entered does not exist in the system.", "Invalid Input");
				ResetFormControls();
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			txtSupplierCode.Enabled = false;
			txtSupplierName.Enabled = true;
			cmbSupplierStatus.Enabled = true;
			txtContactPerson.Enabled = true;
			txtEmailAddress.Enabled = true;
			txtTelNo.Enabled = true;
			txtPhysicalAddress.Enabled = true;
			txtStreet.Enabled = true;
			cmbCountry.Enabled = true;
			cmbCity.Enabled = true;

			btnEdit.Enabled = false;
			btnDelete.Enabled = false;
			btnSave.Enabled = true;

			btnView.Text = "Reset";
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to completely remove this record from the system?", "Confirm Delete Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
				DataAccess dataAccess = new DataAccess();
				dataAccess.DeleteSupplierRecord(this);
				FillDataGrid();
				ResetFormControls();
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtSupplierCode.Text)) {
				MessageBox.Show("Please enter the Supplier Code.", "Missing Input");
				this.txtSupplierCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtSupplierName.Text)) {
				MessageBox.Show("Please enter the Supplier's Name.", "Missing Input");
				this.txtSupplierName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.cmbSupplierStatus.Text)) {
				MessageBox.Show("Please enter the Supplier's status.", "Missing Input");
				this.cmbSupplierStatus.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtContactPerson.Text)) {
				MessageBox.Show("Please enter the Contact Person for the Supplier.", "Missing Input");
				this.txtContactPerson.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtTelNo.Text)) {
				MessageBox.Show("Please enter the Supplier's telephone number.", "Missing Input");
				this.txtTelNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPhysicalAddress.Text)) {
				MessageBox.Show("Please enter the Supplier's physical address.", "Missing Input");
				this.txtPhysicalAddress.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.UpdateSupplierRecord(this);
			FillDataGrid();
			SetFormControls();
		}

		private void frmViewSupplier_Activated(object sender, EventArgs e)
		{
			this.FillDataGrid();
		}
	}
}
