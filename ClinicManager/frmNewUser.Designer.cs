using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmNewUser
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSaveAndNew = new System.Windows.Forms.Button();
			this.btnSaveAndClose = new System.Windows.Forms.Button();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.cmbUserType = new System.Windows.Forms.ComboBox();
			this.lblName = new System.Windows.Forms.Label();
			this.txtSurname = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.lblNationalIDNo = new System.Windows.Forms.Label();
			this.lblUsername = new System.Windows.Forms.Label();
			this.lblUserType = new System.Windows.Forms.Label();
			this.lblSurname = new System.Windows.Forms.Label();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.txtNationalIDNo = new System.Windows.Forms.TextBox();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdUsers = new System.Windows.Forms.DataGridView();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdUsers).BeginInit();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSaveAndNew);
			this.gbxButtons.Controls.Add(this.btnSaveAndClose);
			this.gbxButtons.Controls.Add(this.btnReset);
			this.gbxButtons.Controls.Add(this.btnCancel);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSaveAndNew
			// 
			this.btnSaveAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderSize = 0;
			this.btnSaveAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndNew.Location = new System.Drawing.Point(442, 22);
			this.btnSaveAndNew.Name = "btnSaveAndNew";
			this.btnSaveAndNew.Size = new System.Drawing.Size(94, 33);
			this.btnSaveAndNew.TabIndex = 6;
			this.btnSaveAndNew.Text = "Save and New";
			this.btnSaveAndNew.UseVisualStyleBackColor = false;
			this.btnSaveAndNew.Click += new System.EventHandler(this.btnSaveAndNew_Click);
			// 
			// btnSaveAndClose
			// 
			this.btnSaveAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderSize = 0;
			this.btnSaveAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndClose.Location = new System.Drawing.Point(556, 22);
			this.btnSaveAndClose.Name = "btnSaveAndClose";
			this.btnSaveAndClose.Size = new System.Drawing.Size(94, 33);
			this.btnSaveAndClose.TabIndex = 7;
			this.btnSaveAndClose.Text = "Save and Close";
			this.btnSaveAndClose.UseVisualStyleBackColor = false;
			this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
			// 
			// btnReset
			// 
			this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnReset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderSize = 0;
			this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnReset.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnReset.Location = new System.Drawing.Point(670, 22);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(94, 33);
			this.btnReset.TabIndex = 8;
			this.btnReset.Text = "Reset";
			this.btnReset.UseVisualStyleBackColor = false;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.FlatAppearance.BorderSize = 0;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnCancel.Location = new System.Drawing.Point(784, 22);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(94, 33);
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.cmbUserType);
			this.gbxFields.Controls.Add(this.lblName);
			this.gbxFields.Controls.Add(this.txtSurname);
			this.gbxFields.Controls.Add(this.txtName);
			this.gbxFields.Controls.Add(this.lblPassword);
			this.gbxFields.Controls.Add(this.lblNationalIDNo);
			this.gbxFields.Controls.Add(this.lblUsername);
			this.gbxFields.Controls.Add(this.lblUserType);
			this.gbxFields.Controls.Add(this.lblSurname);
			this.gbxFields.Controls.Add(this.txtPassword);
			this.gbxFields.Controls.Add(this.txtUsername);
			this.gbxFields.Controls.Add(this.txtNationalIDNo);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 182);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// cmbUserType
			// 
			this.cmbUserType.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbUserType.DisplayMember = "Text";
			this.cmbUserType.FormattingEnabled = true;
			this.cmbUserType.Items.AddRange(new object[] {
				"admin",
				"standard"
			});
			this.cmbUserType.Location = new System.Drawing.Point(130, 86);
			this.cmbUserType.Name = "cmbUserType";
			this.cmbUserType.Size = new System.Drawing.Size(266, 23);
			this.cmbUserType.TabIndex = 2;
			this.cmbUserType.Text = "standard";
			this.cmbUserType.ValueMember = "Text";
			// 
			// lblName
			// 
			this.lblName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(83, 145);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(42, 15);
			this.lblName.TabIndex = 0;
			this.lblName.Text = "Name:";
			// 
			// txtSurname
			// 
			this.txtSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtSurname.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtSurname.Location = new System.Drawing.Point(612, 140);
			this.txtSurname.Name = "txtSurname";
			this.txtSurname.Size = new System.Drawing.Size(266, 23);
			this.txtSurname.TabIndex = 5;
			// 
			// txtName
			// 
			this.txtName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtName.Location = new System.Drawing.Point(130, 140);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(266, 23);
			this.txtName.TabIndex = 4;
			// 
			// lblPassword
			// 
			this.lblPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(547, 39);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(60, 15);
			this.lblPassword.TabIndex = 0;
			this.lblPassword.Text = "Password:";
			// 
			// lblNationalIDNo
			// 
			this.lblNationalIDNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNationalIDNo.AutoSize = true;
			this.lblNationalIDNo.Location = new System.Drawing.Point(519, 91);
			this.lblNationalIDNo.Name = "lblNationalIDNo";
			this.lblNationalIDNo.Size = new System.Drawing.Size(88, 15);
			this.lblNationalIDNo.TabIndex = 0;
			this.lblNationalIDNo.Text = "National ID No.";
			// 
			// lblUsername
			// 
			this.lblUsername.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblUsername.AutoSize = true;
			this.lblUsername.Location = new System.Drawing.Point(62, 39);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(63, 15);
			this.lblUsername.TabIndex = 0;
			this.lblUsername.Text = "Username:";
			// 
			// lblUserType
			// 
			this.lblUserType.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblUserType.AutoSize = true;
			this.lblUserType.Location = new System.Drawing.Point(63, 91);
			this.lblUserType.Name = "lblUserType";
			this.lblUserType.Size = new System.Drawing.Size(62, 15);
			this.lblUserType.TabIndex = 0;
			this.lblUserType.Text = "User Type:";
			// 
			// lblSurname
			// 
			this.lblSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblSurname.AutoSize = true;
			this.lblSurname.Location = new System.Drawing.Point(550, 145);
			this.lblSurname.Name = "lblSurname";
			this.lblSurname.Size = new System.Drawing.Size(57, 15);
			this.lblSurname.TabIndex = 0;
			this.lblSurname.Text = "Surname:";
			// 
			// txtPassword
			// 
			this.txtPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPassword.Enabled = false;
			this.txtPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPassword.Location = new System.Drawing.Point(612, 34);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(266, 23);
			this.txtPassword.TabIndex = 15;
			this.txtPassword.TabStop = false;
			this.txtPassword.Text = "default";
			// 
			// txtUsername
			// 
			this.txtUsername.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtUsername.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtUsername.Location = new System.Drawing.Point(130, 34);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(266, 23);
			this.txtUsername.TabIndex = 1;
			// 
			// txtNationalIDNo
			// 
			this.txtNationalIDNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNationalIDNo.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNationalIDNo.Location = new System.Drawing.Point(612, 86);
			this.txtNationalIDNo.Name = "txtNationalIDNo";
			this.txtNationalIDNo.Size = new System.Drawing.Size(266, 23);
			this.txtNationalIDNo.TabIndex = 3;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdUsers);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 250);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 179);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			// 
			// dgdUsers
			// 
			this.dgdUsers.AllowUserToAddRows = false;
			this.dgdUsers.AllowUserToDeleteRows = false;
			this.dgdUsers.AllowUserToOrderColumns = true;
			this.dgdUsers.BackgroundColor = System.Drawing.Color.White;
			this.dgdUsers.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdUsers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdUsers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdUsers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdUsers.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdUsers.Location = new System.Drawing.Point(3, 19);
			this.dgdUsers.Name = "dgdUsers";
			this.dgdUsers.ReadOnly = true;
			this.dgdUsers.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdUsers.RowHeadersVisible = false;
			this.dgdUsers.Size = new System.Drawing.Size(880, 157);
			this.dgdUsers.TabIndex = 0;
			this.dgdUsers.TabStop = false;
			// 
			// frmNewUser
			// 
			this.AcceptButton = this.btnSaveAndNew;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmNewUser";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Drug Input";
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdUsers).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnSaveAndNew;
		public System.Windows.Forms.Button btnSaveAndClose;
		public System.Windows.Forms.Button btnReset;
		public System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.Label lblNationalIDNo;
		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.Label lblUserType;
		private System.Windows.Forms.Label lblSurname;
		public System.Windows.Forms.TextBox txtPassword;
		public System.Windows.Forms.TextBox txtUsername;
		public System.Windows.Forms.TextBox txtNationalIDNo;
		public System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdUsers;
		private System.Windows.Forms.Label lblName;
		public System.Windows.Forms.TextBox txtSurname;
		public System.Windows.Forms.TextBox txtName;
		public System.Windows.Forms.ComboBox cmbUserType;
	}
}
