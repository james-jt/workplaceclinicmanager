using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmViewDrug
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnView = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.lblReorderLevel = new System.Windows.Forms.Label();
			this.txtNotes = new System.Windows.Forms.TextBox();
			this.txtReorderLevel = new System.Windows.Forms.TextBox();
			this.lblDrugName = new System.Windows.Forms.Label();
			this.lblPreferredSupplier = new System.Windows.Forms.Label();
			this.lblDrugCode = new System.Windows.Forms.Label();
			this.lblCategory = new System.Windows.Forms.Label();
			this.lblNotes = new System.Windows.Forms.Label();
			this.txtDrugName = new System.Windows.Forms.TextBox();
			this.txtDrugCode = new System.Windows.Forms.TextBox();
			this.txtCategory = new System.Windows.Forms.TextBox();
			this.dgdDrugs = new System.Windows.Forms.DataGridView();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.cmbPreferredSupplier = new System.Windows.Forms.ComboBox();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdDrugs).BeginInit();
			this.gbxDataGrid.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSave);
			this.gbxButtons.Controls.Add(this.btnDelete);
			this.gbxButtons.Controls.Add(this.btnEdit);
			this.gbxButtons.Controls.Add(this.btnView);
			this.gbxButtons.Controls.Add(this.btnClose);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSave
			// 
			this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSave.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSave.Enabled = false;
			this.btnSave.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSave.FlatAppearance.BorderSize = 0;
			this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSave.Location = new System.Drawing.Point(328, 22);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(94, 33);
			this.btnSave.TabIndex = 7;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = false;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnDelete.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnDelete.Enabled = false;
			this.btnDelete.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnDelete.FlatAppearance.BorderSize = 0;
			this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDelete.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnDelete.Location = new System.Drawing.Point(442, 22);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(94, 33);
			this.btnDelete.TabIndex = 8;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = false;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnEdit.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnEdit.Enabled = false;
			this.btnEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnEdit.FlatAppearance.BorderSize = 0;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnEdit.Location = new System.Drawing.Point(556, 22);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(94, 33);
			this.btnEdit.TabIndex = 9;
			this.btnEdit.Text = "Edit";
			this.btnEdit.UseVisualStyleBackColor = false;
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnView
			// 
			this.btnView.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnView.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnView.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnView.FlatAppearance.BorderSize = 0;
			this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnView.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnView.Location = new System.Drawing.Point(670, 22);
			this.btnView.Name = "btnView";
			this.btnView.Size = new System.Drawing.Size(94, 33);
			this.btnView.TabIndex = 10;
			this.btnView.Text = "View";
			this.btnView.UseVisualStyleBackColor = false;
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(784, 22);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(94, 33);
			this.btnClose.TabIndex = 11;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.cmbPreferredSupplier);
			this.gbxFields.Controls.Add(this.lblReorderLevel);
			this.gbxFields.Controls.Add(this.txtNotes);
			this.gbxFields.Controls.Add(this.txtReorderLevel);
			this.gbxFields.Controls.Add(this.lblDrugName);
			this.gbxFields.Controls.Add(this.lblPreferredSupplier);
			this.gbxFields.Controls.Add(this.lblDrugCode);
			this.gbxFields.Controls.Add(this.lblCategory);
			this.gbxFields.Controls.Add(this.lblNotes);
			this.gbxFields.Controls.Add(this.txtDrugName);
			this.gbxFields.Controls.Add(this.txtDrugCode);
			this.gbxFields.Controls.Add(this.txtCategory);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 182);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// lblReorderLevel
			// 
			this.lblReorderLevel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblReorderLevel.AutoSize = true;
			this.lblReorderLevel.Location = new System.Drawing.Point(38, 145);
			this.lblReorderLevel.Name = "lblReorderLevel";
			this.lblReorderLevel.Size = new System.Drawing.Size(86, 15);
			this.lblReorderLevel.TabIndex = 0;
			this.lblReorderLevel.Text = "Re-order Level:";
			// 
			// txtNotes
			// 
			this.txtNotes.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNotes.Enabled = false;
			this.txtNotes.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNotes.Location = new System.Drawing.Point(612, 140);
			this.txtNotes.Name = "txtNotes";
			this.txtNotes.Size = new System.Drawing.Size(266, 23);
			this.txtNotes.TabIndex = 6;
			// 
			// txtReorderLevel
			// 
			this.txtReorderLevel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtReorderLevel.Enabled = false;
			this.txtReorderLevel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtReorderLevel.Location = new System.Drawing.Point(130, 140);
			this.txtReorderLevel.Name = "txtReorderLevel";
			this.txtReorderLevel.Size = new System.Drawing.Size(266, 23);
			this.txtReorderLevel.TabIndex = 5;
			// 
			// lblDrugName
			// 
			this.lblDrugName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblDrugName.AutoSize = true;
			this.lblDrugName.Location = new System.Drawing.Point(535, 39);
			this.lblDrugName.Name = "lblDrugName";
			this.lblDrugName.Size = new System.Drawing.Size(71, 15);
			this.lblDrugName.TabIndex = 0;
			this.lblDrugName.Text = "Drug Name:";
			// 
			// lblPreferredSupplier
			// 
			this.lblPreferredSupplier.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPreferredSupplier.AutoSize = true;
			this.lblPreferredSupplier.Location = new System.Drawing.Point(502, 91);
			this.lblPreferredSupplier.Name = "lblPreferredSupplier";
			this.lblPreferredSupplier.Size = new System.Drawing.Size(104, 15);
			this.lblPreferredSupplier.TabIndex = 0;
			this.lblPreferredSupplier.Text = "Preferred Supplier:";
			// 
			// lblDrugCode
			// 
			this.lblDrugCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblDrugCode.AutoSize = true;
			this.lblDrugCode.Location = new System.Drawing.Point(57, 39);
			this.lblDrugCode.Name = "lblDrugCode";
			this.lblDrugCode.Size = new System.Drawing.Size(67, 15);
			this.lblDrugCode.TabIndex = 0;
			this.lblDrugCode.Text = "Drug Code:";
			// 
			// lblCategory
			// 
			this.lblCategory.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblCategory.AutoSize = true;
			this.lblCategory.Location = new System.Drawing.Point(66, 91);
			this.lblCategory.Name = "lblCategory";
			this.lblCategory.Size = new System.Drawing.Size(58, 15);
			this.lblCategory.TabIndex = 0;
			this.lblCategory.Text = "Category:";
			// 
			// lblNotes
			// 
			this.lblNotes.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNotes.AutoSize = true;
			this.lblNotes.Location = new System.Drawing.Point(565, 145);
			this.lblNotes.Name = "lblNotes";
			this.lblNotes.Size = new System.Drawing.Size(41, 15);
			this.lblNotes.TabIndex = 0;
			this.lblNotes.Text = "Notes:";
			// 
			// txtDrugName
			// 
			this.txtDrugName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtDrugName.Enabled = false;
			this.txtDrugName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtDrugName.Location = new System.Drawing.Point(612, 34);
			this.txtDrugName.Name = "txtDrugName";
			this.txtDrugName.Size = new System.Drawing.Size(266, 23);
			this.txtDrugName.TabIndex = 2;
			// 
			// txtDrugCode
			// 
			this.txtDrugCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtDrugCode.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtDrugCode.Location = new System.Drawing.Point(130, 34);
			this.txtDrugCode.Name = "txtDrugCode";
			this.txtDrugCode.Size = new System.Drawing.Size(266, 23);
			this.txtDrugCode.TabIndex = 1;
			// 
			// txtCategory
			// 
			this.txtCategory.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtCategory.Enabled = false;
			this.txtCategory.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtCategory.Location = new System.Drawing.Point(130, 86);
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Size = new System.Drawing.Size(266, 23);
			this.txtCategory.TabIndex = 3;
			// 
			// dgdDrugs
			// 
			this.dgdDrugs.AllowUserToAddRows = false;
			this.dgdDrugs.AllowUserToDeleteRows = false;
			this.dgdDrugs.AllowUserToOrderColumns = true;
			this.dgdDrugs.BackgroundColor = System.Drawing.Color.White;
			this.dgdDrugs.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdDrugs.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdDrugs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdDrugs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdDrugs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdDrugs.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdDrugs.Location = new System.Drawing.Point(3, 19);
			this.dgdDrugs.Name = "dgdDrugs";
			this.dgdDrugs.ReadOnly = true;
			this.dgdDrugs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdDrugs.RowHeadersVisible = false;
			this.dgdDrugs.Size = new System.Drawing.Size(880, 157);
			this.dgdDrugs.TabIndex = 0;
			this.dgdDrugs.TabStop = false;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdDrugs);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 250);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 179);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			// 
			// cmbPreferredSupplier
			// 
			this.cmbPreferredSupplier.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbPreferredSupplier.FormattingEnabled = true;
			this.cmbPreferredSupplier.Location = new System.Drawing.Point(612, 88);
			this.cmbPreferredSupplier.Name = "cmbPreferredSupplier";
			this.cmbPreferredSupplier.Size = new System.Drawing.Size(266, 23);
			this.cmbPreferredSupplier.TabIndex = 7;
			// 
			// frmViewDrug
			// 
			this.AcceptButton = this.btnView;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmViewDrug";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Drug Input";
			this.Activated += new System.EventHandler(this.frmViewDrug_Activated);
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdDrugs).EndInit();
			this.gbxDataGrid.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		private System.Windows.Forms.Label lblDrugName;
		private System.Windows.Forms.Label lblPreferredSupplier;
		private System.Windows.Forms.Label lblDrugCode;
		private System.Windows.Forms.Label lblCategory;
		private System.Windows.Forms.Label lblNotes;
		public System.Windows.Forms.TextBox txtDrugName;
		public System.Windows.Forms.TextBox txtDrugCode;
		public System.Windows.Forms.TextBox txtCategory;
		private System.Windows.Forms.Label lblReorderLevel;
		public System.Windows.Forms.TextBox txtNotes;
		public System.Windows.Forms.TextBox txtReorderLevel;
		public System.Windows.Forms.Button btnSave;
		public System.Windows.Forms.Button btnDelete;
		public System.Windows.Forms.Button btnEdit;
		public System.Windows.Forms.Button btnView;
		public System.Windows.Forms.Button btnClose;
		public System.Windows.Forms.DataGridView dgdDrugs;
		public System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.ComboBox cmbPreferredSupplier;
	}
}
