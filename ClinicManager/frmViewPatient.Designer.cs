using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmViewPatient
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnView = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdPatients = new System.Windows.Forms.DataGridView();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.dtpPatientDOB = new System.Windows.Forms.DateTimePicker();
			this.lblPatientName = new System.Windows.Forms.Label();
			this.lblPatientSurname = new System.Windows.Forms.Label();
			this.lblEmpNo = new System.Windows.Forms.Label();
			this.lblDpt = new System.Windows.Forms.Label();
			this.lblPatientDOB = new System.Windows.Forms.Label();
			this.txtPatientName = new System.Windows.Forms.TextBox();
			this.txtEmpNo = new System.Windows.Forms.TextBox();
			this.txtPatientSurname = new System.Windows.Forms.TextBox();
			this.txtDpt = new System.Windows.Forms.TextBox();
			this.gbxButtons.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdPatients).BeginInit();
			this.gbxFields.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 10;
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 9;
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 427);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 14);
			this.pnlBottomPadding.TabIndex = 11;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSave);
			this.gbxButtons.Controls.Add(this.btnDelete);
			this.gbxButtons.Controls.Add(this.btnEdit);
			this.gbxButtons.Controls.Add(this.btnView);
			this.gbxButtons.Controls.Add(this.btnClose);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSave
			// 
			this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSave.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSave.Enabled = false;
			this.btnSave.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSave.FlatAppearance.BorderSize = 0;
			this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSave.Location = new System.Drawing.Point(328, 22);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(94, 33);
			this.btnSave.TabIndex = 6;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = false;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnDelete.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnDelete.Enabled = false;
			this.btnDelete.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnDelete.FlatAppearance.BorderSize = 0;
			this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDelete.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnDelete.Location = new System.Drawing.Point(442, 22);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(94, 33);
			this.btnDelete.TabIndex = 7;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = false;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnEdit.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnEdit.Enabled = false;
			this.btnEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnEdit.FlatAppearance.BorderSize = 0;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnEdit.Location = new System.Drawing.Point(556, 22);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(94, 33);
			this.btnEdit.TabIndex = 8;
			this.btnEdit.Text = "Edit";
			this.btnEdit.UseVisualStyleBackColor = false;
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnView
			// 
			this.btnView.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnView.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnView.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnView.FlatAppearance.BorderSize = 0;
			this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnView.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnView.Location = new System.Drawing.Point(670, 22);
			this.btnView.Name = "btnView";
			this.btnView.Size = new System.Drawing.Size(94, 33);
			this.btnView.TabIndex = 9;
			this.btnView.Text = "View";
			this.btnView.UseVisualStyleBackColor = false;
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(784, 22);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(94, 33);
			this.btnClose.TabIndex = 10;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdPatients);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 248);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 179);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			// 
			// dgdPatients
			// 
			this.dgdPatients.AllowUserToAddRows = false;
			this.dgdPatients.AllowUserToDeleteRows = false;
			this.dgdPatients.AllowUserToOrderColumns = true;
			this.dgdPatients.BackgroundColor = System.Drawing.Color.White;
			this.dgdPatients.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdPatients.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdPatients.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdPatients.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdPatients.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdPatients.Location = new System.Drawing.Point(3, 19);
			this.dgdPatients.Name = "dgdPatients";
			this.dgdPatients.ReadOnly = true;
			this.dgdPatients.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdPatients.RowHeadersVisible = false;
			this.dgdPatients.Size = new System.Drawing.Size(880, 157);
			this.dgdPatients.TabIndex = 0;
			this.dgdPatients.TabStop = false;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.dtpPatientDOB);
			this.gbxFields.Controls.Add(this.lblPatientName);
			this.gbxFields.Controls.Add(this.lblPatientSurname);
			this.gbxFields.Controls.Add(this.lblEmpNo);
			this.gbxFields.Controls.Add(this.lblDpt);
			this.gbxFields.Controls.Add(this.lblPatientDOB);
			this.gbxFields.Controls.Add(this.txtPatientName);
			this.gbxFields.Controls.Add(this.txtEmpNo);
			this.gbxFields.Controls.Add(this.txtPatientSurname);
			this.gbxFields.Controls.Add(this.txtDpt);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 180);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// dtpPatientDOB
			// 
			this.dtpPatientDOB.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.dtpPatientDOB.CalendarForeColor = System.Drawing.Color.DarkSlateGray;
			this.dtpPatientDOB.CalendarMonthBackground = System.Drawing.SystemColors.GradientActiveCaption;
			this.dtpPatientDOB.CalendarTitleBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
			this.dtpPatientDOB.CalendarTitleForeColor = System.Drawing.Color.SteelBlue;
			this.dtpPatientDOB.Enabled = false;
			this.dtpPatientDOB.Location = new System.Drawing.Point(612, 140);
			this.dtpPatientDOB.MaxDate = new System.DateTime(2001, 9, 24, 0, 0, 0, 0);
			this.dtpPatientDOB.MinDate = new System.DateTime(1936, 9, 24, 0, 0, 0, 0);
			this.dtpPatientDOB.Name = "dtpPatientDOB";
			this.dtpPatientDOB.Size = new System.Drawing.Size(266, 23);
			this.dtpPatientDOB.TabIndex = 5;
			this.dtpPatientDOB.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
			// 
			// lblPatientName
			// 
			this.lblPatientName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPatientName.AutoSize = true;
			this.lblPatientName.Location = new System.Drawing.Point(524, 39);
			this.lblPatientName.Name = "lblPatientName";
			this.lblPatientName.Size = new System.Drawing.Size(82, 15);
			this.lblPatientName.TabIndex = 10;
			this.lblPatientName.Text = "Patient Name:";
			// 
			// lblPatientSurname
			// 
			this.lblPatientSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPatientSurname.AutoSize = true;
			this.lblPatientSurname.Location = new System.Drawing.Point(509, 91);
			this.lblPatientSurname.Name = "lblPatientSurname";
			this.lblPatientSurname.Size = new System.Drawing.Size(97, 15);
			this.lblPatientSurname.TabIndex = 11;
			this.lblPatientSurname.Text = "Patient Surname:";
			// 
			// lblEmpNo
			// 
			this.lblEmpNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmpNo.AutoSize = true;
			this.lblEmpNo.Location = new System.Drawing.Point(40, 39);
			this.lblEmpNo.Name = "lblEmpNo";
			this.lblEmpNo.Size = new System.Drawing.Size(84, 15);
			this.lblEmpNo.TabIndex = 12;
			this.lblEmpNo.Text = "Employee No.:";
			// 
			// lblDpt
			// 
			this.lblDpt.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblDpt.AutoSize = true;
			this.lblDpt.Location = new System.Drawing.Point(51, 91);
			this.lblDpt.Name = "lblDpt";
			this.lblDpt.Size = new System.Drawing.Size(73, 15);
			this.lblDpt.TabIndex = 13;
			this.lblDpt.Text = "Department:";
			// 
			// lblPatientDOB
			// 
			this.lblPatientDOB.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPatientDOB.AutoSize = true;
			this.lblPatientDOB.Location = new System.Drawing.Point(532, 145);
			this.lblPatientDOB.Name = "lblPatientDOB";
			this.lblPatientDOB.Size = new System.Drawing.Size(74, 15);
			this.lblPatientDOB.TabIndex = 14;
			this.lblPatientDOB.Text = "Patient DOB:";
			// 
			// txtPatientName
			// 
			this.txtPatientName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPatientName.Enabled = false;
			this.txtPatientName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPatientName.Location = new System.Drawing.Point(612, 34);
			this.txtPatientName.Name = "txtPatientName";
			this.txtPatientName.Size = new System.Drawing.Size(266, 23);
			this.txtPatientName.TabIndex = 3;
			// 
			// txtEmpNo
			// 
			this.txtEmpNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtEmpNo.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtEmpNo.Location = new System.Drawing.Point(130, 34);
			this.txtEmpNo.Name = "txtEmpNo";
			this.txtEmpNo.Size = new System.Drawing.Size(266, 23);
			this.txtEmpNo.TabIndex = 1;
			// 
			// txtPatientSurname
			// 
			this.txtPatientSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPatientSurname.Enabled = false;
			this.txtPatientSurname.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPatientSurname.Location = new System.Drawing.Point(612, 86);
			this.txtPatientSurname.Name = "txtPatientSurname";
			this.txtPatientSurname.Size = new System.Drawing.Size(266, 23);
			this.txtPatientSurname.TabIndex = 4;
			// 
			// txtDpt
			// 
			this.txtDpt.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtDpt.Enabled = false;
			this.txtDpt.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtDpt.Location = new System.Drawing.Point(130, 86);
			this.txtDpt.Name = "txtDpt";
			this.txtDpt.Size = new System.Drawing.Size(266, 23);
			this.txtDpt.TabIndex = 2;
			// 
			// frmViewPatient
			// 
			this.AcceptButton = this.btnView;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmViewPatient";
			this.Text = "frmViewPatient";
			this.Activated += new System.EventHandler(this.frmViewPatient_Activated);
			this.gbxButtons.ResumeLayout(false);
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdPatients).EndInit();
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlRightPadding;
		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnDelete;
		public System.Windows.Forms.Button btnEdit;
		public System.Windows.Forms.Button btnView;
		public System.Windows.Forms.Button btnClose;
		public System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdPatients;
		public System.Windows.Forms.Button btnSave;
		public System.Windows.Forms.GroupBox gbxFields;
		public System.Windows.Forms.DateTimePicker dtpPatientDOB;
		private System.Windows.Forms.Label lblPatientName;
		private System.Windows.Forms.Label lblPatientSurname;
		private System.Windows.Forms.Label lblEmpNo;
		private System.Windows.Forms.Label lblDpt;
		private System.Windows.Forms.Label lblPatientDOB;
		public System.Windows.Forms.TextBox txtPatientName;
		public System.Windows.Forms.TextBox txtEmpNo;
		public System.Windows.Forms.TextBox txtPatientSurname;
		public System.Windows.Forms.TextBox txtDpt;
	}
}
