using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmStockAdjustment
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSaveAndNew = new System.Windows.Forms.Button();
			this.btnSaveAndClose = new System.Windows.Forms.Button();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.txtCountry = new System.Windows.Forms.TextBox();
			this.lblNote = new System.Windows.Forms.Label();
			this.txtNote = new System.Windows.Forms.TextBox();
			this.txtContactPerson = new System.Windows.Forms.TextBox();
			this.lblConactPerson = new System.Windows.Forms.Label();
			this.lblSupplierStatus = new System.Windows.Forms.Label();
			this.txtSupplierStatus = new System.Windows.Forms.TextBox();
			this.txtCity = new System.Windows.Forms.TextBox();
			this.btnSelectBatch = new System.Windows.Forms.Button();
			this.cmbSupplierCode = new System.Windows.Forms.ComboBox();
			this.lblSupplierCode = new System.Windows.Forms.Label();
			this.lblCountry = new System.Windows.Forms.Label();
			this.lblSupplierName = new System.Windows.Forms.Label();
			this.lblCity = new System.Windows.Forms.Label();
			this.txtSupplierName = new System.Windows.Forms.TextBox();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdAddedBatches = new System.Windows.Forms.DataGridView();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdAddedBatches).BeginInit();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSaveAndNew);
			this.gbxButtons.Controls.Add(this.btnSaveAndClose);
			this.gbxButtons.Controls.Add(this.btnReset);
			this.gbxButtons.Controls.Add(this.btnClose);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSaveAndNew
			// 
			this.btnSaveAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderSize = 0;
			this.btnSaveAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndNew.Location = new System.Drawing.Point(438, 22);
			this.btnSaveAndNew.Name = "btnSaveAndNew";
			this.btnSaveAndNew.Size = new System.Drawing.Size(97, 33);
			this.btnSaveAndNew.TabIndex = 4;
			this.btnSaveAndNew.Text = "Save and New";
			this.btnSaveAndNew.UseVisualStyleBackColor = false;
			this.btnSaveAndNew.Click += new System.EventHandler(this.btnSaveAndNew_Click);
			// 
			// btnSaveAndClose
			// 
			this.btnSaveAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderSize = 0;
			this.btnSaveAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndClose.Location = new System.Drawing.Point(552, 22);
			this.btnSaveAndClose.Name = "btnSaveAndClose";
			this.btnSaveAndClose.Size = new System.Drawing.Size(97, 33);
			this.btnSaveAndClose.TabIndex = 5;
			this.btnSaveAndClose.Text = "Save and Close";
			this.btnSaveAndClose.UseVisualStyleBackColor = false;
			this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
			// 
			// btnReset
			// 
			this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnReset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderSize = 0;
			this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnReset.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnReset.Location = new System.Drawing.Point(666, 22);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(97, 33);
			this.btnReset.TabIndex = 6;
			this.btnReset.Text = "Reset";
			this.btnReset.UseVisualStyleBackColor = false;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(780, 22);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(97, 33);
			this.btnClose.TabIndex = 7;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.txtCountry);
			this.gbxFields.Controls.Add(this.lblNote);
			this.gbxFields.Controls.Add(this.txtNote);
			this.gbxFields.Controls.Add(this.txtContactPerson);
			this.gbxFields.Controls.Add(this.lblConactPerson);
			this.gbxFields.Controls.Add(this.lblSupplierStatus);
			this.gbxFields.Controls.Add(this.txtSupplierStatus);
			this.gbxFields.Controls.Add(this.txtCity);
			this.gbxFields.Controls.Add(this.btnSelectBatch);
			this.gbxFields.Controls.Add(this.cmbSupplierCode);
			this.gbxFields.Controls.Add(this.lblSupplierCode);
			this.gbxFields.Controls.Add(this.lblCountry);
			this.gbxFields.Controls.Add(this.lblSupplierName);
			this.gbxFields.Controls.Add(this.lblCity);
			this.gbxFields.Controls.Add(this.txtSupplierName);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 277);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// txtCountry
			// 
			this.txtCountry.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtCountry.Enabled = false;
			this.txtCountry.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtCountry.Location = new System.Drawing.Point(611, 142);
			this.txtCountry.Name = "txtCountry";
			this.txtCountry.Size = new System.Drawing.Size(266, 23);
			this.txtCountry.TabIndex = 32;
			this.txtCountry.TabStop = false;
			// 
			// lblNote
			// 
			this.lblNote.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNote.AutoSize = true;
			this.lblNote.Location = new System.Drawing.Point(571, 212);
			this.lblNote.Name = "lblNote";
			this.lblNote.Size = new System.Drawing.Size(36, 15);
			this.lblNote.TabIndex = 31;
			this.lblNote.Text = "Note:";
			// 
			// txtNote
			// 
			this.txtNote.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNote.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNote.Location = new System.Drawing.Point(611, 209);
			this.txtNote.Multiline = true;
			this.txtNote.Name = "txtNote";
			this.txtNote.Size = new System.Drawing.Size(266, 62);
			this.txtNote.TabIndex = 2;
			// 
			// txtContactPerson
			// 
			this.txtContactPerson.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtContactPerson.Enabled = false;
			this.txtContactPerson.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtContactPerson.Location = new System.Drawing.Point(611, 88);
			this.txtContactPerson.Name = "txtContactPerson";
			this.txtContactPerson.Size = new System.Drawing.Size(266, 23);
			this.txtContactPerson.TabIndex = 29;
			this.txtContactPerson.TabStop = false;
			// 
			// lblConactPerson
			// 
			this.lblConactPerson.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblConactPerson.AutoSize = true;
			this.lblConactPerson.Location = new System.Drawing.Point(515, 93);
			this.lblConactPerson.Name = "lblConactPerson";
			this.lblConactPerson.Size = new System.Drawing.Size(91, 15);
			this.lblConactPerson.TabIndex = 26;
			this.lblConactPerson.Text = "Contact Person:";
			// 
			// lblSupplierStatus
			// 
			this.lblSupplierStatus.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblSupplierStatus.AutoSize = true;
			this.lblSupplierStatus.Location = new System.Drawing.Point(37, 93);
			this.lblSupplierStatus.Name = "lblSupplierStatus";
			this.lblSupplierStatus.Size = new System.Drawing.Size(88, 15);
			this.lblSupplierStatus.TabIndex = 27;
			this.lblSupplierStatus.Text = "Supplier Status:";
			// 
			// txtSupplierStatus
			// 
			this.txtSupplierStatus.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtSupplierStatus.Enabled = false;
			this.txtSupplierStatus.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtSupplierStatus.Location = new System.Drawing.Point(130, 88);
			this.txtSupplierStatus.Name = "txtSupplierStatus";
			this.txtSupplierStatus.Size = new System.Drawing.Size(266, 23);
			this.txtSupplierStatus.TabIndex = 28;
			this.txtSupplierStatus.TabStop = false;
			// 
			// txtCity
			// 
			this.txtCity.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtCity.Enabled = false;
			this.txtCity.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtCity.Location = new System.Drawing.Point(130, 142);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(266, 23);
			this.txtCity.TabIndex = 25;
			this.txtCity.TabStop = false;
			// 
			// btnSelectBatch
			// 
			this.btnSelectBatch.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.btnSelectBatch.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSelectBatch.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSelectBatch.FlatAppearance.BorderSize = 0;
			this.btnSelectBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSelectBatch.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSelectBatch.Location = new System.Drawing.Point(130, 207);
			this.btnSelectBatch.Name = "btnSelectBatch";
			this.btnSelectBatch.Size = new System.Drawing.Size(266, 25);
			this.btnSelectBatch.TabIndex = 3;
			this.btnSelectBatch.Text = "Select Batch";
			this.btnSelectBatch.UseVisualStyleBackColor = false;
			this.btnSelectBatch.Click += new System.EventHandler(this.btnAddBatches_Click);
			// 
			// cmbSupplierCode
			// 
			this.cmbSupplierCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbSupplierCode.FormattingEnabled = true;
			this.cmbSupplierCode.Location = new System.Drawing.Point(611, 34);
			this.cmbSupplierCode.Name = "cmbSupplierCode";
			this.cmbSupplierCode.Size = new System.Drawing.Size(266, 23);
			this.cmbSupplierCode.TabIndex = 1;
			this.cmbSupplierCode.SelectedIndexChanged += new System.EventHandler(this.cmbSupplierCode_SelectedIndexChanged);
			// 
			// lblSupplierCode
			// 
			this.lblSupplierCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblSupplierCode.AutoSize = true;
			this.lblSupplierCode.Location = new System.Drawing.Point(522, 39);
			this.lblSupplierCode.Name = "lblSupplierCode";
			this.lblSupplierCode.Size = new System.Drawing.Size(84, 15);
			this.lblSupplierCode.TabIndex = 10;
			this.lblSupplierCode.Text = "Supplier Code:";
			// 
			// lblCountry
			// 
			this.lblCountry.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblCountry.AutoSize = true;
			this.lblCountry.Location = new System.Drawing.Point(553, 147);
			this.lblCountry.Name = "lblCountry";
			this.lblCountry.Size = new System.Drawing.Size(53, 15);
			this.lblCountry.TabIndex = 11;
			this.lblCountry.Text = "Country:";
			// 
			// lblSupplierName
			// 
			this.lblSupplierName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblSupplierName.AutoSize = true;
			this.lblSupplierName.Location = new System.Drawing.Point(37, 39);
			this.lblSupplierName.Name = "lblSupplierName";
			this.lblSupplierName.Size = new System.Drawing.Size(88, 15);
			this.lblSupplierName.TabIndex = 12;
			this.lblSupplierName.Text = "Supplier Name:";
			// 
			// lblCity
			// 
			this.lblCity.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblCity.AutoSize = true;
			this.lblCity.Location = new System.Drawing.Point(94, 147);
			this.lblCity.Name = "lblCity";
			this.lblCity.Size = new System.Drawing.Size(31, 15);
			this.lblCity.TabIndex = 13;
			this.lblCity.Text = "City:";
			// 
			// txtSupplierName
			// 
			this.txtSupplierName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtSupplierName.Enabled = false;
			this.txtSupplierName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtSupplierName.Location = new System.Drawing.Point(130, 34);
			this.txtSupplierName.Name = "txtSupplierName";
			this.txtSupplierName.Size = new System.Drawing.Size(266, 23);
			this.txtSupplierName.TabIndex = 16;
			this.txtSupplierName.TabStop = false;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdAddedBatches);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 345);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 84);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			this.gbxDataGrid.Text = "Batches";
			// 
			// dgdAddedBatches
			// 
			this.dgdAddedBatches.AllowUserToAddRows = false;
			this.dgdAddedBatches.AllowUserToDeleteRows = false;
			this.dgdAddedBatches.AllowUserToOrderColumns = true;
			this.dgdAddedBatches.BackgroundColor = System.Drawing.Color.White;
			this.dgdAddedBatches.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdAddedBatches.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdAddedBatches.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdAddedBatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdAddedBatches.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdAddedBatches.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdAddedBatches.Location = new System.Drawing.Point(3, 19);
			this.dgdAddedBatches.Name = "dgdAddedBatches";
			this.dgdAddedBatches.ReadOnly = true;
			this.dgdAddedBatches.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdAddedBatches.RowHeadersVisible = false;
			this.dgdAddedBatches.Size = new System.Drawing.Size(880, 62);
			this.dgdAddedBatches.TabIndex = 1;
			this.dgdAddedBatches.TabStop = false;
			// 
			// frmStockAdjustment
			// 
			this.AcceptButton = this.btnSaveAndNew;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmStockAdjustment";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "PatientInput";
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdAddedBatches).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnSaveAndNew;
		public System.Windows.Forms.Button btnSaveAndClose;
		public System.Windows.Forms.Button btnReset;
		public System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		private System.Windows.Forms.Label lblSupplierCode;
		private System.Windows.Forms.Label lblCountry;
		private System.Windows.Forms.Label lblSupplierName;
		private System.Windows.Forms.Label lblCity;
		public System.Windows.Forms.TextBox txtSupplierName;
		public System.Windows.Forms.ComboBox cmbSupplierCode;
		public System.Windows.Forms.Button btnSelectBatch;
		private System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdAddedBatches;
		public System.Windows.Forms.TextBox txtCity;
		public System.Windows.Forms.TextBox txtContactPerson;
		private System.Windows.Forms.Label lblConactPerson;
		private System.Windows.Forms.Label lblSupplierStatus;
		public System.Windows.Forms.TextBox txtSupplierStatus;
		private System.Windows.Forms.Label lblNote;
		public System.Windows.Forms.TextBox txtNote;
		public System.Windows.Forms.TextBox txtCountry;
	}
}
