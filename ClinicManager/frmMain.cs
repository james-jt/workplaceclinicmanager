using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.RibbonHelpers;
namespace Pharmacy
{

	delegate string testVariable();
}
namespace Pharmacy
{
	delegate int CountIt(int end);
}
namespace Pharmacy
{

	public partial class frmMain : Form
	{
		private frmHome homeForm;

		private List<Label> ReorderList;
		public frmMain()
		{
			InitializeComponent();
			homeForm = new frmHome();
			ReorderList = new List<Label>();
			ReorderList.Add(this.lblStatsReorderDrugs);
			homeForm.MdiParent = this;
			homeForm.Dock = DockStyle.Fill;
			this.Text = "Clinic Manager - Home";
			homeForm.Show();
			LoadTheme();
		}

		#region "Events"


		public void UpdateStatistics(object sender, EventArgs e)
		{
			UpdateStatistics();
		}

		#endregion

		#region "Common Methods"

		private void ClearPnlMain()
		{
			foreach (Form f in this.MdiChildren) {
				if (object.ReferenceEquals(f.GetType(), typeof(frmHome))) {
					continue;
				}
				f.Close();
			}
			UpdateStatistics();
		}

		private void SetUpChildForm(Form child, string caption)
		{
			child.MdiParent = this;
			child.Dock = DockStyle.Fill;
			this.Text = "Clinic Manager - " + caption;
			child.Show();
		}

		private void LoadTheme()
		{
			if (this.rbnAppRibbon.OrbStyle == RibbonOrbStyle.Office_2013) {
				this.BackColor = Color.White;
			} else {
				this.BackColor = SystemColors.GradientInactiveCaption;
			}
			this.stpMainForm.BackColor = this.BackColor;
			this.pnlStatistics.BackColor = this.BackColor;
		}

		private void UpdateStatistics()
		{
			lblStatsPatients.Text = new DataAccess().ReadAllPatientRecords().Tables[0].Rows.Count.ToString();
			lblStatsSuppliers.Text = new DataAccess().ReadAllSupplierRecords().Tables[0].Select("SUPPLIERSTATUS = 'ACTV'").Length.ToString();
			lblStatsDrugs.Text = new DataAccess().ReadAllDrugRecords().Tables[0].Rows.Count.ToString();
			lblStatsPractitioners.Text = new DataAccess().ReadAllPractitionerRecords().Tables[0].Select("PRACTITIONERSTATUS = 'ACTV'").Length.ToString();

			DataTable tableDrugs = new DataAccess().ReadAllDrugRecords().Tables[0];
			DataTable drugBatches = new DataAccess().DrugsPastReorderLevel();

			gbxReorderList.Controls.Clear();
			ReorderList.Clear();
			ReorderList.Add(lblStatsReorderDrugs);
			gbxReorderList.Controls.Add(lblStatsReorderDrugs);

			for (int i = 0; i <= drugBatches.Rows.Count - 1; i++) {
				for (int j = 0; j <= tableDrugs.Rows.Count - 1; j++) {
					if (tableDrugs.Rows[j]["DRUGCODE"].ToString() == drugBatches.Rows[i]["DRUGCODE"].ToString()) {
						if (Convert.ToInt32(tableDrugs.Rows[j]["REORDERLEVEL"]) >= Convert.ToInt32(drugBatches.Rows[i][1])) {
							AddToReorderList(tableDrugs.Rows[j]["NAME"].ToString(), drugBatches.Rows[i][1].ToString());
						}
					}
				}
			}
		}

		private void AddToReorderList(string name, string level)
		{
			Label newLabel = new Label();
			newLabel.AutoSize = true;
			newLabel.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			newLabel.ForeColor = Color.Maroon;
			newLabel.Location = new System.Drawing.Point(30, ReorderList[ReorderList.Count - 1].Location.Y + 40);
			newLabel.Name = "lblStatsReorder" + name + (ReorderList.Count - 1).ToString();
			newLabel.Size = new System.Drawing.Size(134, 15);
			newLabel.Text = name + ": " + level;
			ReorderList.Add(newLabel);
			gbxReorderList.Controls.Add(newLabel);
		}

//public void Printing()
//{
//    try
//    {
//        var streamToPrint = new StreamReader(filePath);
//        try
//        {
//            var printFont = new Font("Arial", 10);
//            PrintDocument pd = new PrintDocument();
//            //pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
//            // Print the document.
//            pd.Print();
//        }
//        finally
//        {
//            streamToPrint.Close();
//        }
//    }
//    catch (Exception ex)
//    {
//        MessageBox.Show(ex.Message);
//    }
//}

		#endregion

		private void frmMain_Load(object sender, EventArgs e)
		{
			if (Program.userType != "admin") {
				this.rtbAdmin.Visible = false;
			}
			lblLoggedOnUser.Text = Program.userName.ToLower();
			lblDate.Text = System.DateTime.Now.ToLongDateString();
			lblTime.Text = System.DateTime.Now.ToLongTimeString();
			UpdateStatistics();
		}

		private void tmrCurrentTime_Tick(object sender, EventArgs e)
		{
			lblDate.Text = System.DateTime.Now.ToLongDateString();
			lblTime.Text = System.DateTime.Now.ToLongTimeString();
		}

		private void btnThemeOffice2007_Click(object sender, EventArgs e)
		{
			this.rbnAppRibbon.OrbStyle = RibbonOrbStyle.Office_2007;
			LoadTheme();
		}

		private void btnThemeOffice2010_Click(object sender, EventArgs e)
		{
			this.rbnAppRibbon.OrbStyle = RibbonOrbStyle.Office_2010;
			LoadTheme();
		}

		private void btnThemeOffice2013_Click(object sender, EventArgs e)
		{
			this.rbnAppRibbon.OrbStyle = RibbonOrbStyle.Office_2013;
			LoadTheme();
		}

		private void btnShowHome_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			homeForm.Activate();
			this.Text = "Clinic Manager - Home";
		}

		private void btnNewPatient_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmNewPatient newPatient = new frmNewPatient(UpdateStatistics);
			newPatient.Text = sender.ToString();
			SetUpChildForm(newPatient, btnNewPatient.Text);
		}

		private void btnViewPatient_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmViewPatient viewPatient = new frmViewPatient(UpdateStatistics);
			SetUpChildForm(viewPatient, btnViewPatient.Text);
		}

		private void btnNewDrug_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmNewDrug newDrug = new frmNewDrug(UpdateStatistics);
			SetUpChildForm(newDrug, btnNewDrug.Text);
		}

		private void btnViewDrug_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmViewDrug viewDrug = new frmViewDrug(UpdateStatistics);
			SetUpChildForm(viewDrug, btnViewDrug.Text);
		}

		private void btnNewSupplier_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmNewSupplier newSupplier = new frmNewSupplier(UpdateStatistics);
			SetUpChildForm(newSupplier, btnNewSupplier.Text);
		}

		private void btnViewSupplier_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmViewSupplier viewSupplier = new frmViewSupplier(UpdateStatistics);
			SetUpChildForm(viewSupplier, btnViewSupplier.Text);
		}

		private void btnDispenseDrugs_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmDispenseDrugs newDispenseTransaction = new frmDispenseDrugs(UpdateStatistics);
			SetUpChildForm(newDispenseTransaction, btnDispenseDrugs.Text);
		}

		private void btnNewPractitioner_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmNewPractitioner newPractitioner = new frmNewPractitioner(UpdateStatistics);
			SetUpChildForm(newPractitioner, btnNewPractitioner.Text);
		}

		private void btnViewPractitioner_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmViewPractitioner viewPractitioner = new frmViewPractitioner(UpdateStatistics);
			SetUpChildForm(viewPractitioner, btnViewPractitioner.Text);
		}

		private void btnHomeDrugReturns_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmDispensaryReturns newDispensaryReturnsTransaction = new frmDispensaryReturns(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnHomeDrugReturns.Text);
		}

		private void btnHomeReceiveInventory_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmStockReceipt newDispensaryReturnsTransaction = new frmStockReceipt(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnHomeReceiveInventory.Text);
		}

		private void btnHomeInventoryReturns_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmStockReturns newDispensaryReturnsTransaction = new frmStockReturns(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnHomeInventoryReturns.Text);
		}

		private void btnHomeAdjustInventory_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmStockAdjustment newDispensaryReturnsTransaction = new frmStockAdjustment(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnHomeAdjustInventory.Text);
		}

		private void btnPatientsDispenseDrugs_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmDispenseDrugs newDispenseTransaction = new frmDispenseDrugs(UpdateStatistics);
			SetUpChildForm(newDispenseTransaction, btnPatientsDispenseDrugs.Text);
		}

		private void btnDrugReturns_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmDispensaryReturns newDispensaryReturnsTransaction = new frmDispensaryReturns(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnDrugReturns.Text);
		}

		private void btnReceiveInventory_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmStockReceipt newDispensaryReturnsTransaction = new frmStockReceipt(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnReceiveInventory.Text);
		}

		private void btnAdjustInventory_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmStockAdjustment newDispensaryReturnsTransaction = new frmStockAdjustment(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnAdjustInventory.Text);
		}

		private void btnInvetoryReturns_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmStockReturns newDispensaryReturnsTransaction = new frmStockReturns(UpdateStatistics);
			SetUpChildForm(newDispensaryReturnsTransaction, btnInvetoryReturns.Text);
		}

		private void btnNewUser_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmNewUser newUser = new frmNewUser();
			SetUpChildForm(newUser, btnNewUser.Text);
		}

		private void btnViewUser_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmViewUser viewUser = new frmViewUser();
			SetUpChildForm(viewUser, btnViewUser.Text);
		}

		private void btnResetPassword_Click(object sender, EventArgs e)
		{
			frmResetPassword form = new frmResetPassword();
			form.ShowDialog();
		}

		private void btnHomeShowHome_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			homeForm.Activate();
			this.Text = "Clinic Manager - Home";
		}

		private void btnHomeChangePassword_Click(object sender, EventArgs e)
		{
			frmChangePassword form = new frmChangePassword(false);
			form.ShowDialog();
		}

		private void btnRptViewAllPatients_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmReport report = new frmReport();
			report.lblRptTitle.Text = "All Patients Report";
			SetUpChildForm(report, report.lblRptTitle.Text);

			report.dataSet = new DataAccess().ReadAllPatientRecords();
			report.dgdRptContent.DataSource = report.dataSet;
			report.dgdRptContent.DataMember = report.dataSet.Tables[0].ToString();
			report.FormatDataGridView();
		}

		private void btnRptViewDrugs_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmReport report = new frmReport();
			report.lblRptTitle.Text = "All Drugs Report";
			SetUpChildForm(report, report.lblRptTitle.Text);

			report.dataSet = new DataAccess().ReadAllDrugRecords();
			report.dgdRptContent.DataSource = report.dataSet;
			report.dgdRptContent.DataMember = report.dataSet.Tables[0].ToString();
			report.FormatDataGridView();
		}

		private void btnRptViewBatches_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmReport report = new frmReport();
			report.lblRptTitle.Text = "All Batches Report";
			SetUpChildForm(report, report.lblRptTitle.Text);

			report.dataSet = new DataAccess().ReadAllBatchRecords();
			report.dgdRptContent.DataSource = report.dataSet;
			report.dgdRptContent.DataMember = report.dataSet.Tables[0].ToString();
			report.FormatDataGridView();
		}

		private void btnRptViewSuppliers_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmReport report = new frmReport();
			report.lblRptTitle.Text = "All Suppliers Report";
			SetUpChildForm(report, report.lblRptTitle.Text);

			report.dataSet = new DataAccess().ReadAllSupplierRecords();
			report.dgdRptContent.DataSource = report.dataSet;
			report.dgdRptContent.DataMember = report.dataSet.Tables[0].ToString();
			report.FormatDataGridView();
		}

		private void btnRptViewDispensaryTxn_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmReport report = new frmReport();
			report.lblRptTitle.Text = "All Dispensary Transactions Report";
			SetUpChildForm(report, report.lblRptTitle.Text);

			report.dataSet = new DataAccess().ReadAllDispensaryTransactions();
			report.dgdRptContent.DataSource = report.dataSet;
			report.dgdRptContent.DataMember = report.dataSet.Tables[0].ToString();
			report.FormatDataGridView();
		}

		private void btnRptViewInventory_Click(object sender, EventArgs e)
		{
			ClearPnlMain();
			frmReport report = new frmReport();
			report.lblRptTitle.Text = "All Inventory Transactions Report";
			SetUpChildForm(report, report.lblRptTitle.Text);

			report.dataSet = new DataAccess().ReadAllInventoryTransactions();
			report.dgdRptContent.DataSource = report.dataSet;
			report.dgdRptContent.DataMember = report.dataSet.Tables[0].ToString();
			report.FormatDataGridView();
		}
	}
}
