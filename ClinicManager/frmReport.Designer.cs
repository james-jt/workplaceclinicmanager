using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxRptTitle = new System.Windows.Forms.GroupBox();
			this.lblRptTitle = new System.Windows.Forms.Label();
			this.gbxRptContent = new System.Windows.Forms.GroupBox();
			this.dgdRptContent = new System.Windows.Forms.DataGridView();
			this.btnPrint = new System.Windows.Forms.Button();
			this.btnExport = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.gbxRptTitle.SuspendLayout();
			this.gbxRptContent.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdRptContent).BeginInit();
			this.SuspendLayout();
			// 
			// gbxRptTitle
			// 
			this.gbxRptTitle.Controls.Add(this.btnPrint);
			this.gbxRptTitle.Controls.Add(this.btnExport);
			this.gbxRptTitle.Controls.Add(this.btnClose);
			this.gbxRptTitle.Controls.Add(this.lblRptTitle);
			this.gbxRptTitle.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxRptTitle.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.gbxRptTitle.Location = new System.Drawing.Point(0, 0);
			this.gbxRptTitle.Name = "gbxRptTitle";
			this.gbxRptTitle.Size = new System.Drawing.Size(825, 62);
			this.gbxRptTitle.TabIndex = 0;
			this.gbxRptTitle.TabStop = false;
			// 
			// lblRptTitle
			// 
			this.lblRptTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblRptTitle.AutoSize = true;
			this.lblRptTitle.Font = new System.Drawing.Font("Segoe UI", 18f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblRptTitle.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.lblRptTitle.Location = new System.Drawing.Point(12, 27);
			this.lblRptTitle.Name = "lblRptTitle";
			this.lblRptTitle.Size = new System.Drawing.Size(138, 32);
			this.lblRptTitle.TabIndex = 0;
			this.lblRptTitle.Text = "Report Title";
			// 
			// gbxRptContent
			// 
			this.gbxRptContent.Controls.Add(this.dgdRptContent);
			this.gbxRptContent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxRptContent.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.gbxRptContent.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.gbxRptContent.Location = new System.Drawing.Point(0, 62);
			this.gbxRptContent.Name = "gbxRptContent";
			this.gbxRptContent.Size = new System.Drawing.Size(825, 374);
			this.gbxRptContent.TabIndex = 0;
			this.gbxRptContent.TabStop = false;
			// 
			// dgdRptContent
			// 
			this.dgdRptContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdRptContent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdRptContent.Location = new System.Drawing.Point(3, 19);
			this.dgdRptContent.Name = "dgdRptContent";
			this.dgdRptContent.Size = new System.Drawing.Size(819, 352);
			this.dgdRptContent.TabIndex = 0;
			// 
			// btnPrint
			// 
			this.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnPrint.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnPrint.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnPrint.FlatAppearance.BorderSize = 0;
			this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPrint.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnPrint.Location = new System.Drawing.Point(489, 17);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(97, 33);
			this.btnPrint.TabIndex = 9;
			this.btnPrint.Text = "Print";
			this.btnPrint.UseVisualStyleBackColor = false;
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// btnExport
			// 
			this.btnExport.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnExport.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnExport.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnExport.FlatAppearance.BorderSize = 0;
			this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnExport.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnExport.Location = new System.Drawing.Point(603, 17);
			this.btnExport.Name = "btnExport";
			this.btnExport.Size = new System.Drawing.Size(97, 33);
			this.btnExport.TabIndex = 10;
			this.btnExport.Text = "Export";
			this.btnExport.UseVisualStyleBackColor = false;
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(717, 17);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(97, 33);
			this.btnClose.TabIndex = 11;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// frmReport
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(825, 436);
			this.Controls.Add(this.gbxRptContent);
			this.Controls.Add(this.gbxRptTitle);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmReport";
			this.Text = "frmReport";
			this.gbxRptTitle.ResumeLayout(false);
			this.gbxRptTitle.PerformLayout();
			this.gbxRptContent.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdRptContent).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbxRptTitle;
		private System.Windows.Forms.GroupBox gbxRptContent;
		public System.Windows.Forms.DataGridView dgdRptContent;
		public System.Windows.Forms.Label lblRptTitle;
		public System.Windows.Forms.Button btnPrint;
		public System.Windows.Forms.Button btnExport;
		public System.Windows.Forms.Button btnClose;
	}
}
