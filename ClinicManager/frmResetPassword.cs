using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmResetPassword : Form
	{
		public frmResetPassword()
		{
			InitializeComponent();
		}


		private void frmResetPassword_Load(object sender, EventArgs e)
		{
			DataSet ds = new DataAccess().ReadAllUserRecords();
			if (ds != null) {
				DataTable tableUsers = ds.Tables[0];
				cmbUsername.DisplayMember = "USERNAME";
				cmbUsername.ValueMember = "USERNAME";
				cmbUsername.DataSource = tableUsers;

				string query = string.Format("USERNAME = '{0}'", cmbUsername.SelectedValue.ToString());
				DataRow[] dr = tableUsers.Select(query);
				txtName.Text = dr[0]["NAME"].ToString();
				txtSurname.Text = dr[0]["SURNAME"].ToString();
			}
		}

		private void btnResetPassword_Click(object sender, EventArgs e)
		{
			string message = string.Format("Are you sure yo want to reset the password for {0}?", this.cmbUsername.SelectedValue.ToString());
			if (MessageBox.Show(message, "Confirm Password Reset", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
				DataAccess dataAccess = new DataAccess();
				dataAccess.ResetUserPassword(cmbUsername.SelectedValue.ToString());
				this.Close();
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
