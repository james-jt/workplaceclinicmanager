using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmNewPractitioner
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSaveAndNew = new System.Windows.Forms.Button();
			this.btnSaveAndClose = new System.Windows.Forms.Button();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.cmbCountry = new System.Windows.Forms.ComboBox();
			this.cmbCity = new System.Windows.Forms.ComboBox();
			this.lblCity = new System.Windows.Forms.Label();
			this.lblStreet = new System.Windows.Forms.Label();
			this.lblPhysicalAddress = new System.Windows.Forms.Label();
			this.lblCountry = new System.Windows.Forms.Label();
			this.txtStreet = new System.Windows.Forms.TextBox();
			this.txtPhysicalAddress = new System.Windows.Forms.TextBox();
			this.lblEmailAddress = new System.Windows.Forms.Label();
			this.txtTelNo = new System.Windows.Forms.TextBox();
			this.txtEmailAddress = new System.Windows.Forms.TextBox();
			this.lblPractitionerName = new System.Windows.Forms.Label();
			this.lblQualification = new System.Windows.Forms.Label();
			this.lblPractitionerCode = new System.Windows.Forms.Label();
			this.lblPractitionerStatus = new System.Windows.Forms.Label();
			this.lblTelNo = new System.Windows.Forms.Label();
			this.txtPractitionerName = new System.Windows.Forms.TextBox();
			this.txtPractitionerCode = new System.Windows.Forms.TextBox();
			this.txtQualification = new System.Windows.Forms.TextBox();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdPractitioners = new System.Windows.Forms.DataGridView();
			this.cmbPractitionerStatus = new System.Windows.Forms.ComboBox();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdPractitioners).BeginInit();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSaveAndNew);
			this.gbxButtons.Controls.Add(this.btnSaveAndClose);
			this.gbxButtons.Controls.Add(this.btnReset);
			this.gbxButtons.Controls.Add(this.btnCancel);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSaveAndNew
			// 
			this.btnSaveAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderSize = 0;
			this.btnSaveAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndNew.Location = new System.Drawing.Point(442, 22);
			this.btnSaveAndNew.Name = "btnSaveAndNew";
			this.btnSaveAndNew.Size = new System.Drawing.Size(94, 33);
			this.btnSaveAndNew.TabIndex = 7;
			this.btnSaveAndNew.Text = "Save and New";
			this.btnSaveAndNew.UseVisualStyleBackColor = false;
			this.btnSaveAndNew.Click += new System.EventHandler(this.btnSaveAndNew_Click);
			// 
			// btnSaveAndClose
			// 
			this.btnSaveAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderSize = 0;
			this.btnSaveAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndClose.Location = new System.Drawing.Point(556, 22);
			this.btnSaveAndClose.Name = "btnSaveAndClose";
			this.btnSaveAndClose.Size = new System.Drawing.Size(94, 33);
			this.btnSaveAndClose.TabIndex = 8;
			this.btnSaveAndClose.Text = "Save and Close";
			this.btnSaveAndClose.UseVisualStyleBackColor = false;
			this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
			// 
			// btnReset
			// 
			this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnReset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderSize = 0;
			this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnReset.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnReset.Location = new System.Drawing.Point(670, 22);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(94, 33);
			this.btnReset.TabIndex = 9;
			this.btnReset.Text = "Reset";
			this.btnReset.UseVisualStyleBackColor = false;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.FlatAppearance.BorderSize = 0;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnCancel.Location = new System.Drawing.Point(784, 22);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(94, 33);
			this.btnCancel.TabIndex = 10;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.cmbPractitionerStatus);
			this.gbxFields.Controls.Add(this.cmbCountry);
			this.gbxFields.Controls.Add(this.cmbCity);
			this.gbxFields.Controls.Add(this.lblCity);
			this.gbxFields.Controls.Add(this.lblStreet);
			this.gbxFields.Controls.Add(this.lblPhysicalAddress);
			this.gbxFields.Controls.Add(this.lblCountry);
			this.gbxFields.Controls.Add(this.txtStreet);
			this.gbxFields.Controls.Add(this.txtPhysicalAddress);
			this.gbxFields.Controls.Add(this.lblEmailAddress);
			this.gbxFields.Controls.Add(this.txtTelNo);
			this.gbxFields.Controls.Add(this.txtEmailAddress);
			this.gbxFields.Controls.Add(this.lblPractitionerName);
			this.gbxFields.Controls.Add(this.lblQualification);
			this.gbxFields.Controls.Add(this.lblPractitionerCode);
			this.gbxFields.Controls.Add(this.lblPractitionerStatus);
			this.gbxFields.Controls.Add(this.lblTelNo);
			this.gbxFields.Controls.Add(this.txtPractitionerName);
			this.gbxFields.Controls.Add(this.txtPractitionerCode);
			this.gbxFields.Controls.Add(this.txtQualification);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 193);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// cmbCountry
			// 
			this.cmbCountry.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbCountry.FormattingEnabled = true;
			this.cmbCountry.Items.AddRange(new object[] {
				"South Africa",
				"Zimbabwe"
			});
			this.cmbCountry.Location = new System.Drawing.Point(130, 247);
			this.cmbCountry.Name = "cmbCountry";
			this.cmbCountry.Size = new System.Drawing.Size(266, 23);
			this.cmbCountry.TabIndex = 33;
			// 
			// cmbCity
			// 
			this.cmbCity.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbCity.FormattingEnabled = true;
			this.cmbCity.Items.AddRange(new object[] {
				"Bulawayo",
				"Harare",
				"Johanesburg"
			});
			this.cmbCity.Location = new System.Drawing.Point(612, 247);
			this.cmbCity.Name = "cmbCity";
			this.cmbCity.Size = new System.Drawing.Size(266, 23);
			this.cmbCity.TabIndex = 32;
			// 
			// lblCity
			// 
			this.lblCity.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblCity.AutoSize = true;
			this.lblCity.Location = new System.Drawing.Point(575, 252);
			this.lblCity.Name = "lblCity";
			this.lblCity.Size = new System.Drawing.Size(31, 15);
			this.lblCity.TabIndex = 31;
			this.lblCity.Text = "City:";
			// 
			// lblStreet
			// 
			this.lblStreet.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblStreet.AutoSize = true;
			this.lblStreet.Location = new System.Drawing.Point(566, 198);
			this.lblStreet.Name = "lblStreet";
			this.lblStreet.Size = new System.Drawing.Size(40, 15);
			this.lblStreet.TabIndex = 24;
			this.lblStreet.Text = "Street:";
			// 
			// lblPhysicalAddress
			// 
			this.lblPhysicalAddress.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPhysicalAddress.AutoSize = true;
			this.lblPhysicalAddress.Location = new System.Drawing.Point(26, 198);
			this.lblPhysicalAddress.Name = "lblPhysicalAddress";
			this.lblPhysicalAddress.Size = new System.Drawing.Size(98, 15);
			this.lblPhysicalAddress.TabIndex = 25;
			this.lblPhysicalAddress.Text = "Physical Address:";
			// 
			// lblCountry
			// 
			this.lblCountry.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblCountry.AutoSize = true;
			this.lblCountry.Location = new System.Drawing.Point(71, 252);
			this.lblCountry.Name = "lblCountry";
			this.lblCountry.Size = new System.Drawing.Size(53, 15);
			this.lblCountry.TabIndex = 26;
			this.lblCountry.Text = "Country:";
			// 
			// txtStreet
			// 
			this.txtStreet.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtStreet.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtStreet.Location = new System.Drawing.Point(612, 193);
			this.txtStreet.Name = "txtStreet";
			this.txtStreet.Size = new System.Drawing.Size(266, 23);
			this.txtStreet.TabIndex = 27;
			// 
			// txtPhysicalAddress
			// 
			this.txtPhysicalAddress.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPhysicalAddress.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPhysicalAddress.Location = new System.Drawing.Point(130, 193);
			this.txtPhysicalAddress.Name = "txtPhysicalAddress";
			this.txtPhysicalAddress.Size = new System.Drawing.Size(266, 23);
			this.txtPhysicalAddress.TabIndex = 28;
			// 
			// lblEmailAddress
			// 
			this.lblEmailAddress.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmailAddress.AutoSize = true;
			this.lblEmailAddress.Location = new System.Drawing.Point(522, 147);
			this.lblEmailAddress.Name = "lblEmailAddress";
			this.lblEmailAddress.Size = new System.Drawing.Size(84, 15);
			this.lblEmailAddress.TabIndex = 0;
			this.lblEmailAddress.Text = "Email Address:";
			// 
			// txtTelNo
			// 
			this.txtTelNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtTelNo.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtTelNo.Location = new System.Drawing.Point(612, 86);
			this.txtTelNo.Name = "txtTelNo";
			this.txtTelNo.Size = new System.Drawing.Size(266, 23);
			this.txtTelNo.TabIndex = 4;
			// 
			// txtEmailAddress
			// 
			this.txtEmailAddress.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtEmailAddress.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtEmailAddress.Location = new System.Drawing.Point(612, 142);
			this.txtEmailAddress.Name = "txtEmailAddress";
			this.txtEmailAddress.Size = new System.Drawing.Size(266, 23);
			this.txtEmailAddress.TabIndex = 6;
			// 
			// lblPractitionerName
			// 
			this.lblPractitionerName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPractitionerName.AutoSize = true;
			this.lblPractitionerName.Location = new System.Drawing.Point(500, 39);
			this.lblPractitionerName.Name = "lblPractitionerName";
			this.lblPractitionerName.Size = new System.Drawing.Size(106, 15);
			this.lblPractitionerName.TabIndex = 0;
			this.lblPractitionerName.Text = "Practitioner Name:";
			// 
			// lblQualification
			// 
			this.lblQualification.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblQualification.AutoSize = true;
			this.lblQualification.Location = new System.Drawing.Point(46, 145);
			this.lblQualification.Name = "lblQualification";
			this.lblQualification.Size = new System.Drawing.Size(78, 15);
			this.lblQualification.TabIndex = 0;
			this.lblQualification.Text = "Qualification:";
			// 
			// lblPractitionerCode
			// 
			this.lblPractitionerCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPractitionerCode.AutoSize = true;
			this.lblPractitionerCode.Location = new System.Drawing.Point(22, 39);
			this.lblPractitionerCode.Name = "lblPractitionerCode";
			this.lblPractitionerCode.Size = new System.Drawing.Size(102, 15);
			this.lblPractitionerCode.TabIndex = 0;
			this.lblPractitionerCode.Text = "Practitioner Code:";
			// 
			// lblPractitionerStatus
			// 
			this.lblPractitionerStatus.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPractitionerStatus.AutoSize = true;
			this.lblPractitionerStatus.Location = new System.Drawing.Point(18, 91);
			this.lblPractitionerStatus.Name = "lblPractitionerStatus";
			this.lblPractitionerStatus.Size = new System.Drawing.Size(106, 15);
			this.lblPractitionerStatus.TabIndex = 0;
			this.lblPractitionerStatus.Text = "Practitioner Status:";
			// 
			// lblTelNo
			// 
			this.lblTelNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblTelNo.AutoSize = true;
			this.lblTelNo.Location = new System.Drawing.Point(555, 91);
			this.lblTelNo.Name = "lblTelNo";
			this.lblTelNo.Size = new System.Drawing.Size(51, 15);
			this.lblTelNo.TabIndex = 0;
			this.lblTelNo.Text = "Tel. No.:";
			// 
			// txtPractitionerName
			// 
			this.txtPractitionerName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPractitionerName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPractitionerName.Location = new System.Drawing.Point(612, 34);
			this.txtPractitionerName.Name = "txtPractitionerName";
			this.txtPractitionerName.Size = new System.Drawing.Size(266, 23);
			this.txtPractitionerName.TabIndex = 2;
			// 
			// txtPractitionerCode
			// 
			this.txtPractitionerCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPractitionerCode.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPractitionerCode.Location = new System.Drawing.Point(130, 34);
			this.txtPractitionerCode.Name = "txtPractitionerCode";
			this.txtPractitionerCode.Size = new System.Drawing.Size(266, 23);
			this.txtPractitionerCode.TabIndex = 1;
			// 
			// txtQualification
			// 
			this.txtQualification.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtQualification.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtQualification.Location = new System.Drawing.Point(130, 140);
			this.txtQualification.Name = "txtQualification";
			this.txtQualification.Size = new System.Drawing.Size(266, 23);
			this.txtQualification.TabIndex = 5;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdPractitioners);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 261);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 168);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			// 
			// dgdPractitioners
			// 
			this.dgdPractitioners.AllowUserToAddRows = false;
			this.dgdPractitioners.AllowUserToDeleteRows = false;
			this.dgdPractitioners.AllowUserToOrderColumns = true;
			this.dgdPractitioners.BackgroundColor = System.Drawing.Color.White;
			this.dgdPractitioners.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdPractitioners.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdPractitioners.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdPractitioners.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdPractitioners.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdPractitioners.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdPractitioners.Location = new System.Drawing.Point(3, 19);
			this.dgdPractitioners.Name = "dgdPractitioners";
			this.dgdPractitioners.ReadOnly = true;
			this.dgdPractitioners.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdPractitioners.RowHeadersVisible = false;
			this.dgdPractitioners.Size = new System.Drawing.Size(880, 146);
			this.dgdPractitioners.TabIndex = 0;
			this.dgdPractitioners.TabStop = false;
			// 
			// cmbPractitionerStatus
			// 
			this.cmbPractitionerStatus.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbPractitionerStatus.FormattingEnabled = true;
			this.cmbPractitionerStatus.Location = new System.Drawing.Point(130, 86);
			this.cmbPractitionerStatus.Name = "cmbPractitionerStatus";
			this.cmbPractitionerStatus.Size = new System.Drawing.Size(266, 23);
			this.cmbPractitionerStatus.TabIndex = 3;
			// 
			// frmNewPractitioner
			// 
			this.AcceptButton = this.btnSaveAndNew;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmNewPractitioner";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Drug Input";
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdPractitioners).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnSaveAndNew;
		public System.Windows.Forms.Button btnSaveAndClose;
		public System.Windows.Forms.Button btnReset;
		public System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		private System.Windows.Forms.Label lblPractitionerName;
		private System.Windows.Forms.Label lblQualification;
		private System.Windows.Forms.Label lblPractitionerCode;
		private System.Windows.Forms.Label lblPractitionerStatus;
		private System.Windows.Forms.Label lblTelNo;
		public System.Windows.Forms.TextBox txtPractitionerName;
		public System.Windows.Forms.TextBox txtPractitionerCode;
		public System.Windows.Forms.TextBox txtQualification;
		public System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdPractitioners;
		private System.Windows.Forms.Label lblEmailAddress;
		public System.Windows.Forms.TextBox txtTelNo;
		public System.Windows.Forms.TextBox txtEmailAddress;
		private System.Windows.Forms.Label lblCity;
		private System.Windows.Forms.Label lblStreet;
		private System.Windows.Forms.Label lblPhysicalAddress;
		private System.Windows.Forms.Label lblCountry;
		public System.Windows.Forms.TextBox txtStreet;
		public System.Windows.Forms.TextBox txtPhysicalAddress;
		public System.Windows.Forms.ComboBox cmbCountry;
		public System.Windows.Forms.ComboBox cmbCity;
		public System.Windows.Forms.ComboBox cmbPractitionerStatus;
	}
}
