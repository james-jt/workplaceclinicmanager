using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmViewDrug : Form, IUpdatesDashboard
	{

		private DataTable tableSuppliers;
		public frmViewDrug(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			FillDataGrid();
			ResetFormControls();

			EntityChange += handler;
		}

		#region "Common Methods"

		private void InitializeDataTables()
		{
			tableSuppliers = (new DataAccess().ReadAllSupplierRecords()).Tables[0];
			if (tableSuppliers != null) {
				this.cmbPreferredSupplier.DisplayMember = "SUPPLIERCODE";
				this.cmbPreferredSupplier.ValueMember = "SUPPLIERCODE";
				this.cmbPreferredSupplier.DataSource = tableSuppliers;
			}
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllDrugRecords();
			dgdDrugs.DataSource = ds;
			dgdDrugs.DataMember = ds.Tables[0].ToString();
		}

		private void SetFormControls()
		{
			txtDrugCode.Enabled = true;
			txtDrugName.Enabled = false;
			txtCategory.Enabled = false;
			cmbPreferredSupplier.Enabled = false;
			txtNotes.Enabled = false;
			txtReorderLevel.Enabled = false;

			btnEdit.Enabled = true;
			btnDelete.Enabled = true;
			btnSave.Enabled = false;

			txtDrugCode.SelectAll();
			txtDrugCode.Focus();

			btnView.Text = "View";
		}

		private void ResetFormControls()
		{
			txtDrugCode.SelectAll();
			txtDrugCode.Focus();
			txtDrugName.Text = "";
			txtCategory.Text = "";
			cmbPreferredSupplier.Text = "";
			txtNotes.Text = "";
			txtReorderLevel.Text = "";
		}

		public event EventHandler EntityChange;

		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnView_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtDrugCode.Text)) {
				MessageBox.Show("Please enter Drug Code.", "Missing Input");
				ResetFormControls();
				return;
			}

			DataSet ds = new DataAccess().ReadDrugRecord(this);

			if (ds.Tables[0].Rows.Count > 0) {
				txtDrugName.Text = ds.Tables[0].Rows[0]["NAME"].ToString();
				txtCategory.Text = ds.Tables[0].Rows[0]["CATEGORY"].ToString();
				txtNotes.Text = ds.Tables[0].Rows[0]["SPECIALNOTE"].ToString();
				cmbPreferredSupplier.Text = ds.Tables[0].Rows[0]["PREFERREDSUPPLIER"].ToString();
				txtReorderLevel.Text = ds.Tables[0].Rows[0]["REORDERLEVEL"].ToString();

				SetFormControls();
			} else {
				MessageBox.Show("The Drug Code you entered does not exist in the system.", "Invalid Input");
				ResetFormControls();
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			txtDrugCode.Enabled = false;
			txtDrugName.Enabled = true;
			txtCategory.Enabled = true;
			cmbPreferredSupplier.Enabled = true;
			txtNotes.Enabled = true;
			txtReorderLevel.Enabled = true;

			btnEdit.Enabled = false;
			btnDelete.Enabled = false;
			btnSave.Enabled = true;

			btnView.Text = "Reset";
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to completely remove this record from the system?", "Confirm Delete Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
				DataAccess dataAccess = new DataAccess();
				dataAccess.DeleteDrugRecord(this);
				if (EntityChange != null) {
					EntityChange(null, null);
				}
				FillDataGrid();
				ResetFormControls();
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtDrugCode.Text)) {
				MessageBox.Show("Please enter the drug code.", "Missing Input");
				this.txtDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtDrugName.Text)) {
				MessageBox.Show("Please enter the drug name.", "Missing Input");
				this.txtDrugName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtCategory.Text)) {
				MessageBox.Show("Please enter the drug's category.", "Missing Input");
				this.txtCategory.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtReorderLevel.Text)) {
				MessageBox.Show("Please enter the drug's re-order level.", "Missing Input");
				this.txtReorderLevel.Focus();
				return;
			}
			int reOrderLevel = 0;
			if (!(int.TryParse(txtReorderLevel.Text, out reOrderLevel))) {
				MessageBox.Show("Only numeric (integer) values permitted for Quantity.", "Invalid Input");
				txtReorderLevel.SelectAll();
				txtReorderLevel.Focus();
				return;
			}
			if (reOrderLevel < 1) {
				MessageBox.Show("Quantity must be greater than zero.", "Invalid Input");
				txtReorderLevel.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.UpdateDrugRecord(this);
			FillDataGrid();
			SetFormControls();
		}

		private void frmViewDrug_Activated(object sender, EventArgs e)
		{
			this.FillDataGrid();
		}
	}
}
