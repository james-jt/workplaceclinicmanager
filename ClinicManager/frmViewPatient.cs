using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmViewPatient : Form, IUpdatesDashboard
	{
		public frmViewPatient(EventHandler handler)
		{
			InitializeComponent();
			FillDataGrid();
			ResetFormControls();

			EntityChange += handler;
		}


		#region "Common Methods"

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllPatientRecords();
			dgdPatients.DataSource = ds;
			dgdPatients.DataMember = ds.Tables[0].ToString();
		}

		private void SetFormControls()
		{
			txtEmpNo.Enabled = true;
			txtPatientName.Enabled = false;
			txtPatientSurname.Enabled = false;
			txtDpt.Enabled = false;
			dtpPatientDOB.Enabled = false;

			btnEdit.Enabled = true;
			btnDelete.Enabled = true;
			btnSave.Enabled = false;

			txtEmpNo.SelectAll();
			txtEmpNo.Focus();

			btnView.Text = "View";
		}

		private void ResetFormControls()
		{
			txtEmpNo.SelectAll();
			txtEmpNo.Focus();
			txtPatientName.Text = "";
			txtPatientSurname.Text = "";
			txtDpt.Text = "";
			dtpPatientDOB.Value = DateTime.Today.AddYears(-30);
		}

		public event EventHandler EntityChange;
		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnView_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtEmpNo.Text)) {
				MessageBox.Show("Please enter Patient's Employee Number.", "Missing Input");
				ResetFormControls();
				return;
			}

			DataSet ds = new DataAccess().ReadPatientRecord(this);

			if (ds.Tables[0].Rows.Count > 0) {
				txtPatientName.Text = ds.Tables[0].Rows[0]["NAME"].ToString();
				txtPatientSurname.Text = ds.Tables[0].Rows[0]["SURNAME"].ToString();
				txtDpt.Text = ds.Tables[0].Rows[0]["DEPARTMENT"].ToString();
				dtpPatientDOB.Value = Convert.ToDateTime(ds.Tables[0].Rows[0]["DATEOFBIRTH"].ToString());

				SetFormControls();
			} else {
				MessageBox.Show("The Employee Number you entered does not exist in the system.", "Invalid Input");
				ResetFormControls();
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			txtEmpNo.Enabled = false;
			txtPatientName.Enabled = true;
			txtPatientSurname.Enabled = true;
			txtDpt.Enabled = true;
			dtpPatientDOB.Enabled = true;

			btnEdit.Enabled = false;
			btnDelete.Enabled = false;
			btnSave.Enabled = true;

			btnView.Text = "Reset";
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to completely remove this record from the system?", "Confirm Delete Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
				DataAccess dataAccess = new DataAccess();
				dataAccess.DeletePatientRecord(this);
				if (EntityChange != null) {
					EntityChange(null, null);
				}
				FillDataGrid();
				ResetFormControls();
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtPatientName.Text)) {
				MessageBox.Show("Please Enter Patient's Name.", "Missing Input");
				this.txtPatientName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPatientSurname.Text)) {
				MessageBox.Show("Please Enter Patient's Surname.", "Missing Input");
				this.txtPatientSurname.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtDpt.Text)) {
				MessageBox.Show("Please Enter Patient's Department.", "Missing Input");
				this.txtDpt.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.UpdatePatientRecord(this);
			FillDataGrid();
			SetFormControls();
		}

		private void frmViewPatient_Activated(object sender, EventArgs e)
		{
			this.FillDataGrid();
		}
	}
}
