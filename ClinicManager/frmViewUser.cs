using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmViewUser : Form
	{
		public frmViewUser()
		{
			InitializeComponent();
			FillDataGrid();
			ResetFormControls();
		}

		#region "Common Methods"

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllDrugRecords();
			dgdDrugs.DataSource = ds;
			dgdDrugs.DataMember = ds.Tables[0].ToString();
		}

		private void SetFormControls()
		{
			txtUsername.Enabled = true;
			txtPassword.Enabled = false;
			cmbUserType.Enabled = false;
			txtNationalIDNo.Enabled = false;
			txtName.Enabled = false;
			txtSurname.Enabled = false;

			btnEdit.Enabled = true;
			btnDelete.Enabled = true;
			btnSave.Enabled = false;

			txtUsername.SelectAll();
			txtUsername.Focus();

			btnView.Text = "View";
		}

		private void ResetFormControls()
		{
			txtUsername.SelectAll();
			txtUsername.Focus();
			txtPassword.Text = "";
			cmbUserType.Text = "";
			txtNationalIDNo.Text = "";
			txtName.Text = "";
			txtSurname.Text = "";
		}

		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnView_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtUsername.Text)) {
				MessageBox.Show("Please enter Username.", "Missing Input");
				ResetFormControls();
				return;
			}

			DataSet ds = new DataAccess().ReadUserRecord(this);

			if (ds.Tables[0].Rows.Count > 0) {
				txtPassword.Text = ds.Tables[0].Rows[0]["PASSWORD"].ToString();
				cmbUserType.Text = ds.Tables[0].Rows[0]["USERTYPE"].ToString();
				txtNationalIDNo.Text = ds.Tables[0].Rows[0]["NATIONALID"].ToString();
				txtName.Text = ds.Tables[0].Rows[0]["NAME"].ToString();
				txtSurname.Text = ds.Tables[0].Rows[0]["SURNAME"].ToString();

				SetFormControls();
			} else {
				MessageBox.Show("The Drug Code you entered does not exist in the system.", "Invalid Input");
				ResetFormControls();
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			txtUsername.Enabled = false;
			cmbUserType.Enabled = true;
			txtNationalIDNo.Enabled = true;
			txtName.Enabled = true;
			txtSurname.Enabled = true;

			btnEdit.Enabled = false;
			btnDelete.Enabled = false;
			btnSave.Enabled = true;

			btnView.Text = "Reset";
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to completely remove this record from the system?", "Confirm Delete Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
				DataAccess dataAccess = new DataAccess();
				dataAccess.DeleteUserRecord(this);
				FillDataGrid();
				ResetFormControls();
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.cmbUserType.Text)) {
				MessageBox.Show("Please enter the User Type.", "Missing Input");
				this.cmbUserType.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtNationalIDNo.Text)) {
				MessageBox.Show("Please enter User' National ID Number.", "Missing Input");
				this.txtNationalIDNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtName.Text)) {
				MessageBox.Show("Please enter the User's name.", "Missing Input");
				this.txtName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtUsername.Text)) {
				MessageBox.Show("Please enter the User's Surname.", "Missing Input");
				this.txtUsername.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.UpdateUserRecord(this);
			FillDataGrid();
			SetFormControls();
		}

		private void frmViewDrug_Activated(object sender, EventArgs e)
		{
			this.FillDataGrid();
		}
	}
}
