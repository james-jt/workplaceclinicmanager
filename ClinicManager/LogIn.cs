using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmLogIn : Form
	{
		private DataSet dsUsers;
		private DataTable tableUsers;
		private string query;

		private DataRow[] dr;
		public frmLogIn()
		{
			InitializeComponent();
			dsUsers = (new DataAccess()).ReadAllUserRecords();
			if (dsUsers == null) {
				Application.Exit();
			}
			tableUsers = dsUsers.Tables[0];
		}

		private void frmLogIn_Load(object sender, EventArgs e)
		{
			this.txtUsername.Text = Program.userName;
			Program.userName = string.Empty;
		}

		private void btnLogIn_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtUsername.Text)) {
				MessageBox.Show("Please enter a username.", "Missing Input");
				txtUsername.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(txtPassword.Text)) {
				MessageBox.Show("Please enter a password.", "Missing Input");
				txtPassword.Focus();
				return;
			}

			query = string.Format("USERNAME = '{0}'", txtUsername.Text.ToUpper());
			dr = null;
			dr = tableUsers.Select(query);
			if (dr.Length < 1) {
				MessageBox.Show("Invalid username.", "Incorrect Input");
				txtUsername.SelectAll();
				txtUsername.Focus();
				return;
			}
			Program.userName = txtUsername.Text.ToUpper();
			Program.userType = dr[0]["USERTYPE"].ToString();
			if (dr[0]["PASSWORD"].ToString() == "default") {
				txtPassword.Text = "default";
				frmChangePassword form = new frmChangePassword(true);
				form.ShowDialog();
			}
			if (dr[0]["PASSWORD"].ToString() != txtPassword.Text) {
				MessageBox.Show("Incorrect Password.", "Missing Input");
				txtPassword.SelectAll();
				txtPassword.Focus();
				return;
			}
			this.Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

	}
}
