using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmAddAdjustments : Form
	{
		private DataTable tableDrugs;
		private DataTable tableBatches;
		private DataTable tableAddedBatches;

		private DataGridView dgdAddedBatches;
		public frmAddAdjustments(ref DataTable tableAddedBatches, ref DataGridView dgdBatches)
		{
			InitializeComponent();
			InitializeDataTables();
			this.tableAddedBatches = tableAddedBatches;
			this.dgdAddedBatches = dgdBatches;
		}

		private void InitializeDataTables()
		{
			tableDrugs = (new DataAccess().ReadAllDrugRecords()).Tables[0];
			if (tableDrugs != null) {
				this.cmbDrugCode.DisplayMember = "DRUGCODE";
				this.cmbDrugCode.ValueMember = "DRUGCODE";
				this.cmbDrugCode.DataSource = tableDrugs;
			}

			tableBatches = (new DataAccess().ReadAllBatchRecords()).Tables[0];
			UpdateBatchesDropDownList();
		}

		private void UpdateExpiryDate()
		{
			if (tableBatches != null) {
				string query = string.Format("DRUGCODE = '{0}' AND BATCHNO = '{1}'", cmbDrugCode.Text, cmbBatchNo.Text);
				DataRow[] selectedBatch = tableBatches.Select(query);
				if (selectedBatch.Length > 0) {
					txtCurrentExpiryDate.Text = selectedBatch[0]["EXPIRYDATE"].ToString();
					dtpExpiryDate.Value = Convert.ToDateTime(selectedBatch[0]["EXPIRYDATE"]);
				}
			}
		}

		private void UpdateBatchesDropDownList()
		{
			if (tableBatches != null) {
				string query = string.Format("DRUGCODE = '{0}'", cmbDrugCode.SelectedValue);
				DataView viewBatches = tableBatches.DefaultView;
				viewBatches.RowFilter = (query);
				this.cmbBatchNo.ValueMember = "BATCHNO";
				this.cmbBatchNo.DisplayMember = "BATCHNO";
				this.cmbBatchNo.DataSource = viewBatches;
			}
		}

		private void cmbDrugCode_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.cmbDrugCode.Text)) {
				return;
			}
			string query = string.Format("DRUGCODE = '{0}'", cmbDrugCode.SelectedValue);
			DataRow[] selectedDrug = tableDrugs.Select(query);
			txtDescription.Text = selectedDrug[0]["NAME"].ToString();
			UpdateBatchesDropDownList();
			UpdateExpiryDate();
		}

		private void cmbBatchNo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.cmbDrugCode.Text)) {
				return;
			}
			string query = string.Format("DRUGCODE = '{0}' AND BATCHNO = '{1}'", cmbDrugCode.SelectedValue, cmbBatchNo.SelectedValue);
			DataRow[] selectedBatch = tableBatches.Select(query);
			txtCurrentQuantity.Text = selectedBatch[0]["QUANTITY"].ToString();
			UpdateExpiryDate();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnAddAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbDrugCode.Text)) {
				MessageBox.Show("Please Enter a Drug Code.", "Missing Input");
				cmbDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(cmbBatchNo.Text)) {
				MessageBox.Show("Please specify the Batch Number.", "Missing Input");
				cmbBatchNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(txtNewQuantity.Text)) {
				MessageBox.Show("Please specify the Quantity.", "Missing Input");
				txtNewQuantity.Focus();
				return;
			}
			int quantity = 0;
			if (!(int.TryParse(txtNewQuantity.Text, out quantity))) {
				MessageBox.Show("Only numeric (integer) values permitted for Quantity.", "Invalid Input");
				txtNewQuantity.SelectAll();
				txtNewQuantity.Focus();
				return;
			}
			if (quantity < 1) {
				MessageBox.Show("Quantity must be greater than zero.", "Invalid Input");
				txtNewQuantity.Focus();
				return;
			}

			DataRow dr = tableAddedBatches.NewRow();
			dr["Drug Code"] = cmbDrugCode.SelectedValue;
			dr["Drug Name"] = txtDescription.Text;
			dr["Batch No."] = cmbBatchNo.Text;
			dr["Quantity"] = quantity;
			dr["Expiry Date"] = dtpExpiryDate.Value;
			dr["Notes"] = txtNote.Text;
			tableAddedBatches.Rows.Add(dr);

			dgdAddedBatches.Update();
			this.Close();
		}

		private void btnAddAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbDrugCode.Text)) {
				MessageBox.Show("Please Enter a Drug Code.", "Missing Input");
				cmbDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(cmbBatchNo.Text)) {
				MessageBox.Show("Please specify the Batch Number.", "Missing Input");
				cmbBatchNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(txtNewQuantity.Text)) {
				MessageBox.Show("Please specify the Quantity.", "Missing Input");
				txtNewQuantity.Focus();
				return;
			}
			int quantity = 0;
			if (!(int.TryParse(txtNewQuantity.Text, out quantity))) {
				MessageBox.Show("Only numeric (integer) values permitted for Quantity.", "Invalid Input");
				txtNewQuantity.SelectAll();
				txtNewQuantity.Focus();
				return;
			}
			if (quantity < 1) {
				MessageBox.Show("Quantity must be greater than zero.", "Invalid Input");
				txtNewQuantity.Focus();
				return;
			}

			DataRow dr = tableAddedBatches.NewRow();
			dr["Drug Code"] = cmbDrugCode.SelectedValue;
			dr["Drug Name"] = txtDescription.Text;
			dr["Batch No."] = cmbBatchNo.Text;
			dr["Quantity"] = quantity;
			dr["Expiry Date"] = dtpExpiryDate.Value;
			dr["Notes"] = txtNote.Text;
			tableAddedBatches.Rows.Add(dr);

			dgdAddedBatches.Update();

			cmbDrugCode.Text = "";
			txtDescription.Text = "";
			cmbBatchNo.Text = "";
			txtNewQuantity.Text = "";
			txtNote.Text = "";

			cmbDrugCode.Focus();
		}

	}
}
