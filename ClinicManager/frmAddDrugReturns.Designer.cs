using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmAddDrugReturns
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnAddAndNew = new System.Windows.Forms.Button();
			this.btnAddAndClose = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtExpiryDate = new System.Windows.Forms.TextBox();
			this.cmbBatchNo = new System.Windows.Forms.ComboBox();
			this.lblBatchNo = new System.Windows.Forms.Label();
			this.txtNote = new System.Windows.Forms.TextBox();
			this.txtQuantity = new System.Windows.Forms.TextBox();
			this.cmbDrugCode = new System.Windows.Forms.ComboBox();
			this.lblDrugCode = new System.Windows.Forms.Label();
			this.lblQuantity = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lblNote = new System.Windows.Forms.Label();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 525);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(388, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 525);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnAddAndNew);
			this.gbxButtons.Controls.Add(this.btnAddAndClose);
			this.gbxButtons.Controls.Add(this.btnClose);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(376, 68);
			this.gbxButtons.TabIndex = 9;
			this.gbxButtons.TabStop = false;
			// 
			// btnAddAndNew
			// 
			this.btnAddAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnAddAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnAddAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnAddAndNew.FlatAppearance.BorderSize = 0;
			this.btnAddAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAddAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnAddAndNew.Location = new System.Drawing.Point(42, 22);
			this.btnAddAndNew.Name = "btnAddAndNew";
			this.btnAddAndNew.Size = new System.Drawing.Size(97, 33);
			this.btnAddAndNew.TabIndex = 5;
			this.btnAddAndNew.Text = "Add and New";
			this.btnAddAndNew.UseVisualStyleBackColor = false;
			this.btnAddAndNew.Click += new System.EventHandler(this.btnAddAndNew_Click);
			// 
			// btnAddAndClose
			// 
			this.btnAddAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnAddAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnAddAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnAddAndClose.FlatAppearance.BorderSize = 0;
			this.btnAddAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAddAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnAddAndClose.Location = new System.Drawing.Point(156, 22);
			this.btnAddAndClose.Name = "btnAddAndClose";
			this.btnAddAndClose.Size = new System.Drawing.Size(97, 33);
			this.btnAddAndClose.TabIndex = 6;
			this.btnAddAndClose.Text = "Add and Close";
			this.btnAddAndClose.UseVisualStyleBackColor = false;
			this.btnAddAndClose.Click += new System.EventHandler(this.btnAddAndClose_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(270, 22);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(97, 33);
			this.btnClose.TabIndex = 7;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 513);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(376, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.label1);
			this.gbxFields.Controls.Add(this.txtExpiryDate);
			this.gbxFields.Controls.Add(this.cmbBatchNo);
			this.gbxFields.Controls.Add(this.lblBatchNo);
			this.gbxFields.Controls.Add(this.txtNote);
			this.gbxFields.Controls.Add(this.txtQuantity);
			this.gbxFields.Controls.Add(this.cmbDrugCode);
			this.gbxFields.Controls.Add(this.lblDrugCode);
			this.gbxFields.Controls.Add(this.lblQuantity);
			this.gbxFields.Controls.Add(this.lblDescription);
			this.gbxFields.Controls.Add(this.lblNote);
			this.gbxFields.Controls.Add(this.txtDescription);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(376, 445);
			this.gbxFields.TabIndex = 15;
			this.gbxFields.TabStop = false;
			// 
			// label1
			// 
			this.label1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(27, 209);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(68, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "Expiry Date:";
			// 
			// txtExpiryDate
			// 
			this.txtExpiryDate.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtExpiryDate.Enabled = false;
			this.txtExpiryDate.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtExpiryDate.Location = new System.Drawing.Point(101, 204);
			this.txtExpiryDate.Name = "txtExpiryDate";
			this.txtExpiryDate.Size = new System.Drawing.Size(266, 23);
			this.txtExpiryDate.TabIndex = 29;
			this.txtExpiryDate.TabStop = false;
			// 
			// cmbBatchNo
			// 
			this.cmbBatchNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbBatchNo.FormattingEnabled = true;
			this.cmbBatchNo.Location = new System.Drawing.Point(101, 145);
			this.cmbBatchNo.Name = "cmbBatchNo";
			this.cmbBatchNo.Size = new System.Drawing.Size(266, 23);
			this.cmbBatchNo.TabIndex = 2;
			this.cmbBatchNo.SelectedValueChanged += new System.EventHandler(this.cmbBatchNo_SelectedValueChanged);
			// 
			// lblBatchNo
			// 
			this.lblBatchNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblBatchNo.AutoSize = true;
			this.lblBatchNo.Location = new System.Drawing.Point(8, 150);
			this.lblBatchNo.Name = "lblBatchNo";
			this.lblBatchNo.Size = new System.Drawing.Size(87, 15);
			this.lblBatchNo.TabIndex = 0;
			this.lblBatchNo.Text = "Batch Number:";
			// 
			// txtNote
			// 
			this.txtNote.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNote.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNote.Location = new System.Drawing.Point(101, 322);
			this.txtNote.Multiline = true;
			this.txtNote.Name = "txtNote";
			this.txtNote.Size = new System.Drawing.Size(266, 90);
			this.txtNote.TabIndex = 4;
			// 
			// txtQuantity
			// 
			this.txtQuantity.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtQuantity.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtQuantity.Location = new System.Drawing.Point(101, 266);
			this.txtQuantity.Name = "txtQuantity";
			this.txtQuantity.Size = new System.Drawing.Size(266, 23);
			this.txtQuantity.TabIndex = 3;
			// 
			// cmbDrugCode
			// 
			this.cmbDrugCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbDrugCode.FormattingEnabled = true;
			this.cmbDrugCode.Location = new System.Drawing.Point(101, 34);
			this.cmbDrugCode.Name = "cmbDrugCode";
			this.cmbDrugCode.Size = new System.Drawing.Size(266, 23);
			this.cmbDrugCode.TabIndex = 1;
			this.cmbDrugCode.SelectedIndexChanged += new System.EventHandler(this.cmbDrugCode_SelectedIndexChanged);
			// 
			// lblDrugCode
			// 
			this.lblDrugCode.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblDrugCode.AutoSize = true;
			this.lblDrugCode.Location = new System.Drawing.Point(28, 39);
			this.lblDrugCode.Name = "lblDrugCode";
			this.lblDrugCode.Size = new System.Drawing.Size(67, 15);
			this.lblDrugCode.TabIndex = 0;
			this.lblDrugCode.Text = "Drug Code:";
			// 
			// lblQuantity
			// 
			this.lblQuantity.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblQuantity.AutoSize = true;
			this.lblQuantity.Location = new System.Drawing.Point(39, 271);
			this.lblQuantity.Name = "lblQuantity";
			this.lblQuantity.Size = new System.Drawing.Size(56, 15);
			this.lblQuantity.TabIndex = 0;
			this.lblQuantity.Text = "Quantity:";
			// 
			// lblDescription
			// 
			this.lblDescription.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblDescription.AutoSize = true;
			this.lblDescription.Location = new System.Drawing.Point(25, 93);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(70, 15);
			this.lblDescription.TabIndex = 0;
			this.lblDescription.Text = "Description:";
			// 
			// lblNote
			// 
			this.lblNote.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNote.AutoSize = true;
			this.lblNote.Location = new System.Drawing.Point(59, 327);
			this.lblNote.Name = "lblNote";
			this.lblNote.Size = new System.Drawing.Size(36, 15);
			this.lblNote.TabIndex = 0;
			this.lblNote.Text = "Note:";
			// 
			// txtDescription
			// 
			this.txtDescription.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtDescription.Enabled = false;
			this.txtDescription.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtDescription.Location = new System.Drawing.Point(101, 88);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(266, 23);
			this.txtDescription.TabIndex = 16;
			this.txtDescription.TabStop = false;
			// 
			// frmAddDrugReturns
			// 
			this.AcceptButton = this.btnAddAndNew;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.GhostWhite;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(400, 525);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "frmAddDrugReturns";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Add Drugs";
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnAddAndNew;
		public System.Windows.Forms.Button btnAddAndClose;
		public System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		private System.Windows.Forms.Label lblDrugCode;
		private System.Windows.Forms.Label lblQuantity;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Label lblNote;
		public System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.ComboBox cmbDrugCode;
		public System.Windows.Forms.TextBox txtNote;
		public System.Windows.Forms.TextBox txtQuantity;
		private System.Windows.Forms.ComboBox cmbBatchNo;
		private System.Windows.Forms.Label lblBatchNo;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.TextBox txtExpiryDate;
	}
}
