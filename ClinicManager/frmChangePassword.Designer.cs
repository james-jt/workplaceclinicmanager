using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmChangePassword
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblReneterNewPassword = new System.Windows.Forms.Label();
			this.txtConfirmPassword = new System.Windows.Forms.TextBox();
			this.lblNewPassword = new System.Windows.Forms.Label();
			this.lblCurrentPassword = new System.Windows.Forms.Label();
			this.txtNewPassword = new System.Windows.Forms.TextBox();
			this.btnChangePassword = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.txtCurrentPassword = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblReneterNewPassword
			// 
			this.lblReneterNewPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblReneterNewPassword.AutoSize = true;
			this.lblReneterNewPassword.Location = new System.Drawing.Point(15, 95);
			this.lblReneterNewPassword.Name = "lblReneterNewPassword";
			this.lblReneterNewPassword.Size = new System.Drawing.Size(134, 15);
			this.lblReneterNewPassword.TabIndex = 0;
			this.lblReneterNewPassword.Text = "Confirm New Password:";
			// 
			// txtConfirmPassword
			// 
			this.txtConfirmPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtConfirmPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtConfirmPassword.Location = new System.Drawing.Point(155, 90);
			this.txtConfirmPassword.Name = "txtConfirmPassword";
			this.txtConfirmPassword.PasswordChar = '*';
			this.txtConfirmPassword.Size = new System.Drawing.Size(203, 23);
			this.txtConfirmPassword.TabIndex = 3;
			// 
			// lblNewPassword
			// 
			this.lblNewPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNewPassword.AutoSize = true;
			this.lblNewPassword.Location = new System.Drawing.Point(63, 55);
			this.lblNewPassword.Name = "lblNewPassword";
			this.lblNewPassword.Size = new System.Drawing.Size(87, 15);
			this.lblNewPassword.TabIndex = 0;
			this.lblNewPassword.Text = "New Password:";
			// 
			// lblCurrentPassword
			// 
			this.lblCurrentPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblCurrentPassword.AutoSize = true;
			this.lblCurrentPassword.Location = new System.Drawing.Point(47, 16);
			this.lblCurrentPassword.Name = "lblCurrentPassword";
			this.lblCurrentPassword.Size = new System.Drawing.Size(103, 15);
			this.lblCurrentPassword.TabIndex = 0;
			this.lblCurrentPassword.Text = "Current Password:";
			// 
			// txtNewPassword
			// 
			this.txtNewPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNewPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNewPassword.Location = new System.Drawing.Point(155, 51);
			this.txtNewPassword.Name = "txtNewPassword";
			this.txtNewPassword.PasswordChar = '*';
			this.txtNewPassword.Size = new System.Drawing.Size(203, 23);
			this.txtNewPassword.TabIndex = 2;
			// 
			// btnChangePassword
			// 
			this.btnChangePassword.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnChangePassword.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnChangePassword.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnChangePassword.FlatAppearance.BorderSize = 0;
			this.btnChangePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChangePassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnChangePassword.Location = new System.Drawing.Point(155, 128);
			this.btnChangePassword.Name = "btnChangePassword";
			this.btnChangePassword.Size = new System.Drawing.Size(99, 31);
			this.btnChangePassword.TabIndex = 4;
			this.btnChangePassword.Text = "OK";
			this.btnChangePassword.UseVisualStyleBackColor = false;
			this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.FlatAppearance.BorderSize = 0;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnCancel.Location = new System.Drawing.Point(259, 128);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(99, 31);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// txtCurrentPassword
			// 
			this.txtCurrentPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtCurrentPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtCurrentPassword.Location = new System.Drawing.Point(155, 12);
			this.txtCurrentPassword.Name = "txtCurrentPassword";
			this.txtCurrentPassword.PasswordChar = '*';
			this.txtCurrentPassword.Size = new System.Drawing.Size(203, 23);
			this.txtCurrentPassword.TabIndex = 1;
			// 
			// frmChangePassword
			// 
			this.AcceptButton = this.btnChangePassword;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(370, 171);
			this.Controls.Add(this.txtCurrentPassword);
			this.Controls.Add(this.btnChangePassword);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.lblReneterNewPassword);
			this.Controls.Add(this.txtConfirmPassword);
			this.Controls.Add(this.lblNewPassword);
			this.Controls.Add(this.lblCurrentPassword);
			this.Controls.Add(this.txtNewPassword);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "frmChangePassword";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Change Password";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label lblReneterNewPassword;
		public System.Windows.Forms.TextBox txtConfirmPassword;
		private System.Windows.Forms.Label lblNewPassword;
		private System.Windows.Forms.Label lblCurrentPassword;
		public System.Windows.Forms.TextBox txtNewPassword;
		public System.Windows.Forms.Button btnChangePassword;
		public System.Windows.Forms.Button btnCancel;
		public System.Windows.Forms.TextBox txtCurrentPassword;
	}
}
