using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmLogIn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogIn));
			this.lblUsername = new System.Windows.Forms.Label();
			this.lblPassword = new System.Windows.Forms.Label();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.btnLogIn = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.pnlPicture = new System.Windows.Forms.Panel();
			this.pbxPicture = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.pnlPicture.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.pbxPicture).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.pictureBox1).BeginInit();
			this.SuspendLayout();
			//
			//lblUsername
			//
			this.lblUsername.AutoSize = true;
			this.lblUsername.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblUsername.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.lblUsername.Location = new System.Drawing.Point(310, 100);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(63, 15);
			this.lblUsername.TabIndex = 0;
			this.lblUsername.Text = "Username:";
			//
			//lblPassword
			//
			this.lblPassword.AutoSize = true;
			this.lblPassword.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lblPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.lblPassword.Location = new System.Drawing.Point(313, 139);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(60, 15);
			this.lblPassword.TabIndex = 1;
			this.lblPassword.Text = "Password:";
			//
			//txtUsername
			//
			this.txtUsername.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.txtUsername.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtUsername.Location = new System.Drawing.Point(390, 92);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(256, 23);
			this.txtUsername.TabIndex = 2;
			//
			//txtPassword
			//
			this.txtPassword.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.txtPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPassword.Location = new System.Drawing.Point(390, 134);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = Strings.ChrW(42);
			this.txtPassword.Size = new System.Drawing.Size(256, 23);
			this.txtPassword.TabIndex = 3;
			this.txtPassword.Text = "default";
			//
			//btnLogIn
			//
			this.btnLogIn.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnLogIn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnLogIn.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnLogIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLogIn.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.btnLogIn.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnLogIn.Location = new System.Drawing.Point(390, 174);
			this.btnLogIn.Name = "btnLogIn";
			this.btnLogIn.Size = new System.Drawing.Size(115, 31);
			this.btnLogIn.TabIndex = 4;
			this.btnLogIn.Text = "Log In";
			this.btnLogIn.UseVisualStyleBackColor = false;
			this.btnLogIn.Click += new System.EventHandler(this.btnLogIn_Click);
			//
			//btnCancel
			//
			this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnCancel.Location = new System.Drawing.Point(529, 174);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(117, 31);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			//
			//pnlPicture
			//
			this.pnlPicture.Controls.Add(this.pbxPicture);
			this.pnlPicture.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlPicture.Location = new System.Drawing.Point(0, 0);
			this.pnlPicture.Name = "pnlPicture";
			this.pnlPicture.Size = new System.Drawing.Size(302, 212);
			this.pnlPicture.TabIndex = 7;
			//
			//pbxPicture
			//
			this.pbxPicture.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbxPicture.Image = (System.Drawing.Image)resources.GetObject("pbxPicture.Image");
			this.pbxPicture.Location = new System.Drawing.Point(0, 0);
			this.pbxPicture.Name = "pbxPicture";
			this.pbxPicture.Size = new System.Drawing.Size(302, 212);
			this.pbxPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pbxPicture.TabIndex = 6;
			this.pbxPicture.TabStop = false;
			//
			//label2
			//
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Trebuchet MS", 20.25f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.label2.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.label2.Location = new System.Drawing.Point(523, 54);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(123, 35);
			this.label2.TabIndex = 10;
			this.label2.Text = "Welcome";
			//
			//label3
			//
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.label3.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.label3.Location = new System.Drawing.Point(356, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(234, 38);
			this.label3.TabIndex = 11;
			this.label3.Text = "Clinic Manager";
			//
			//pictureBox1
			//
			this.pictureBox1.Location = new System.Drawing.Point(594, 9);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(52, 42);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 12;
			this.pictureBox1.TabStop = false;
			//
			//frmLogIn
			//
			this.AcceptButton = this.btnLogIn;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(652, 212);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.pnlPicture);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLogIn);
			this.Controls.Add(this.txtPassword);
			this.Controls.Add(this.txtUsername);
			this.Controls.Add(this.lblPassword);
			this.Controls.Add(this.lblUsername);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmLogIn";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Clinic Manager - Log In";
			this.pnlPicture.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.pbxPicture).EndInit();
			((System.ComponentModel.ISupportInitialize)this.pictureBox1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Button btnLogIn;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.PictureBox pbxPicture;
		private System.Windows.Forms.Panel pnlPicture;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}
