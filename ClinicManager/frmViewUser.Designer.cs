using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmViewUser
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnView = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.cmbUserType = new System.Windows.Forms.ComboBox();
			this.lblName = new System.Windows.Forms.Label();
			this.txtSurname = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.lblNationalIDNo = new System.Windows.Forms.Label();
			this.lblUsername = new System.Windows.Forms.Label();
			this.lblUserType = new System.Windows.Forms.Label();
			this.lblSurname = new System.Windows.Forms.Label();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.txtNationalIDNo = new System.Windows.Forms.TextBox();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdDrugs = new System.Windows.Forms.DataGridView();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdDrugs).BeginInit();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 7;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 8;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSave);
			this.gbxButtons.Controls.Add(this.btnDelete);
			this.gbxButtons.Controls.Add(this.btnEdit);
			this.gbxButtons.Controls.Add(this.btnView);
			this.gbxButtons.Controls.Add(this.btnClose);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSave
			// 
			this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSave.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSave.Enabled = false;
			this.btnSave.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSave.FlatAppearance.BorderSize = 0;
			this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSave.Location = new System.Drawing.Point(328, 22);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(94, 33);
			this.btnSave.TabIndex = 6;
			this.btnSave.Text = "Save";
			this.btnSave.UseVisualStyleBackColor = false;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnDelete.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnDelete.Enabled = false;
			this.btnDelete.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnDelete.FlatAppearance.BorderSize = 0;
			this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDelete.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnDelete.Location = new System.Drawing.Point(442, 22);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(94, 33);
			this.btnDelete.TabIndex = 7;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = false;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnEdit.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnEdit.Enabled = false;
			this.btnEdit.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnEdit.FlatAppearance.BorderSize = 0;
			this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnEdit.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnEdit.Location = new System.Drawing.Point(556, 22);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(94, 33);
			this.btnEdit.TabIndex = 8;
			this.btnEdit.Text = "Edit";
			this.btnEdit.UseVisualStyleBackColor = false;
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnView
			// 
			this.btnView.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnView.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnView.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnView.FlatAppearance.BorderSize = 0;
			this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnView.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnView.Location = new System.Drawing.Point(670, 22);
			this.btnView.Name = "btnView";
			this.btnView.Size = new System.Drawing.Size(94, 33);
			this.btnView.TabIndex = 9;
			this.btnView.Text = "View";
			this.btnView.UseVisualStyleBackColor = false;
			this.btnView.Click += new System.EventHandler(this.btnView_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(784, 22);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(94, 33);
			this.btnClose.TabIndex = 10;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 10;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.cmbUserType);
			this.gbxFields.Controls.Add(this.lblName);
			this.gbxFields.Controls.Add(this.txtSurname);
			this.gbxFields.Controls.Add(this.txtName);
			this.gbxFields.Controls.Add(this.lblPassword);
			this.gbxFields.Controls.Add(this.lblNationalIDNo);
			this.gbxFields.Controls.Add(this.lblUsername);
			this.gbxFields.Controls.Add(this.lblUserType);
			this.gbxFields.Controls.Add(this.lblSurname);
			this.gbxFields.Controls.Add(this.txtPassword);
			this.gbxFields.Controls.Add(this.txtUsername);
			this.gbxFields.Controls.Add(this.txtNationalIDNo);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 182);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// cmbUserType
			// 
			this.cmbUserType.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbUserType.DisplayMember = "Text";
			this.cmbUserType.FormattingEnabled = true;
			this.cmbUserType.Items.AddRange(new object[] {
				"admin",
				"standard"
			});
			this.cmbUserType.Location = new System.Drawing.Point(130, 86);
			this.cmbUserType.Name = "cmbUserType";
			this.cmbUserType.Size = new System.Drawing.Size(266, 23);
			this.cmbUserType.TabIndex = 2;
			this.cmbUserType.Text = "standard";
			this.cmbUserType.ValueMember = "Text";
			// 
			// lblName
			// 
			this.lblName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(83, 145);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(42, 15);
			this.lblName.TabIndex = 35;
			this.lblName.Text = "Name:";
			// 
			// txtSurname
			// 
			this.txtSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtSurname.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtSurname.Location = new System.Drawing.Point(612, 140);
			this.txtSurname.Name = "txtSurname";
			this.txtSurname.Size = new System.Drawing.Size(266, 23);
			this.txtSurname.TabIndex = 5;
			// 
			// txtName
			// 
			this.txtName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtName.Location = new System.Drawing.Point(130, 140);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(266, 23);
			this.txtName.TabIndex = 4;
			// 
			// lblPassword
			// 
			this.lblPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(547, 39);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(60, 15);
			this.lblPassword.TabIndex = 25;
			this.lblPassword.Text = "Password:";
			// 
			// lblNationalIDNo
			// 
			this.lblNationalIDNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNationalIDNo.AutoSize = true;
			this.lblNationalIDNo.Location = new System.Drawing.Point(519, 91);
			this.lblNationalIDNo.Name = "lblNationalIDNo";
			this.lblNationalIDNo.Size = new System.Drawing.Size(88, 15);
			this.lblNationalIDNo.TabIndex = 26;
			this.lblNationalIDNo.Text = "National ID No.";
			// 
			// lblUsername
			// 
			this.lblUsername.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblUsername.AutoSize = true;
			this.lblUsername.Location = new System.Drawing.Point(62, 39);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(63, 15);
			this.lblUsername.TabIndex = 27;
			this.lblUsername.Text = "Username:";
			// 
			// lblUserType
			// 
			this.lblUserType.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblUserType.AutoSize = true;
			this.lblUserType.Location = new System.Drawing.Point(63, 91);
			this.lblUserType.Name = "lblUserType";
			this.lblUserType.Size = new System.Drawing.Size(62, 15);
			this.lblUserType.TabIndex = 28;
			this.lblUserType.Text = "User Type:";
			// 
			// lblSurname
			// 
			this.lblSurname.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblSurname.AutoSize = true;
			this.lblSurname.Location = new System.Drawing.Point(550, 145);
			this.lblSurname.Name = "lblSurname";
			this.lblSurname.Size = new System.Drawing.Size(57, 15);
			this.lblSurname.TabIndex = 29;
			this.lblSurname.Text = "Surname:";
			// 
			// txtPassword
			// 
			this.txtPassword.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPassword.Enabled = false;
			this.txtPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPassword.Location = new System.Drawing.Point(612, 34);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(266, 23);
			this.txtPassword.TabIndex = 30;
			this.txtPassword.Text = "default";
			// 
			// txtUsername
			// 
			this.txtUsername.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtUsername.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtUsername.Location = new System.Drawing.Point(130, 34);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(266, 23);
			this.txtUsername.TabIndex = 1;
			// 
			// txtNationalIDNo
			// 
			this.txtNationalIDNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNationalIDNo.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNationalIDNo.Location = new System.Drawing.Point(612, 86);
			this.txtNationalIDNo.Name = "txtNationalIDNo";
			this.txtNationalIDNo.Size = new System.Drawing.Size(266, 23);
			this.txtNationalIDNo.TabIndex = 3;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdDrugs);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 250);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 179);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			// 
			// dgdDrugs
			// 
			this.dgdDrugs.AllowUserToAddRows = false;
			this.dgdDrugs.AllowUserToDeleteRows = false;
			this.dgdDrugs.AllowUserToOrderColumns = true;
			this.dgdDrugs.BackgroundColor = System.Drawing.Color.White;
			this.dgdDrugs.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdDrugs.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdDrugs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdDrugs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdDrugs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdDrugs.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdDrugs.Location = new System.Drawing.Point(3, 19);
			this.dgdDrugs.Name = "dgdDrugs";
			this.dgdDrugs.ReadOnly = true;
			this.dgdDrugs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdDrugs.RowHeadersVisible = false;
			this.dgdDrugs.Size = new System.Drawing.Size(880, 157);
			this.dgdDrugs.TabIndex = 0;
			this.dgdDrugs.TabStop = false;
			// 
			// frmViewUser
			// 
			this.AcceptButton = this.btnView;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmViewUser";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Drug Input";
			this.Activated += new System.EventHandler(this.frmViewDrug_Activated);
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdDrugs).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		public System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdDrugs;
		public System.Windows.Forms.Button btnSave;
		public System.Windows.Forms.Button btnDelete;
		public System.Windows.Forms.Button btnEdit;
		public System.Windows.Forms.Button btnView;
		public System.Windows.Forms.Button btnClose;
		public System.Windows.Forms.ComboBox cmbUserType;
		private System.Windows.Forms.Label lblName;
		public System.Windows.Forms.TextBox txtSurname;
		public System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.Label lblNationalIDNo;
		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.Label lblUserType;
		private System.Windows.Forms.Label lblSurname;
		public System.Windows.Forms.TextBox txtPassword;
		public System.Windows.Forms.TextBox txtUsername;
		public System.Windows.Forms.TextBox txtNationalIDNo;
	}
}
