using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmViewPractitioner : Form, IUpdatesDashboard
	{

		private DataTable tablePractitionerStatus;
		public frmViewPractitioner(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			FillDataGrid();

			EntityChange += handler;
		}

		#region "Common Methods"

		private void InitializeDataTables()
		{
			tablePractitionerStatus = (new DataAccess().ReadPractitionerStatus()).Tables[0];
			if (tablePractitionerStatus != null) {
				this.cmbPractitionerStatus.DisplayMember = "STATUSCODE";
				this.cmbPractitionerStatus.ValueMember = "STATUSCODE";
				this.cmbPractitionerStatus.DataSource = tablePractitionerStatus;
			}
		}


		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllPractitionerRecords();
			dgdPractitioners.DataSource = ds;
			dgdPractitioners.DataMember = ds.Tables[0].ToString();
		}

		private void SetFormControls()
		{
			txtPractitionerCode.Enabled = true;
			txtPractitionerName.Enabled = false;
			cmbPractitionerStatus.Enabled = false;
			txtQualification.Enabled = false;
			txtEmailAddress.Enabled = false;
			txtTelNo.Enabled = false;
			txtPhysicalAddress.Enabled = false;
			txtStreet.Enabled = false;
			cmbCountry.Enabled = false;
			cmbCity.Enabled = false;

			btnEdit.Enabled = true;
			btnDelete.Enabled = true;
			btnSave.Enabled = false;

			txtPractitionerCode.SelectAll();
			txtPractitionerCode.Focus();

			btnView.Text = "View";
		}

		private void ResetFormControls()
		{
			txtPractitionerCode.SelectAll();
			txtPractitionerCode.Focus();
			txtPractitionerCode.Text = "";
			txtPractitionerName.Text = "";
			cmbPractitionerStatus.Text = "";
			txtQualification.Text = "";
			txtEmailAddress.Text = "";
			txtTelNo.Text = "";
			txtPhysicalAddress.Text = "";
			txtStreet.Text = "";
			cmbCountry.Text = Convert.ToString(cmbCountry.Items[cmbCountry.Items.Count - 1]);
			cmbCity.Text = Convert.ToString(cmbCity.Items[0]);
		}

		public event EventHandler EntityChange;
		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnView_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtPractitionerCode.Text)) {
				MessageBox.Show("Please enter Practitioner Code.", "Missing Input");
				ResetFormControls();
				return;
			}

			DataSet ds = new DataAccess().ReadPractitionerRecord(this);

			if (ds.Tables[0].Rows.Count > 0) {
				txtPractitionerName.Text = ds.Tables[0].Rows[0]["NAME"].ToString();
				cmbPractitionerStatus.Text = ds.Tables[0].Rows[0]["PractitionerSTATUS"].ToString();
				txtQualification.Text = ds.Tables[0].Rows[0]["QUALIFICATION"].ToString();
				txtTelNo.Text = ds.Tables[0].Rows[0]["TELNO"].ToString();
				txtEmailAddress.Text = ds.Tables[0].Rows[0]["EMAILADDRESS"].ToString();
				txtPhysicalAddress.Text = ds.Tables[0].Rows[0]["ADDRESS"].ToString();
				txtStreet.Text = ds.Tables[0].Rows[0]["STREET"].ToString();
				cmbCity.SelectedItem = ds.Tables[0].Rows[0]["CITY"].ToString();
				cmbCountry.SelectedItem = ds.Tables[0].Rows[0]["TELNO"].ToString();

				SetFormControls();
			} else {
				MessageBox.Show("The Practitioner Code you entered does not exist in the system.", "Invalid Input");
				ResetFormControls();
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			txtPractitionerCode.Enabled = false;
			txtPractitionerName.Enabled = true;
			cmbPractitionerStatus.Enabled = true;
			txtQualification.Enabled = true;
			txtEmailAddress.Enabled = true;
			txtTelNo.Enabled = true;
			txtPhysicalAddress.Enabled = true;
			txtStreet.Enabled = true;
			cmbCountry.Enabled = true;
			cmbCity.Enabled = true;

			btnEdit.Enabled = false;
			btnDelete.Enabled = false;
			btnSave.Enabled = true;

			btnView.Text = "Reset";
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to completely remove this record from the system?", "Confirm Delete Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
				DataAccess dataAccess = new DataAccess();
				dataAccess.DeletePractitionerRecord(this);
				if (EntityChange != null) {
					EntityChange(null, null);
				}
				FillDataGrid();
				ResetFormControls();
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtPractitionerCode.Text)) {
				MessageBox.Show("Please enter the Practitioner Code.", "Missing Input");
				this.txtPractitionerCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPractitionerName.Text)) {
				MessageBox.Show("Please enter the Practitioner's Name.", "Missing Input");
				this.txtPractitionerName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.cmbPractitionerStatus.Text)) {
				MessageBox.Show("Please enter the Practitioner's status.", "Missing Input");
				this.cmbPractitionerStatus.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtQualification.Text)) {
				MessageBox.Show("Please enter the Qualification for the Practitioner.", "Missing Input");
				this.txtQualification.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtTelNo.Text)) {
				MessageBox.Show("Please enter the Practitioner's telephone number.", "Missing Input");
				this.txtTelNo.Focus();
				return;
			}
			int telNo = 0;
			if (!(int.TryParse(txtTelNo.Text, out telNo))) {
				MessageBox.Show("Only numeric values permitted for telephone number.", "Invalid Input");
				txtTelNo.SelectAll();
				txtTelNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPhysicalAddress.Text)) {
				MessageBox.Show("Please enter the Practitioner's physical address.", "Missing Input");
				this.txtPhysicalAddress.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.UpdatePractitionerRecord(this);
			FillDataGrid();
			SetFormControls();
		}

		private void frmViewPractitioner_Activated(object sender, EventArgs e)
		{
			this.FillDataGrid();
		}
	}
}
