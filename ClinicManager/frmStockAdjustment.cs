using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmStockAdjustment : Form, IUpdatesDashboard
	{
		public DataSet ds;
		public DataTable tableAddedBatches;

		private DataTable tableSuppliers;
		public frmStockAdjustment(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			dgdAddedBatches.DataSource = ds;
			dgdAddedBatches.DataMember = ds.Tables["Batch"].ToString();

			EntityChange += handler;
		}


		#region "Common Methods"

		private void ClearForm()
		{
			cmbSupplierCode.Text = "";
			txtSupplierName.Text = "";
			txtSupplierStatus.Text = "";
			txtContactPerson.Text = "";
			txtCountry.Text = "";
			txtCity.Text = "";
			tableAddedBatches.Rows.Clear();
			dgdAddedBatches.Update();

			cmbSupplierCode.Focus();
		}

		private void InitializeDataTables()
		{
			ds = new DataSet();

			tableAddedBatches = new DataTable("Batch");
			DataColumn drugCodeCol = tableAddedBatches.Columns.Add("Drug Code", typeof(string));
			drugCodeCol.AllowDBNull = false;
			DataColumn drugNameCol = tableAddedBatches.Columns.Add("Drug Name", typeof(string));
			drugNameCol.AllowDBNull = false;
			DataColumn batchNoCol = tableAddedBatches.Columns.Add("Batch No.", typeof(string));
			batchNoCol.AllowDBNull = false;
			DataColumn quantityCol = tableAddedBatches.Columns.Add("Quantity", typeof(Int32));
			quantityCol.AllowDBNull = false;
			DataColumn expiryCol = tableAddedBatches.Columns.Add("Expiry Date", typeof(DateTime));
			expiryCol.AllowDBNull = false;
			DataColumn noteCol = tableAddedBatches.Columns.Add("Notes", typeof(string));
			ds.Tables.Add(tableAddedBatches);

			tableSuppliers = (new DataAccess().ReadAllSupplierRecords()).Tables[0];
			if (tableSuppliers != null) {
				this.cmbSupplierCode.DisplayMember = "SUPPLIERCODE";
				this.cmbSupplierCode.ValueMember = "SUPPLIERCODE";
				this.cmbSupplierCode.DataSource = tableSuppliers;
			}
		}

		public event EventHandler EntityChange;

		#endregion


		private void cmbSupplierCode_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbSupplierCode.Text)) {
				return;
			}
			string query = string.Format("SUPPLIERCODE = '{0}'", cmbSupplierCode.SelectedValue);
			DataRow[] selectedSupplier = tableSuppliers.Select(query);
			this.txtSupplierName.Text = selectedSupplier[0]["NAME"].ToString();
			this.txtSupplierStatus.Text = selectedSupplier[0]["SUPPLIERSTATUS"].ToString();
			this.txtContactPerson.Text = selectedSupplier[0]["CONTACTPERSON"].ToString();
			this.txtCity.Text = selectedSupplier[0]["CITY"].ToString();
			this.txtCountry.Text = selectedSupplier[0]["COUNTRY"].ToString();
		}

		private void btnAddBatches_Click(object sender, EventArgs e)
		{
			frmAddAdjustments form = new frmAddAdjustments(ref this.tableAddedBatches, ref this.dgdAddedBatches);
			form.ShowDialog();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbSupplierCode.Text)) {
				MessageBox.Show("Please enter Supplier Code.", "Missing Input");
				return;
			}
			if (tableAddedBatches.Rows.Count < 1) {
				MessageBox.Show("Please add at least 1 batch.", "Missing Input");
				return;
			}
			if (!(new DataAccess().SaveStockAdjustmentTransaction(this))) {
				return;
			}
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.Close();
		}

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbSupplierCode.Text)) {
				MessageBox.Show("Please enter Supplier Code.", "Missing Input");
				return;
			}
			if (tableAddedBatches.Rows.Count < 1) {
				MessageBox.Show("Please add at least 1 batch.", "Missing Input");
				return;
			}
			if (!(new DataAccess().SaveStockAdjustmentTransaction(this))) {
				return;
			}
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.ClearForm();
		}
	}
}
