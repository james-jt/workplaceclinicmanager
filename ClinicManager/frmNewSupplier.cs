using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmNewSupplier : Form
	{

		private DataTable tableSupplierStatus;
		public frmNewSupplier(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			this.gbxButtons.BackColor = this.BackColor;
			this.gbxDataGrid.BackColor = this.BackColor;
			this.gbxFields.BackColor = this.BackColor;
			this.dgdSuppliers.BackgroundColor = this.BackColor;
			this.dgdSuppliers.GridColor = this.BackColor;
			FillDataGrid();

			EntityChange += handler;
		}

		#region "Common Methods"

		private void InitializeDataTables()
		{
			tableSupplierStatus = (new DataAccess().ReadSupplierStatus()).Tables[0];
			if (tableSupplierStatus != null) {
				this.cmbSupplierStatus.DisplayMember = "STATUSCODE";
				this.cmbSupplierStatus.ValueMember = "STATUSCODE";
				this.cmbSupplierStatus.DataSource = tableSupplierStatus;
			}
		}

		private void ClearForm()
		{
			txtSupplierCode.Text = "";
			txtSupplierName.Text = "";
			cmbSupplierStatus.Text = "";
			txtContactPerson.Text = "";
			txtEmailAddress.Text = "";
			txtTelNo.Text = "";
			txtPhysicalAddress.Text = "";
			txtStreet.Text = "";
			cmbCountry.Text = Convert.ToString(cmbCountry.Items[cmbCountry.Items.Count - 1]);
			cmbCity.Text = Convert.ToString(cmbCity.Items[0]);

			txtSupplierCode.Focus();
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllSupplierRecords();
			dgdSuppliers.DataSource = ds;
			dgdSuppliers.DataMember = ds.Tables[0].ToString();
		}

		public event EventHandler EntityChange;
		#endregion

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtSupplierCode.Text)) {
				MessageBox.Show("Please enter the Supplier Code.", "Missing Input");
				this.txtSupplierCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtSupplierName.Text)) {
				MessageBox.Show("Please enter the Supplier's Name.", "Missing Input");
				this.txtSupplierName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.cmbSupplierStatus.Text)) {
				MessageBox.Show("Please enter the Supplier's status.", "Missing Input");
				this.cmbSupplierStatus.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtContactPerson.Text)) {
				MessageBox.Show("Please enter the Contact Person for the Supplier.", "Missing Input");
				this.txtContactPerson.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtTelNo.Text)) {
				MessageBox.Show("Please enter the Supplier's telephone number.", "Missing Input");
				this.txtTelNo.Focus();
				return;
			}
			int telNo = 0;
			if (!(int.TryParse(txtTelNo.Text, out telNo))) {
				MessageBox.Show("Only numeric values permitted for telephone number.", "Invalid Input");
				txtTelNo.SelectAll();
				txtTelNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPhysicalAddress.Text)) {
				MessageBox.Show("Please enter the Supplier's physical address.", "Missing Input");
				this.txtPhysicalAddress.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreateSupplierRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.Close();
		}

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtSupplierCode.Text)) {
				MessageBox.Show("Please enter the Supplier Code.", "Missing Input");
				this.txtSupplierCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtSupplierName.Text)) {
				MessageBox.Show("Please enter the Supplier's Name.", "Missing Input");
				this.txtSupplierName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.cmbSupplierStatus.Text)) {
				MessageBox.Show("Please enter the Supplier's status.", "Missing Input");
				this.cmbSupplierStatus.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtContactPerson.Text)) {
				MessageBox.Show("Please enter the Contact Person for the Supplier.", "Missing Input");
				this.txtContactPerson.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtTelNo.Text)) {
				MessageBox.Show("Please enter the Supplier's telephone number.", "Missing Input");
				this.txtTelNo.Focus();
				return;
			}
			int telNo = 0;
			if (!(int.TryParse(txtTelNo.Text, out telNo))) {
				MessageBox.Show("Only numeric values permitted for telephone number.", "Invalid Input");
				txtTelNo.SelectAll();
				txtTelNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtPhysicalAddress.Text)) {
				MessageBox.Show("Please enter the Supplier's physical address.", "Missing Input");
				this.txtPhysicalAddress.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreateSupplierRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			FillDataGrid();
			ClearForm();
		}
	}
}
