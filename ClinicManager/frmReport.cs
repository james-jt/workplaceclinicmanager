using Microsoft.VisualBasic;
using System;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmReport : Form
	{
		public DataSet dataSet;
		private System.Drawing.Font printFont;

		private StreamReader streamToPrint;
		public frmReport()
		{
			InitializeComponent();
			dataSet = new DataSet();
		}

		public void FormatDataGridView()
		{
			this.dgdRptContent.AdvancedColumnHeadersBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.Single;
			this.dgdRptContent.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font(this.dgdRptContent.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
			this.dgdRptContent.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			this.dgdRptContent.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
			this.dgdRptContent.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			this.dgdRptContent.AllowUserToAddRows = false;
			this.dgdRptContent.RowHeadersVisible = false;
			this.dgdRptContent.AutoResizeColumns();

		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			if (dataSet.Tables.Count > 0) {
				ExportReport(dataSet.Tables[0], true);
			} else {
				MessageBox.Show("Report contains no data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void btnExport_Click(object sender, EventArgs e)
		{
			if (dataSet.Tables.Count > 0) {
				ExportReport(dataSet.Tables[0], false);
			} else {
				MessageBox.Show("Report contains no data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private bool ExportReport(DataTable currentTable, bool printing)
		{
			if (currentTable == null) {
				MessageBox.Show("No data selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			string outputFilePath = null;
			outputFilePath = string.Empty;
			if (printing) {
				outputFilePath = lblRptTitle.Text + ".xlsx";
			} else {
				SaveFileDialog fileSaveDialogue = new SaveFileDialog();
				fileSaveDialogue.Filter = "Microsoft Office Xml Excel (*.xlsx)|*.xlsx";
				fileSaveDialogue.ShowDialog();
				if (string.IsNullOrEmpty(fileSaveDialogue.FileName)) {
					return false;
				}
				outputFilePath = fileSaveDialogue.FileName.ToString();
			}
			SLDocument Wbook = PrepareWorkbook(currentTable);
			if (outputFilePath.EndsWith(".xlsx")) {
				try {
					Wbook.SaveAs(outputFilePath);
					if (printing) {
						ProcessStartInfo info = new ProcessStartInfo(outputFilePath.Trim());
						info.Verb = "Print";
						info.CreateNoWindow = true;
						info.WindowStyle = ProcessWindowStyle.Hidden;
						Process.Start(info);
					} else {
						if (MessageBox.Show("{lblRptTitle.Text} successfully saved. Do you wish to open this file now?", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes) {
							System.Diagnostics.Process.Start(outputFilePath);
						}
					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			} else {
				MessageBox.Show("Invalid file extension.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			return true;
		}

		private SLDocument PrepareWorkbook(DataTable currentTable)
		{
			SLDocument wBook = new SLDocument();
			wBook.DeleteWorksheet("Sheet1");
			wBook.AddWorksheet(lblRptTitle.Text);
			int iStartRowIndex = 8;
			int iStartColumnIndex = 1;
			wBook.ImportDataTable(iStartRowIndex, iStartColumnIndex, currentTable, true);

			//Create and set page settings
			SLPageSettings pageSettings = new SLPageSettings();
			pageSettings.Orientation = OrientationValues.Landscape;
			pageSettings.SetTabColor(SLThemeColorIndexValues.Accent1Color);
			wBook.SetPageSettings(pageSettings);

			//Create and add table
			int iEndRowIndex = iStartRowIndex + currentTable.Rows.Count + 1 - 1;
			int iEndColumnIndex = iStartColumnIndex + currentTable.Columns.Count - 1;
			SLTable table = wBook.CreateTable(iStartRowIndex, iStartColumnIndex, iEndRowIndex, iEndColumnIndex);
			table.SetTableStyle(SLTableStyleTypeValues.Light13);
			wBook.InsertTable(table);

			//Create and set company name font
			SLStyle cellStyle = wBook.CreateStyle();
			cellStyle = wBook.GetCellStyle(3, 1);
			cellStyle.SetFontBold(true);
			cellStyle.SetFont("Calibri", 14);
			wBook.SetCellStyle(1, 1, cellStyle);
			wBook.SetCellStyle(2, 1, cellStyle);
			wBook.SetCellStyle(3, 1, cellStyle);
			wBook.SetCellStyle(iEndRowIndex + 2, 1, cellStyle);

			//Add report header
			wBook.SetCellValue(1, 1, "Kango Products");
			wBook.SetCellValue(2, 1, "Clinic Manager");
			wBook.SetCellValue(3, 1, lblRptTitle.Text);

			wBook.SetCellValue(4, 1, "Date : ");
			wBook.SetCellValue(5, 1, "Time : ");
			wBook.SetCellValue(4, 2, System.DateTime.Now.ToLongDateString());
			wBook.SetCellValue(5, 2, System.DateTime.Now.ToLongTimeString());

			//Add report footer
			wBook.SetCellValue(iEndRowIndex + 2, 1, "Signature:");
			return wBook;
		}
	}
}
