using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmChangePassword : Form
	{
		private bool resetAccount = false;
		public frmChangePassword(bool resetAccount)
		{
			InitializeComponent();
			if (resetAccount) {
				this.resetAccount = resetAccount;
				this.txtCurrentPassword.Text = "default";
				this.txtCurrentPassword.Enabled = false;
			}
		}

		private void btnChangePassword_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtCurrentPassword.Text)) {
				MessageBox.Show("Please enter your current password.", "Missing Input");
				return;
			}
			if (string.IsNullOrWhiteSpace(txtNewPassword.Text)) {
				MessageBox.Show("Please enter the new password.", "Missing Input");
				return;
			}
			if (string.IsNullOrWhiteSpace(txtConfirmPassword.Text)) {
				MessageBox.Show("Please confirm your new password.", "Missing Input");
				return;
			}
			if (txtCurrentPassword.Text == txtNewPassword.Text) {
				MessageBox.Show("Please enter a password different from your current.", "Incorrect Input");
				return;
			}
			if (txtNewPassword.Text != txtConfirmPassword.Text) {
				MessageBox.Show("The password entered for confirmation does not match the new password. Please try again.", "Incorrect Input");
				return;
			}
			if (txtNewPassword.Text.Length < 8) {
				MessageBox.Show("Password must be at least 8 characters in length.", "Password Too Short");
				return;
			}
			DataTable tableUsers = new DataAccess().ReadAllUserRecords().Tables[0];
			string query = string.Format("USERNAME = '{0}'", Program.userName);
			DataRow[] dr = tableUsers.Select(query);
			if (dr[0]["PASSWORD"].ToString() != txtCurrentPassword.Text) {
				MessageBox.Show("Your current password is incorrect.", "Incorrect Input");
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.ChangePassword(txtNewPassword.Text);
			this.Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			if (this.resetAccount) {
				Program.userName = string.Empty;
			}
			this.Close();
		}
	}
}
