using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmNewDrug : Form, IUpdatesDashboard
	{

		private DataTable tableSuppliers;
		public frmNewDrug(EventHandler handler)
		{
			InitializeComponent();
			InitializeDataTables();
			this.gbxButtons.BackColor = this.BackColor;
			this.gbxDataGrid.BackColor = this.BackColor;
			this.gbxFields.BackColor = this.BackColor;
			this.dgdDrugs.BackgroundColor = this.BackColor;
			this.dgdDrugs.GridColor = this.BackColor;
			FillDataGrid();
			EntityChange += handler;
		}

		#region "Common Methods"

		private void InitializeDataTables()
		{
			tableSuppliers = (new DataAccess().ReadAllSupplierRecords()).Tables[0];
			if (tableSuppliers != null) {
				this.cmbPreferredSupplier.DisplayMember = "SUPPLIERCODE";
				this.cmbPreferredSupplier.ValueMember = "SUPPLIERCODE";
				this.cmbPreferredSupplier.DataSource = tableSuppliers;
			}
		}

		private void ClearForm()
		{
			txtDrugCode.Text = "";
			txtDrugName.Text = "";
			cmbPreferredSupplier.Text = "";
			txtCategory.Text = "";
			txtNotes.Text = "";
			txtReorderLevel.Text = "";

			txtDrugCode.Focus();
		}

		private void FillDataGrid()
		{
			DataSet ds = new DataSet();
			ds = new DataAccess().ReadAllDrugRecords();
			dgdDrugs.DataSource = ds;
			dgdDrugs.DataMember = ds.Tables[0].ToString();
		}

		public event EventHandler EntityChange;

		#endregion

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtDrugCode.Text)) {
				MessageBox.Show("Please enter the drug code.", "Missing Input");
				this.txtDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtDrugName.Text)) {
				MessageBox.Show("Please enter the drug name.", "Missing Input");
				this.txtDrugName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtCategory.Text)) {
				MessageBox.Show("Please enter the drug's category.", "Missing Input");
				this.txtCategory.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtReorderLevel.Text)) {
				MessageBox.Show("Please enter the drug's re-order level.", "Missing Input");
				this.txtReorderLevel.Focus();
				return;
			}
			int reOrderLevel = 0;
			if (!(int.TryParse(txtReorderLevel.Text, out reOrderLevel))) {
				MessageBox.Show("Only numeric (integer) values permitted for reorder level.", "Invalid Input");
				txtReorderLevel.SelectAll();
				txtReorderLevel.Focus();
				return;
			}
			if (reOrderLevel < 1) {
				MessageBox.Show("Reorder level must be greater than zero.", "Invalid Input");
				txtReorderLevel.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreateDrugRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			this.Close();
		}

		private void btnSaveAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.txtDrugCode.Text)) {
				MessageBox.Show("Please enter the drug code.", "Missing Input");
				this.txtDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtDrugName.Text)) {
				MessageBox.Show("Please enter the drug name.", "Missing Input");
				this.txtDrugName.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtCategory.Text)) {
				MessageBox.Show("Please enter the drug's category.", "Missing Input");
				this.txtCategory.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(this.txtReorderLevel.Text)) {
				MessageBox.Show("Please enter the drug's re-order level.", "Missing Input");
				this.txtReorderLevel.Focus();
				return;
			}
			int reOrderLevel = 0;
			if (!(int.TryParse(txtReorderLevel.Text, out reOrderLevel))) {
				MessageBox.Show("Only numeric (integer) values permitted for reorder level.", "Invalid Input");
				txtReorderLevel.SelectAll();
				txtReorderLevel.Focus();
				return;
			}
			if (reOrderLevel < 1) {
				MessageBox.Show("Reorder level must be greater than zero.", "Invalid Input");
				txtReorderLevel.Focus();
				return;
			}
			DataAccess dataAccess = new DataAccess();
			dataAccess.CreateDrugRecord(this);
			if (EntityChange != null) {
				EntityChange(null, null);
			}
			FillDataGrid();
			ClearForm();
		}
	}
}
