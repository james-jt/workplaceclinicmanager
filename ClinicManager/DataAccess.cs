using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
namespace Pharmacy
{

	class DataAccess
	{
		private string connectionString;
		private SqlConnection connection = null;
		private string sqlCommand = string.Empty;

		private SqlCommand command;
		public DataAccess()
		{
			connectionString = ConfigurationManager.ConnectionStrings["CLINICConnectionString"].ConnectionString;
			SqlConnection testConnection = new SqlConnection(connectionString);
			try {
				testConnection.Open();
			} catch (Exception e) {
				MessageBox.Show(e.Message);
				return;
			}
			connection = testConnection;
			command = connection.CreateCommand();
			command.CommandType = CommandType.Text;
		}


		#region "Common Methods"

		private DataSet ExecuteDatabaseQuery(string query)
		{
			if (connection == null) {
				return null;
			}
			try {
				command.CommandText = query;
				SqlDataAdapter da = new SqlDataAdapter(command);
				DataSet resultTable = new DataSet();
				da.Fill(resultTable);
				return resultTable;
			} catch {
				return null;
			}
		}

		private string ChangeDateFormat(DateTime date)
		{
			return date.Year + "/" + date.Month + "/" + date.Day;
		}

		#endregion


		#region "Users Methods"

		public void CreateUserRecord(frmNewUser sender)
		{
			sqlCommand = String.Format("SELECT USERNAME FROM PHUSER WHERE USERNAME = '{0}';", sender.txtUsername.Text.ToUpper());
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds.Tables[0].Rows.Count > 0) {
				MessageBox.Show("The Username entered is already in use, try another Username.", "Operation Aborted");
				return;
			}

			sqlCommand = string.Format("INSERT INTO PHUSER VALUES ('{0}', '{1}',  '{2}', '{3}', '{4}', '{5}', '{6}', '{7}');", sender.txtUsername.Text.ToUpper(), sender.txtPassword.Text, sender.cmbUserType.SelectedValue, sender.txtName.Text, sender.txtSurname.Text, sender.txtNationalIDNo.Text, Program.userName, ChangeDateFormat(System.DateTime.Today));

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("User record successfully created.", "Operation Sucessful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public DataSet ReadAllUserRecords()
		{
			sqlCommand = "SELECT * FROM PHUSER";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadUserRecord(frmViewUser sender)
		{
			sqlCommand = string.Format("SELECT * FROM PHUSER WHERE USERNAME = '{0}';", sender.txtUsername.Text);
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public void UpdateUserRecord(frmViewUser sender)
		{
			sqlCommand = string.Format("UPDATE PHUSER SET USERTYPE = '{0}', NAME = '{1}', " + Constants.vbCr + Constants.vbLf + "                SURNAME = '{2}', NATIONALID = '{3}' WHERE USERNAME = '{4}';", sender.cmbUserType.SelectedValue, sender.txtName.Text, sender.txtSurname.Text, sender.txtNationalIDNo.Text, sender.txtUsername.Text.ToUpper());
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show(string.Format("Record for User Code {0} successfully updated.", sender.txtUsername.Text), "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void DeleteUserRecord(frmViewUser sender)
		{
			sqlCommand = string.Format("DELETE FROM PHUSER WHERE USERNAME = '{0}';", sender.txtUsername.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Record successfully deleted.", "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void ResetUserPassword(string username)
		{
			sqlCommand = string.Format("UPDATE PHUSER SET PASSWORD = 'default' WHERE USERNAME = '{0}';", username);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				string message = string.Format("Password for {0} successfully reset.", username);
				MessageBox.Show(message);
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void ChangePassword(string newPassword)
		{
			sqlCommand = string.Format("UPDATE PHUSER SET PASSWORD = '{0}' WHERE USERNAME = '{1}';", newPassword, Program.userName);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				string message = string.Format("Password successfully changed.");
				MessageBox.Show(message);
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		#endregion


		#region "Patient Methods"

		public void CreatePatientRecord(frmNewPatient sender)
		{
			sqlCommand = String.Format("SELECT EMPLOYEEID FROM PATIENT WHERE EMPLOYEEID = '{0}';", sender.txtEmpNo.Text);
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds.Tables[0].Rows.Count > 0) {
				MessageBox.Show("The Employee Number entered already exists in the system.", "Operation Aborted");
				return;
			}

			sqlCommand = string.Format("INSERT INTO PATIENT VALUES ('{0}', '{1}',  '{2}', '{3}', '{4}', '{5}', '{6}');", sender.txtEmpNo.Text, sender.txtPatientName.Text, sender.txtPatientSurname.Text, ChangeDateFormat(sender.dtpPatientDOB.Value), sender.txtDpt.Text, Program.userName, ChangeDateFormat(DateTime.Today.Date));

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Patient record successfully created.", "Operation Sucessful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public DataSet ReadAllPatientRecords()
		{
			sqlCommand = "SELECT * FROM PATIENT;";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadPatientRecord(frmViewPatient sender)
		{
			sqlCommand = string.Format("SELECT * FROM PATIENT WHERE EMPLOYEEID = {0};", sender.txtEmpNo.Text);
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public void UpdatePatientRecord(frmViewPatient sender)
		{
			sqlCommand = string.Format("UPDATE PATIENT SET NAME = '{0}', SURNAME = '{1}', DEPARTMENT = '{2}', " + Constants.vbCr + Constants.vbLf + "                DATEOFBIRTH = '{3}' WHERE EMPLOYEEID = '{4}';", sender.txtPatientName.Text, sender.txtPatientSurname.Text, sender.txtDpt.Text, ChangeDateFormat(sender.dtpPatientDOB.Value), sender.txtEmpNo.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show(string.Format("Record for Employee Number {0} successfully updated.", sender.txtEmpNo.Text), "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void DeletePatientRecord(frmViewPatient sender)
		{
			sqlCommand = string.Format("DELETE FROM PATIENT WHERE EMPLOYEEID = '{0}';", sender.txtEmpNo.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Record successfully deleted.", "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		#endregion


		#region "Drug Methods"

		public void CreateDrugRecord(frmNewDrug sender)
		{
			sqlCommand = String.Format("SELECT DRUGCODE FROM DRUG WHERE DRUGCODE = '{0}';", sender.txtDrugCode.Text);
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds.Tables[0].Rows.Count > 0) {
				MessageBox.Show("The Drug Code entered already exists in the system.", "Operation Aborted");
				return;
			}

			sqlCommand = string.Format("INSERT INTO DRUG VALUES ('{0}', '{1}',  '{2}', '{3}', '{4}', '{5}');", sender.txtDrugCode.Text, sender.txtDrugName.Text, sender.txtCategory.Text, sender.txtNotes.Text, sender.cmbPreferredSupplier.SelectedValue, sender.txtReorderLevel.Text);

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Drug record successfully created.", "Operation Sucessful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public DataSet ReadAllDrugRecords()
		{
			sqlCommand = "SELECT * FROM DRUG;";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadDrugRecord(frmViewDrug sender)
		{
			sqlCommand = string.Format("SELECT * FROM DRUG WHERE DRUGCODE = '{0}';", sender.txtDrugCode.Text);
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public void UpdateDrugRecord(frmViewDrug sender)
		{
			sqlCommand = string.Format("UPDATE DRUG SET NAME = '{0}', CATEGORY = '{1}', SPECIALNOTE = '{2}', " + Constants.vbCr + Constants.vbLf + "                PREFERREDSUPPLIER = '{3}', REORDERLEVEL = '{4}' WHERE DRUGCODE = '{5}';", sender.txtDrugName.Text, sender.txtCategory.Text, sender.txtNotes.Text, sender.cmbPreferredSupplier.Text, sender.txtReorderLevel.Text, sender.txtDrugCode.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show(string.Format("Record for Drug Code {0} successfully updated.", sender.txtDrugCode.Text), "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void DeleteDrugRecord(frmViewDrug sender)
		{
			sqlCommand = string.Format("DELETE FROM DRUG WHERE DRUGCODE = '{0}';", sender.txtDrugCode.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Record successfully deleted.", "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public DataTable DrugsPastReorderLevel()
		{
			sqlCommand = " SELECT B.DRUGCODE, SUM(B.QUANTITY) FROM DRUGBATCH B GROUP BY B.DRUGCODE";
			return ExecuteDatabaseQuery(sqlCommand).Tables[0];
		}

		#endregion


		#region "Supplier Methods"

		public void CreateSupplierRecord(frmNewSupplier sender)
		{
			sqlCommand = String.Format("SELECT SUPPLIERCODE FROM SUPPLIER WHERE SUPPLIERCODE = '{0}';", sender.txtSupplierCode.Text);
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds.Tables[0].Rows.Count > 0) {
				MessageBox.Show("The Supplier Code entered already exists in the system.", "Operation Aborted");
				return;
			}

			sqlCommand = string.Format("INSERT INTO SUPPLIER VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}');", sender.txtSupplierCode.Text, sender.cmbSupplierStatus.SelectedValue, sender.txtSupplierName.Text, sender.txtPhysicalAddress.Text, sender.txtStreet.Text, sender.cmbCity.Text, sender.cmbCountry.Text, sender.txtTelNo.Text, sender.txtEmailAddress.Text,
			sender.txtContactPerson.Text);

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Supplier record successfully created.", "Operation Sucessful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public DataSet ReadAllSupplierRecords()
		{
			sqlCommand = "SELECT * FROM SUPPLIER;";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadSupplierRecord(frmViewSupplier sender)
		{
			sqlCommand = string.Format("SELECT * FROM SUPPLIER WHERE SUPPLIERCODE = '{0}';", sender.txtSupplierCode.Text);
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadSupplierStatus()
		{
			sqlCommand = "SELECT * FROM SUPPLIERSTATUS";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public void UpdateSupplierRecord(frmViewSupplier sender)
		{
			sqlCommand = string.Format("UPDATE SUPPLIER SET SUPPLIERSTATUS = '{0}', NAME = '{1}', ADDRESS = '{2}', " + Constants.vbCr + Constants.vbLf + "                STREET = '{3}', CITY = '{4}', COUNTRY = '{5}', TELNO = '{6}', EMAILADDRESS = '{7}', " + Constants.vbCr + Constants.vbLf + "                CONTACTPERSON = '{8}' WHERE SUPPLIERCODE = '{9}';", sender.cmbSupplierStatus.Text, sender.txtSupplierName.Text, sender.txtPhysicalAddress.Text, sender.txtStreet.Text, sender.cmbCity.Text, sender.cmbCountry.Text, sender.txtTelNo.Text, sender.txtEmailAddress.Text, sender.txtContactPerson.Text,
			sender.txtSupplierCode.Text);

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show(string.Format("Record for Supplier Code {0} successfully updated.", sender.txtSupplierCode.Text), "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void DeleteSupplierRecord(frmViewSupplier sender)
		{
			sqlCommand = string.Format("DELETE FROM SUPPLIER WHERE SUPPLIERCODE = '{0}';", sender.txtSupplierCode.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Record successfully deleted.", "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		#endregion


		#region "Practitioner Methods"

		public void CreatePractitionerRecord(frmNewPractitioner sender)
		{
			sqlCommand = String.Format("SELECT PRACTITIONERCODE FROM PRACTITIONER WHERE PRACTITIONERCODE = '{0}';", sender.txtPractitionerCode.Text);
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds.Tables[0].Rows.Count > 0) {
				MessageBox.Show("The Practitioner Code entered already exists in the system.", "Operation Aborted");
				return;
			}

			sqlCommand = string.Format("INSERT INTO PRACTITIONER VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}');", sender.txtPractitionerCode.Text, sender.cmbPractitionerStatus.SelectedValue, sender.txtPractitionerName.Text, sender.txtPhysicalAddress.Text, sender.txtStreet.Text, Convert.ToString(sender.cmbCity.SelectedValue), Convert.ToString(sender.cmbCountry.SelectedValue), sender.txtTelNo.Text, sender.txtEmailAddress.Text,
			sender.txtQualification.Text);

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Practitioner record successfully created.", "Operation Sucessful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public DataSet ReadAllPractitionerRecords()
		{
			sqlCommand = "SELECT * FROM PRACTITIONER;";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadPractitionerRecord(frmViewPractitioner sender)
		{
			sqlCommand = string.Format("SELECT * FROM PRACTITIONER WHERE PRACTITIONERCODE = '{0}';", sender.txtPractitionerCode.Text);
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadPractitionerStatus()
		{
			sqlCommand = "SELECT * FROM PRACTITIONERSTATUS";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public void UpdatePractitionerRecord(frmViewPractitioner sender)
		{
			sqlCommand = string.Format("UPDATE PRACTITIONER SET PRACTITIONERSTATUS = '{0}', NAME = '{1}', ADDRESS = '{2}', " + Constants.vbCr + Constants.vbLf + "                STREET = '{3}', CITY = '{4}', COUNTRY = '{5}', TELNO = '{6}', EMAILADDRESS = '{7}', " + Constants.vbCr + Constants.vbLf + "                QUALIFICATION = '{8}' WHERE PRACTITIONERCODE = '{9}';", sender.cmbPractitionerStatus.Text, sender.txtPractitionerName.Text, sender.txtPhysicalAddress.Text, sender.txtStreet.Text, sender.cmbCity.SelectedValue, sender.cmbCountry.SelectedValue, sender.txtTelNo.Text, sender.txtEmailAddress.Text, sender.txtQualification.Text,
			sender.txtPractitionerCode.Text);

			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show(string.Format("Record for Practitioner Code {0} successfully updated.", sender.txtPractitionerCode.Text), "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		public void DeletePractitionerRecord(frmViewPractitioner sender)
		{
			sqlCommand = string.Format("DELETE FROM PRACTITIONER WHERE PRACTITIONERCODE = '{0}';", sender.txtPractitionerCode.Text);
			command.CommandText = sqlCommand;
			try {
				command.ExecuteNonQuery();
				MessageBox.Show("Record successfully deleted.", "Operation Successful");
			} catch (Exception e) {
				MessageBox.Show(e.Message);
			}
		}

		#endregion


		#region "Transaction Methods"

		public DataSet ReadAllBatchRecords()
		{
			sqlCommand = "SELECT * FROM DRUGBATCH;";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		public bool SaveDispensaryTransaction(frmDispenseDrugs sender)
		{

			string transactionNumber = null;
			string transactionType = null;
			int transactionSequence = 0;

			//Specify transaction type
			transactionType = "DISP";

			//Generate transaction number
			sqlCommand = "SELECT * FROM CONFIGURATION";
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds == null) {
				MessageBox.Show("Unable to obtain last transaction number.", "Possible Database Error");
				return false;
			}
			transactionSequence = Convert.ToInt32(ds.Tables[0].Rows[0]["TRANSACTIONNOSEQUENCE"]) + 1;
			transactionNumber = string.Format("{0}{1}", transactionType, transactionSequence.ToString());
			sqlCommand = string.Format("UPDATE CONFIGURATION SET TRANSACTIONNOSEQUENCE = '{0}'", transactionSequence);
			command.CommandText = sqlCommand;
			command.ExecuteNonQuery();

			//Save transaction entries
			command.Transaction = connection.BeginTransaction();
			try {
				//Insert record into table dispensarytransaction
				sqlCommand = string.Format("INSERT INTO DISPENSARYTRANSACTION VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", transactionNumber, transactionType, sender.txtNote.Text, Program.userName, ChangeDateFormat(System.DateTime.Today), sender.cmbEmpNo.SelectedValue, sender.cmbPractitionerNo.SelectedValue);
				command.CommandText = sqlCommand;
				command.ExecuteNonQuery();

				foreach (DataRow dr in sender.tableAddedDrugs.Rows) {
					int currentQuantity = GetCurrentBatchQuantity(dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					currentQuantity -= Convert.ToInt32(dr["Quantity"]);
					if (currentQuantity < 0) {
						string message = string.Format("Insufficient quantities of drug {0} in batch {1}. Please try another batch.", dr["Drug Code"].ToString(), dr["Batch No."].ToString());
						MessageBox.Show(message);
						command.Transaction.Rollback();
						return false;
					}
					//Update quantities of affected drugs in table drugbatch
					sqlCommand = string.Format("UPDATE DRUGBATCH SET QUANTITY = '{0}' WHERE DRUGCODE = '{1}' AND BATCHNO = '{2}';", currentQuantity, dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();

					//Insert records into table dispensaryitem for each dispensed item
					sqlCommand = string.Format("INSERT INTO DISPENSEITEM VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", transactionNumber, dr["Drug Code"].ToString(), dr["Batch No."].ToString(), dr["Quantity"].ToString(), dr["Notes"].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();
				}
				command.Transaction.Commit();
				return true;
			} catch (Exception e) {
				MessageBox.Show(e.Message);
				command.Transaction.Rollback();
				return false;
			}
		}

		public bool SaveDispensaryReturnsTransaction(frmDispensaryReturns sender)
		{

			string transactionNumber = null;
			string transactionType = null;
			int transactionSequence = 0;

			//Specify transaction type
			transactionType = "RINW";

			//Generate transaction number
			sqlCommand = "SELECT * FROM CONFIGURATION";
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds == null) {
				MessageBox.Show("Unable to obtain last transaction number.", "Possible Database Error");
				return false;
			}
			transactionSequence = Convert.ToInt32(ds.Tables[0].Rows[0]["TRANSACTIONNOSEQUENCE"]) + 1;
			transactionNumber = string.Format("{0}{1}", transactionType, transactionSequence.ToString());
			sqlCommand = string.Format("UPDATE CONFIGURATION SET TRANSACTIONNOSEQUENCE = '{0}'", transactionSequence);
			command.CommandText = sqlCommand;
			command.ExecuteNonQuery();

			//Save transaction entries
			command.Transaction = connection.BeginTransaction();
			try {
				//Insert record into table dispensarytransaction
				sqlCommand = string.Format("INSERT INTO DISPENSARYTRANSACTION VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '')", transactionNumber, transactionType, sender.txtNote.Text, Program.userName, ChangeDateFormat(System.DateTime.Today), sender.cmbEmpNo.SelectedValue);
				command.CommandText = sqlCommand;
				command.ExecuteNonQuery();

				foreach (DataRow dr in sender.tableAddedDrugs.Rows) {
					int currentQuantity = GetCurrentBatchQuantity(dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					currentQuantity += Convert.ToInt32(dr["Quantity"]);

					//Update quantities of affected drugs in table drugbatch
					sqlCommand = string.Format("UPDATE DRUGBATCH SET QUANTITY = '{0}' WHERE DRUGCODE = '{1}' AND BATCHNO = '{2}';", currentQuantity, dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();

					//Insert records into table dispensaryitem for each dispensed item
					sqlCommand = string.Format("INSERT INTO DISPENSEITEM VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", transactionNumber, dr["Drug Code"].ToString(), dr["Batch No."].ToString(), dr["Quantity"].ToString(), dr["Notes"].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();
				}
				command.Transaction.Commit();
				return true;
			} catch (Exception e) {
				MessageBox.Show(e.Message);
				command.Transaction.Rollback();
				return false;
			}
		}

		public bool SaveStockReceiptTransaction(frmStockReceipt sender)
		{

			string transactionNumber = null;
			string transactionType = null;
			int transactionSequence = 0;

			//Specify transaction type
			transactionType = "RECI";

			//Generate transaction number
			sqlCommand = "SELECT * FROM CONFIGURATION";
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds == null) {
				MessageBox.Show("Unable to obtain last transaction number.", "Possible Database Error");
				return false;
			}
			transactionSequence = Convert.ToInt32(ds.Tables[0].Rows[0]["TRANSACTIONNOSEQUENCE"]) + 1;
			transactionNumber = string.Format("{0}{1}", transactionType, transactionSequence.ToString());
			sqlCommand = string.Format("UPDATE CONFIGURATION SET TRANSACTIONNOSEQUENCE = '{0}'", transactionSequence);
			command.CommandText = sqlCommand;
			command.ExecuteNonQuery();

			//Save transaction entries
			command.Transaction = connection.BeginTransaction();
			try {
				//Insert record into table inventorytransaction
				sqlCommand = string.Format("INSERT INTO INVENTORYTRANSACTION VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", transactionNumber, transactionType, sender.txtNote.Text, Program.userName, ChangeDateFormat(System.DateTime.Today), sender.cmbSupplierCode.SelectedValue);
				command.CommandText = sqlCommand;
				command.ExecuteNonQuery();

				foreach (DataRow dr in sender.tableAddedBatches.Rows) {
					//Insert record into table drugbatch
					sqlCommand = string.Format("INSERT INTO DRUGBATCH VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}');", dr["Drug Code"].ToString(), dr["Batch No."].ToString(), sender.cmbSupplierCode.SelectedValue, dr["Quantity"].ToString(), ChangeDateFormat(Convert.ToDateTime(dr["Expiry Date"])), ChangeDateFormat(System.DateTime.Today), Program.userName);
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();

					//Insert records into table inventoryitem for each received item
					sqlCommand = string.Format("INSERT INTO INVENTORYITEM VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", transactionNumber, dr["Drug Code"].ToString(), dr["Batch No."].ToString(), dr["Quantity"].ToString(), dr["Notes"].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();
				}
				command.Transaction.Commit();
				return true;
			} catch (Exception e) {
				MessageBox.Show(e.Message);
				command.Transaction.Rollback();
				return false;
			}
		}

		public bool SaveStockReturnsTransaction(frmStockReturns sender)
		{

			string transactionNumber = null;
			string transactionType = null;
			int transactionSequence = 0;

			//Specify transaction type
			transactionType = "ROUT";

			//Generate transaction number
			sqlCommand = "SELECT * FROM CONFIGURATION";
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds == null) {
				MessageBox.Show("Unable to obtain last transaction number.", "Possible Database Error");
				return false;
			}
			transactionSequence = Convert.ToInt32(ds.Tables[0].Rows[0]["TRANSACTIONNOSEQUENCE"]) + 1;
			transactionNumber = string.Format("{0}{1}", transactionType, transactionSequence.ToString());
			sqlCommand = string.Format("UPDATE CONFIGURATION SET TRANSACTIONNOSEQUENCE = '{0}'", transactionSequence);
			command.CommandText = sqlCommand;
			command.ExecuteNonQuery();

			//Save transaction entries
			command.Transaction = connection.BeginTransaction();
			try {
				//Insert record into table dispensarytransaction
				sqlCommand = string.Format("INSERT INTO INVENTORYTRANSACTION VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", transactionNumber, transactionType, sender.txtNote.Text, Program.userName, ChangeDateFormat(System.DateTime.Today), sender.cmbSupplierCode.SelectedValue);
				command.CommandText = sqlCommand;
				command.ExecuteNonQuery();

				foreach (DataRow dr in sender.tableAddedBatches.Rows) {
					int currentQuantity = GetCurrentBatchQuantity(dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					currentQuantity -= Convert.ToInt32(dr["Quantity"]);
					if (currentQuantity < 0) {
						string message = string.Format("Return quantity of drug {0} in batch {1} exceeds currently available quantity.", dr["Drug Code"].ToString(), dr["Batch No."].ToString());
						MessageBox.Show(message);
						command.Transaction.Rollback();
						return false;
					}
					//Update quantities of affected drugs in table drugbatch
					sqlCommand = string.Format("UPDATE DRUGBATCH SET QUANTITY = '{0}' WHERE DRUGCODE = '{1}' AND BATCHNO = '{2}';", currentQuantity, dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();

					//Insert records into table inventoryitem for each returned item
					sqlCommand = string.Format("INSERT INTO INVENTORYITEM VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", transactionNumber, dr["Drug Code"].ToString(), dr["Batch No."].ToString(), dr["Quantity"].ToString(), dr["Notes"].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();
				}
				command.Transaction.Commit();
				return true;
			} catch (Exception e) {
				MessageBox.Show(e.Message);
				command.Transaction.Rollback();
				return false;
			}
		}

		public bool SaveStockAdjustmentTransaction(frmStockAdjustment sender)
		{

			string transactionNumber = null;
			string transactionType = null;
			int transactionSequence = 0;

			//Specify transaction type
			transactionType = "ADJI";

			//Generate transaction number
			sqlCommand = "SELECT * FROM CONFIGURATION";
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds == null) {
				MessageBox.Show("Unable to obtain last transaction number.", "Possible Database Error");
				return false;
			}
			transactionSequence = Convert.ToInt32(ds.Tables[0].Rows[0]["TRANSACTIONNOSEQUENCE"]) + 1;
			transactionNumber = string.Format("{0}{1}", transactionType, transactionSequence.ToString());
			sqlCommand = string.Format("UPDATE CONFIGURATION SET TRANSACTIONNOSEQUENCE = '{0}'", transactionSequence);
			command.CommandText = sqlCommand;
			command.ExecuteNonQuery();

			//Save transaction entries
			command.Transaction = connection.BeginTransaction();
			try {
				//Insert record into table dispensarytransaction
				sqlCommand = string.Format("INSERT INTO INVENTORYTRANSACTION VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", transactionNumber, transactionType, sender.txtNote.Text, Program.userName, ChangeDateFormat(System.DateTime.Today), sender.cmbSupplierCode.SelectedValue);
				command.CommandText = sqlCommand;
				command.ExecuteNonQuery();

				foreach (DataRow dr in sender.tableAddedBatches.Rows) {
					//Update quantities of affected drugs in table drugbatch
					sqlCommand = string.Format("UPDATE DRUGBATCH SET QUANTITY = '{0}', EXPIRYDATE = '{1}' WHERE DRUGCODE = '{2}' AND BATCHNO = '{3}';", dr["Quantity"].ToString(), ChangeDateFormat(Convert.ToDateTime(dr["Expiry Date"])), dr["Drug Code"].ToString(), dr["Batch No."].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();

					//Insert records into table dispensaryitem for each dispensed item
					sqlCommand = string.Format("INSERT INTO INVENTORYITEM VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", transactionNumber, dr["Drug Code"].ToString(), dr["Batch No."].ToString(), dr["Quantity"].ToString(), dr["Notes"].ToString());
					command.CommandText = sqlCommand;
					command.ExecuteNonQuery();
				}
				command.Transaction.Commit();
				return true;
			} catch (Exception e) {
				MessageBox.Show(e.Message);
				command.Transaction.Rollback();
				return false;
			}
		}

		private int GetCurrentBatchQuantity(string drugCode, string batchNumber)
		{
			sqlCommand = string.Format("SELECT QUANTITY FROM DRUGBATCH WHERE DRUGCODE = '{0}' AND BATCHNO = '{1}'", drugCode, batchNumber);
			// dr["Batch No."].ToString(), dr["Batch No."].ToString());
			DataSet ds = ExecuteDatabaseQuery(sqlCommand);
			if (ds != null) {
				DataTable dt = ds.Tables[0];
				DataRow dr = dt.Rows[0];

				return Convert.ToInt32(dr["Quantity"]);
			}
			return 0;
		}

		public DataSet ReadAllDispensaryTransactions()
		{
			sqlCommand = "SELECT T.TRANSACTIONNO, T.TRANSACTIONTYPE, T.TRANSACTIONNOTE, T.USERID, T.TRANSACTIONDATE, T.EMPLOYEEID, T.PRACTITIONER, I.DRUGCODE, I.BATCHNNO, I.QUANTITY, I.SPECIALNOTE FROM DISPENSARYTRANSACTION T, DISPENSEITEM I WHERE T.TRANSACTIONNO = I.TRANSACTIONNO;";

			return ExecuteDatabaseQuery(sqlCommand);
		}

		public DataSet ReadAllInventoryTransactions()
		{
			sqlCommand = "SELECT T.TRANSACTIONNO, T.TRANSACTIONTYPE, T.TRANSACTIONNOTE, T.USERID, T.TRANSACTIONDATE, T.SUPPLIERCODE, I.DRUGCODE, I.BATCHNNO, I.QUANTITY, I.SPECIALNOTE FROM INVENTORYTRANSACTION T, INVENTORYITEM I WHERE T.TRANSACTIONNO = I.TRANSACTIONNO;";
			return ExecuteDatabaseQuery(sqlCommand);
		}

		#endregion
	}
}
