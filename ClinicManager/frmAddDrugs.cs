using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pharmacy
{

	public partial class frmAddDrugs : Form
	{
		private DataTable tableDrugs;
		private DataTable tableBatches;
		private DataTable tableAddedDrugs;

		private DataGridView dgdAddedDrugs;
		public frmAddDrugs(ref DataTable tableAddedDrugs, ref DataGridView dgdDrugs)
		{
			InitializeComponent();
			InitializeDataTables();
			this.tableAddedDrugs = tableAddedDrugs;
			this.dgdAddedDrugs = dgdDrugs;
		}

		private void InitializeDataTables()
		{
			tableDrugs = (new DataAccess().ReadAllDrugRecords()).Tables[0];
			if (tableDrugs != null) {
				this.cmbDrugCode.DisplayMember = "DRUGCODE";
				this.cmbDrugCode.ValueMember = "DRUGCODE";
				this.cmbDrugCode.DataSource = tableDrugs;
			}

			tableBatches = (new DataAccess().ReadAllBatchRecords()).Tables[0];
			UpdateBatchesDropDownList();
		}

		private void UpdateBatchesDropDownList()
		{
			if (tableBatches != null) {
				string query = string.Format("DRUGCODE = '{0}'", cmbDrugCode.SelectedValue);
				DataView viewBatches = tableBatches.DefaultView;
				viewBatches.RowFilter = (query);
				this.cmbBatchNo.ValueMember = "BATCHNO";
				this.cmbBatchNo.DisplayMember = "BATCHNO";
				this.cmbBatchNo.DataSource = viewBatches;
			}
		}

		private void cmbDrugCode_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.cmbDrugCode.Text)) {
				return;
			}
			string query = string.Format("DRUGCODE = '{0}'", cmbDrugCode.SelectedValue);
			DataRow[] selectedDrug = tableDrugs.Select(query);
			txtDescription.Text = selectedDrug[0]["NAME"].ToString();
			UpdateBatchesDropDownList();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnAddAndClose_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbDrugCode.Text)) {
				MessageBox.Show("Please Enter a Drug Code.", "Missing Input");
				cmbDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(cmbBatchNo.Text)) {
				MessageBox.Show("Please Enter a valid Batch Number.", "Missing Input");
				cmbBatchNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(txtQuantity.Text)) {
				MessageBox.Show("Please specify the Quantity.", "Missing Input");
				txtQuantity.Focus();
				return;
			}
			int quantity = 0;
			if (!(int.TryParse(txtQuantity.Text, out quantity))) {
				MessageBox.Show("Only numeric (integer) values permitted for Quantity.", "Invalid Input");
				txtQuantity.SelectAll();
				txtQuantity.Focus();
				return;
			}
			if (quantity < 1) {
				MessageBox.Show("Quantity must be greater than zero.", "Invalid Input");
				txtQuantity.Focus();
				return;
			}

			DataRow dr = tableAddedDrugs.NewRow();
			dr["Drug Code"] = cmbDrugCode.SelectedValue;
			dr["Drug Name"] = txtDescription.Text;
			dr["Batch No."] = cmbBatchNo.SelectedValue;
			dr["Quantity"] = quantity;
			dr["Notes"] = txtNote.Text;
			tableAddedDrugs.Rows.Add(dr);

			dgdAddedDrugs.Update();
			this.Close();
		}

		private void btnAddAndNew_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cmbDrugCode.Text)) {
				MessageBox.Show("Please Enter a Drug Code.", "Missing Input");
				cmbDrugCode.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(cmbBatchNo.Text)) {
				MessageBox.Show("Please Enter a valid Batch Number.", "Missing Input");
				cmbBatchNo.Focus();
				return;
			}
			if (string.IsNullOrWhiteSpace(txtQuantity.Text)) {
				MessageBox.Show("Please specify the Quantity.", "Missing Input");
				txtQuantity.Focus();
				return;
			}
			int quantity = 0;
			if (!(int.TryParse(txtQuantity.Text, out quantity))) {
				MessageBox.Show("Only numeric (integer) values permitted for Quantity.", "Invalid Input");
				txtQuantity.SelectAll();
				txtQuantity.Focus();
				return;
			}
			if (quantity < 1) {
				MessageBox.Show("Quantity must be greater than zero.", "Invalid Input");
				txtQuantity.Focus();
				return;
			}

			DataRow dr = tableAddedDrugs.NewRow();
			dr["Drug Code"] = cmbDrugCode.SelectedValue;
			dr["Drug Name"] = txtDescription.Text;
			dr["Batch No."] = cmbBatchNo.SelectedValue;
			dr["Quantity"] = quantity;
			dr["Notes"] = txtNote.Text;
			tableAddedDrugs.Rows.Add(dr);

			dgdAddedDrugs.Update();

			cmbDrugCode.Text = "";
			txtDescription.Text = "";
			cmbBatchNo.Text = "";
			txtQuantity.Text = "";
			txtNote.Text = "";

			cmbDrugCode.Focus();
		}
	}
}
