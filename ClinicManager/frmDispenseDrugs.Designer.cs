using Microsoft.VisualBasic;
using System;
namespace Pharmacy
{
	partial class frmDispenseDrugs
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		private System.ComponentModel.IContainer components = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region "Windows Form Designer generated code"

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlLeftPadding = new System.Windows.Forms.Panel();
			this.pnlRightPadding = new System.Windows.Forms.Panel();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnSaveAndNew = new System.Windows.Forms.Button();
			this.btnSaveAndClose = new System.Windows.Forms.Button();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.pnlBottomPadding = new System.Windows.Forms.Panel();
			this.gbxFields = new System.Windows.Forms.GroupBox();
			this.lblNote = new System.Windows.Forms.Label();
			this.txtNote = new System.Windows.Forms.TextBox();
			this.txtDpt = new System.Windows.Forms.TextBox();
			this.lblEmpDpt = new System.Windows.Forms.Label();
			this.lblEmpDOB = new System.Windows.Forms.Label();
			this.txtEmpDOB = new System.Windows.Forms.TextBox();
			this.txtPractitionerName = new System.Windows.Forms.TextBox();
			this.cmbPractitionerNo = new System.Windows.Forms.ComboBox();
			this.btnAddDrugs = new System.Windows.Forms.Button();
			this.cmbEmpNo = new System.Windows.Forms.ComboBox();
			this.lblEmpNo = new System.Windows.Forms.Label();
			this.lblPractitionerNo = new System.Windows.Forms.Label();
			this.lblEmpName = new System.Windows.Forms.Label();
			this.lblPractitionerName = new System.Windows.Forms.Label();
			this.txtEmpName = new System.Windows.Forms.TextBox();
			this.gbxDataGrid = new System.Windows.Forms.GroupBox();
			this.dgdAddedDrugs = new System.Windows.Forms.DataGridView();
			this.gbxButtons.SuspendLayout();
			this.gbxFields.SuspendLayout();
			this.gbxDataGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.dgdAddedDrugs).BeginInit();
			this.SuspendLayout();
			// 
			// pnlLeftPadding
			// 
			this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
			this.pnlLeftPadding.Name = "pnlLeftPadding";
			this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlLeftPadding.TabIndex = 0;
			// 
			// pnlRightPadding
			// 
			this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
			this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
			this.pnlRightPadding.Name = "pnlRightPadding";
			this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
			this.pnlRightPadding.TabIndex = 0;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Controls.Add(this.btnSaveAndNew);
			this.gbxButtons.Controls.Add(this.btnSaveAndClose);
			this.gbxButtons.Controls.Add(this.btnReset);
			this.gbxButtons.Controls.Add(this.btnClose);
			this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxButtons.Location = new System.Drawing.Point(12, 0);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(886, 68);
			this.gbxButtons.TabIndex = 0;
			this.gbxButtons.TabStop = false;
			// 
			// btnSaveAndNew
			// 
			this.btnSaveAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndNew.FlatAppearance.BorderSize = 0;
			this.btnSaveAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndNew.Location = new System.Drawing.Point(438, 22);
			this.btnSaveAndNew.Name = "btnSaveAndNew";
			this.btnSaveAndNew.Size = new System.Drawing.Size(97, 33);
			this.btnSaveAndNew.TabIndex = 5;
			this.btnSaveAndNew.Text = "Save and New";
			this.btnSaveAndNew.UseVisualStyleBackColor = false;
			this.btnSaveAndNew.Click += new System.EventHandler(this.btnSaveAndNew_Click);
			// 
			// btnSaveAndClose
			// 
			this.btnSaveAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnSaveAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnSaveAndClose.FlatAppearance.BorderSize = 0;
			this.btnSaveAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnSaveAndClose.Location = new System.Drawing.Point(552, 22);
			this.btnSaveAndClose.Name = "btnSaveAndClose";
			this.btnSaveAndClose.Size = new System.Drawing.Size(97, 33);
			this.btnSaveAndClose.TabIndex = 6;
			this.btnSaveAndClose.Text = "Save and Close";
			this.btnSaveAndClose.UseVisualStyleBackColor = false;
			this.btnSaveAndClose.Click += new System.EventHandler(this.btnSaveAndClose_Click);
			// 
			// btnReset
			// 
			this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnReset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnReset.FlatAppearance.BorderSize = 0;
			this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnReset.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnReset.Location = new System.Drawing.Point(666, 22);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(97, 33);
			this.btnReset.TabIndex = 7;
			this.btnReset.Text = "Reset";
			this.btnReset.UseVisualStyleBackColor = false;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.btnClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnClose.FlatAppearance.BorderSize = 0;
			this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnClose.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnClose.Location = new System.Drawing.Point(780, 22);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(97, 33);
			this.btnClose.TabIndex = 8;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = false;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// pnlBottomPadding
			// 
			this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
			this.pnlBottomPadding.Name = "pnlBottomPadding";
			this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
			this.pnlBottomPadding.TabIndex = 0;
			// 
			// gbxFields
			// 
			this.gbxFields.Controls.Add(this.lblNote);
			this.gbxFields.Controls.Add(this.txtNote);
			this.gbxFields.Controls.Add(this.txtDpt);
			this.gbxFields.Controls.Add(this.lblEmpDpt);
			this.gbxFields.Controls.Add(this.lblEmpDOB);
			this.gbxFields.Controls.Add(this.txtEmpDOB);
			this.gbxFields.Controls.Add(this.txtPractitionerName);
			this.gbxFields.Controls.Add(this.cmbPractitionerNo);
			this.gbxFields.Controls.Add(this.btnAddDrugs);
			this.gbxFields.Controls.Add(this.cmbEmpNo);
			this.gbxFields.Controls.Add(this.lblEmpNo);
			this.gbxFields.Controls.Add(this.lblPractitionerNo);
			this.gbxFields.Controls.Add(this.lblEmpName);
			this.gbxFields.Controls.Add(this.lblPractitionerName);
			this.gbxFields.Controls.Add(this.txtEmpName);
			this.gbxFields.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbxFields.Location = new System.Drawing.Point(12, 68);
			this.gbxFields.Name = "gbxFields";
			this.gbxFields.Size = new System.Drawing.Size(886, 277);
			this.gbxFields.TabIndex = 0;
			this.gbxFields.TabStop = false;
			// 
			// lblNote
			// 
			this.lblNote.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNote.AutoSize = true;
			this.lblNote.Location = new System.Drawing.Point(571, 212);
			this.lblNote.Name = "lblNote";
			this.lblNote.Size = new System.Drawing.Size(36, 15);
			this.lblNote.TabIndex = 0;
			this.lblNote.Text = "Note:";
			// 
			// txtNote
			// 
			this.txtNote.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtNote.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtNote.Location = new System.Drawing.Point(611, 209);
			this.txtNote.Multiline = true;
			this.txtNote.Name = "txtNote";
			this.txtNote.Size = new System.Drawing.Size(266, 62);
			this.txtNote.TabIndex = 3;
			// 
			// txtDpt
			// 
			this.txtDpt.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtDpt.Enabled = false;
			this.txtDpt.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtDpt.Location = new System.Drawing.Point(611, 88);
			this.txtDpt.Name = "txtDpt";
			this.txtDpt.Size = new System.Drawing.Size(266, 23);
			this.txtDpt.TabIndex = 29;
			this.txtDpt.TabStop = false;
			// 
			// lblEmpDpt
			// 
			this.lblEmpDpt.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmpDpt.AutoSize = true;
			this.lblEmpDpt.Location = new System.Drawing.Point(534, 93);
			this.lblEmpDpt.Name = "lblEmpDpt";
			this.lblEmpDpt.Size = new System.Drawing.Size(73, 15);
			this.lblEmpDpt.TabIndex = 0;
			this.lblEmpDpt.Text = "Department:";
			// 
			// lblEmpDOB
			// 
			this.lblEmpDOB.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmpDOB.AutoSize = true;
			this.lblEmpDOB.Location = new System.Drawing.Point(37, 93);
			this.lblEmpDOB.Name = "lblEmpDOB";
			this.lblEmpDOB.Size = new System.Drawing.Size(89, 15);
			this.lblEmpDOB.TabIndex = 0;
			this.lblEmpDOB.Text = "Employee DOB:";
			// 
			// txtEmpDOB
			// 
			this.txtEmpDOB.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtEmpDOB.Enabled = false;
			this.txtEmpDOB.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtEmpDOB.Location = new System.Drawing.Point(130, 88);
			this.txtEmpDOB.Name = "txtEmpDOB";
			this.txtEmpDOB.Size = new System.Drawing.Size(266, 23);
			this.txtEmpDOB.TabIndex = 28;
			this.txtEmpDOB.TabStop = false;
			// 
			// txtPractitionerName
			// 
			this.txtPractitionerName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtPractitionerName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtPractitionerName.Location = new System.Drawing.Point(130, 142);
			this.txtPractitionerName.Name = "txtPractitionerName";
			this.txtPractitionerName.Size = new System.Drawing.Size(266, 23);
			this.txtPractitionerName.TabIndex = 25;
			this.txtPractitionerName.TabStop = false;
			// 
			// cmbPractitionerNo
			// 
			this.cmbPractitionerNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbPractitionerNo.FormattingEnabled = true;
			this.cmbPractitionerNo.Location = new System.Drawing.Point(611, 142);
			this.cmbPractitionerNo.Name = "cmbPractitionerNo";
			this.cmbPractitionerNo.Size = new System.Drawing.Size(266, 23);
			this.cmbPractitionerNo.TabIndex = 2;
			// 
			// btnAddDrugs
			// 
			this.btnAddDrugs.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.btnAddDrugs.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnAddDrugs.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.btnAddDrugs.FlatAppearance.BorderSize = 0;
			this.btnAddDrugs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAddDrugs.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.btnAddDrugs.Location = new System.Drawing.Point(130, 207);
			this.btnAddDrugs.Name = "btnAddDrugs";
			this.btnAddDrugs.Size = new System.Drawing.Size(266, 25);
			this.btnAddDrugs.TabIndex = 4;
			this.btnAddDrugs.Text = "Add Drugs";
			this.btnAddDrugs.UseVisualStyleBackColor = false;
			this.btnAddDrugs.Click += new System.EventHandler(this.btnAddDrugs_Click);
			// 
			// cmbEmpNo
			// 
			this.cmbEmpNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmbEmpNo.FormattingEnabled = true;
			this.cmbEmpNo.Location = new System.Drawing.Point(611, 34);
			this.cmbEmpNo.Name = "cmbEmpNo";
			this.cmbEmpNo.Size = new System.Drawing.Size(266, 23);
			this.cmbEmpNo.TabIndex = 1;
			this.cmbEmpNo.SelectedIndexChanged += new System.EventHandler(this.cmbEmpNo_SelectedIndexChanged);
			// 
			// lblEmpNo
			// 
			this.lblEmpNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmpNo.AutoSize = true;
			this.lblEmpNo.Location = new System.Drawing.Point(498, 39);
			this.lblEmpNo.Name = "lblEmpNo";
			this.lblEmpNo.Size = new System.Drawing.Size(109, 15);
			this.lblEmpNo.TabIndex = 0;
			this.lblEmpNo.Text = "Employee Number:";
			// 
			// lblPractitionerNo
			// 
			this.lblPractitionerNo.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPractitionerNo.AutoSize = true;
			this.lblPractitionerNo.Location = new System.Drawing.Point(489, 147);
			this.lblPractitionerNo.Name = "lblPractitionerNo";
			this.lblPractitionerNo.Size = new System.Drawing.Size(118, 15);
			this.lblPractitionerNo.TabIndex = 0;
			this.lblPractitionerNo.Text = "Practitioner Number:";
			// 
			// lblEmpName
			// 
			this.lblEmpName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblEmpName.AutoSize = true;
			this.lblEmpName.Location = new System.Drawing.Point(29, 39);
			this.lblEmpName.Name = "lblEmpName";
			this.lblEmpName.Size = new System.Drawing.Size(97, 15);
			this.lblEmpName.TabIndex = 0;
			this.lblEmpName.Text = "Employee Name:";
			// 
			// lblPractitionerName
			// 
			this.lblPractitionerName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblPractitionerName.AutoSize = true;
			this.lblPractitionerName.Location = new System.Drawing.Point(20, 147);
			this.lblPractitionerName.Name = "lblPractitionerName";
			this.lblPractitionerName.Size = new System.Drawing.Size(106, 15);
			this.lblPractitionerName.TabIndex = 0;
			this.lblPractitionerName.Text = "Practitioner Name:";
			// 
			// txtEmpName
			// 
			this.txtEmpName.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.txtEmpName.Enabled = false;
			this.txtEmpName.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.txtEmpName.Location = new System.Drawing.Point(130, 34);
			this.txtEmpName.Name = "txtEmpName";
			this.txtEmpName.Size = new System.Drawing.Size(266, 23);
			this.txtEmpName.TabIndex = 16;
			this.txtEmpName.TabStop = false;
			// 
			// gbxDataGrid
			// 
			this.gbxDataGrid.Controls.Add(this.dgdAddedDrugs);
			this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbxDataGrid.Location = new System.Drawing.Point(12, 345);
			this.gbxDataGrid.Name = "gbxDataGrid";
			this.gbxDataGrid.Size = new System.Drawing.Size(886, 84);
			this.gbxDataGrid.TabIndex = 0;
			this.gbxDataGrid.TabStop = false;
			this.gbxDataGrid.Text = "Drugs";
			// 
			// dgdAddedDrugs
			// 
			this.dgdAddedDrugs.AllowUserToAddRows = false;
			this.dgdAddedDrugs.AllowUserToDeleteRows = false;
			this.dgdAddedDrugs.AllowUserToOrderColumns = true;
			this.dgdAddedDrugs.BackgroundColor = System.Drawing.Color.White;
			this.dgdAddedDrugs.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgdAddedDrugs.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dgdAddedDrugs.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdAddedDrugs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgdAddedDrugs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgdAddedDrugs.GridColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(192)), Convert.ToInt32(Convert.ToByte(255)));
			this.dgdAddedDrugs.Location = new System.Drawing.Point(3, 19);
			this.dgdAddedDrugs.Name = "dgdAddedDrugs";
			this.dgdAddedDrugs.ReadOnly = true;
			this.dgdAddedDrugs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgdAddedDrugs.RowHeadersVisible = false;
			this.dgdAddedDrugs.Size = new System.Drawing.Size(880, 62);
			this.dgdAddedDrugs.TabIndex = 1;
			this.dgdAddedDrugs.TabStop = false;
			// 
			// frmDispenseDrugs
			// 
			this.AcceptButton = this.btnSaveAndNew;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7f, 15f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(910, 441);
			this.Controls.Add(this.gbxDataGrid);
			this.Controls.Add(this.gbxFields);
			this.Controls.Add(this.pnlBottomPadding);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.pnlRightPadding);
			this.Controls.Add(this.pnlLeftPadding);
			this.Font = new System.Drawing.Font("Segoe UI", 9f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "frmDispenseDrugs";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "PatientInput";
			this.gbxButtons.ResumeLayout(false);
			this.gbxFields.ResumeLayout(false);
			this.gbxFields.PerformLayout();
			this.gbxDataGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.dgdAddedDrugs).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlLeftPadding;
		private System.Windows.Forms.Panel pnlRightPadding;
		public System.Windows.Forms.GroupBox gbxButtons;
		public System.Windows.Forms.Button btnSaveAndNew;
		public System.Windows.Forms.Button btnSaveAndClose;
		public System.Windows.Forms.Button btnReset;
		public System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel pnlBottomPadding;
		public System.Windows.Forms.GroupBox gbxFields;
		private System.Windows.Forms.Label lblEmpNo;
		private System.Windows.Forms.Label lblPractitionerNo;
		private System.Windows.Forms.Label lblEmpName;
		private System.Windows.Forms.Label lblPractitionerName;
		public System.Windows.Forms.TextBox txtEmpName;
		public System.Windows.Forms.ComboBox cmbEmpNo;
		public System.Windows.Forms.Button btnAddDrugs;
		private System.Windows.Forms.GroupBox gbxDataGrid;
		public System.Windows.Forms.DataGridView dgdAddedDrugs;
		public System.Windows.Forms.ComboBox cmbPractitionerNo;
		public System.Windows.Forms.TextBox txtPractitionerName;
		public System.Windows.Forms.TextBox txtDpt;
		private System.Windows.Forms.Label lblEmpDpt;
		private System.Windows.Forms.Label lblEmpDOB;
		public System.Windows.Forms.TextBox txtEmpDOB;
		private System.Windows.Forms.Label lblNote;
		public System.Windows.Forms.TextBox txtNote;
	}
}
